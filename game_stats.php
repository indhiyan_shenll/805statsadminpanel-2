<?php 
include_once('session_check.php'); 
include_once('connect.php');
include_once('common_functions.php');
include_once('usertype_check.php');
// error_reporting(E_ALL);
$msgPopup = "";
$home_gameid = '';
$game_id = "";
if (isset($_REQUEST['gid'])) {
    
    $xml_game_id = base64_decode($_REQUEST['gid']);
    $newqry_str = "SELECT * FROM game_details where xml_game_id =".$xml_game_id;
    $get_gameid_prepare = $conn->prepare($newqry_str);
    $get_gameid_prepare->execute();     
    $get_gameid_rowCount = $get_gameid_prepare->rowCount();
    if ($get_gameid_rowCount>0) { // If game already have stats
        $get_gameid_data  =  $get_gameid_prepare->fetchAll(PDO::FETCH_ASSOC);                                                   
        foreach ($get_gameid_data as $get_gameid_value) {
            $home_gameid = $get_gameid_value['id'];
            $game_id = $home_gameid;
            $home_team_code = $get_gameid_value['home_team_code'];
            $visitor_team_code = $get_gameid_value['visitor_team_code'];            
            $home_team_name = $get_gameid_value['home_name'];
            $visitor_team_name = $get_gameid_value['visitor_name']; 
            $game_season = $get_gameid_value['season'];
            $game_date = $get_gameid_value['date'];
            $explode_game_year = explode("-", $game_date);
            $game_year = $explode_game_year[0]; 
            $game_month = $explode_game_year[1];
            $game_day = $explode_game_year[2];
            $xml_name = "BB".$visitor_team_code.$home_team_code.$game_month.$game_day.$game_year.$xml_game_id.".xml";

        }
        $qryPlayerTotSts = "SELECT id,teamcode,playercode,year,season,checkname FROM individual_player_stats"." where gamecode='".$game_id."'";
        $preparePlayeSts = $conn->prepare($qryPlayerTotSts);
        $preparePlayeSts->execute();
        $getPlayerStsCount = $preparePlayeSts->rowCount(); 
        $getPlayerStsdata  =  $preparePlayeSts->fetchAll(PDO::FETCH_ASSOC);
        foreach ($getPlayerStsdata as $playerStsval) {
            //$indplayerid = $playerStsval['id'];
            $indplayerid = $getPlayerCode = $playerStsval['playercode'];
            $getPlayerYear = $playerStsval['year'];
            $getPlayerSeason = $playerStsval['season'];
            $getTeamCode = $playerStsval['teamcode'];
			$checkname = $playerStsval['checkname'];

            $ArrIndPlayerDetails[$indplayerid]["playercode"]=$getPlayerCode;
            $ArrIndPlayerDetails[$indplayerid]["year"]=$getPlayerYear;
            $ArrIndPlayerDetails[$indplayerid]["season"]=$getPlayerSeason;
            $ArrIndPlayerDetails[$indplayerid]["teamcode"]=$getTeamCode;
			$ArrIndPlayerDetails[$indplayerid]["checkname"]=$checkname;
        }
        $qryTeamTotSts = "SELECT id,teamcode,month,year,season FROM individual_team_stats"." where gamecode='".$game_id."'";
        $prepareTeamSts = $conn->prepare($qryTeamTotSts);
        $prepareTeamSts->execute();
        $getTeamStsCount = $prepareTeamSts->rowCount(); 
        $getTeamStsdata  =  $prepareTeamSts->fetchAll(PDO::FETCH_ASSOC);
        foreach ($getTeamStsdata as $TeamStsval) {
            $indTeamStsid = $TeamStsval['id'];
            $getTeamStsMonth = $TeamStsval['month'];
            $getTeamStsYear = $TeamStsval['year'];
            $getTeamStsSeason = $TeamStsval['season'];
            $getTeamStsTeamCode = $TeamStsval['teamcode'];

            $ArrIndTeamDetails[$getTeamStsTeamCode]["month"]=$getTeamStsMonth;          
            $ArrIndTeamDetails[$getTeamStsTeamCode]["year"]=$getTeamStsYear;
            $ArrIndTeamDetails[$getTeamStsTeamCode]["season"]=$getTeamStsSeason;
            
        }
    }
    else { // If game not having stats
        $newqry_str = "SELECT * FROM games_info where id =".$xml_game_id;
        $get_gameid_prepare = $conn->prepare($newqry_str);
        $get_gameid_prepare->execute();     
        $get_gameid_rowCount = $get_gameid_prepare->rowCount();
        $get_gameid_value  =  $get_gameid_prepare->fetch();

        $home_team_code = $get_gameid_value['home_team_id'];
        $visitor_team_code = $get_gameid_value['visitor_team_id'];            
        $home_team_name = json_decode(getTeamName($get_gameid_value['home_team_id']), true);
        $visitor_team_name = json_decode(getTeamName($get_gameid_value['visitor_team_id']), true); 
        $game_season = $get_gameid_value['season'];
		$game_location = $get_gameid_value['location'];
        $game_date = $get_gameid_value['date'];
		$game_time = $get_gameid_value['time'];
        $explode_game_year = explode("/", $game_date);
        $game_year = $explode_game_year[2];
        $game_month = $explode_game_year[0];
        $game_day = $explode_game_year[1];  
        $detail_game_date = $game_year."-".$game_month."-".$game_day; 
        $xml_name = "BB".$visitor_team_code.$home_team_code.$game_month.$game_day.$game_year.$xml_game_id.".xml";

        $qryPlayerTotSts = "SELECT * FROM player_info where team_id in ($home_team_code, $visitor_team_code)";
        $preparePlayeSts = $conn->prepare($qryPlayerTotSts);
        $preparePlayeSts->execute();
        $getPlayerStsCount = $preparePlayeSts->rowCount(); 
        $getPlayerStsdata  =  $preparePlayeSts->fetchAll(PDO::FETCH_ASSOC);
        foreach ($getPlayerStsdata as $playerStsval) {
            $indplayerid = $playerStsval['id'];
            $getPlayerCode = $playerStsval['id'];
            $getPlayerYear = $game_year;
            $getPlayerSeason = $game_season;
            $getTeamCode = $playerStsval['team_id'];

            $ArrIndPlayerDetails[$indplayerid]["playercode"]=$getPlayerCode;
            $ArrIndPlayerDetails[$indplayerid]["year"]=$getPlayerYear;
            $ArrIndPlayerDetails[$indplayerid]["season"]=$getPlayerSeason;
            $ArrIndPlayerDetails[$indplayerid]["teamcode"]=$getTeamCode;
			$ArrIndPlayerDetails[$indplayerid]["checkname"] = $playerStsval["lastname"].",".$playerStsval["firstname"];;
        }
        $qryTeamTotSts = "SELECT * FROM teams_info"." where id in ($home_team_code, $visitor_team_code)";
        $prepareTeamSts = $conn->prepare($qryTeamTotSts);
        $prepareTeamSts->execute();
        $getTeamStsCount = $prepareTeamSts->rowCount(); 
        $getTeamStsdata  =  $prepareTeamSts->fetchAll(PDO::FETCH_ASSOC);
        foreach ($getTeamStsdata as $TeamStsval) {
            $indTeamStsid = $TeamStsval['id'];
            $getTeamStsMonth = $game_month;
            $getTeamStsYear = $game_year;
            $getTeamStsSeason = $game_season;
            $getTeamStsTeamCode = $TeamStsval['id'];

            $ArrIndTeamDetails[$getTeamStsTeamCode]["month"]=$getTeamStsMonth;          
            $ArrIndTeamDetails[$getTeamStsTeamCode]["year"]=$getTeamStsYear;
            $ArrIndTeamDetails[$getTeamStsTeamCode]["season"]=$getTeamStsSeason;
            $ArrTeamDetails[$indTeamStsid]["team_name"] = $TeamStsval['team_name'];
			$ArrTeamDetails[$indTeamStsid]["abbrevation"] = $TeamStsval['abbrevation'];
        }
    }
}
$newgameflag = false;
//Visitor team
if (isset($_POST['visitor_gp'])){
    $getVisitorId = $_POST['visitor_id'];   
    $getVisitorGp = $_POST['visitor_gp'];   
    $getVisitorGs = $_POST['visitor_gs'];
    $getVisitor2ptmade = $_POST['visitor_2ptmade'];
    $getVisitor2ptatt = $_POST['visitor_2ptatt'];
    $getVisitor3fgm = $_POST['visitor_3fgm'];
    $getVisitor3fga = $_POST['visitor_3fga'];
    $getVisitorftm = $_POST['visitor_ftm'];
    $getVisitorfta = $_POST['visitor_fta'];
    $getVisitortp = $_POST['visitor_tp'];
    $getVisitororeb = $_POST['visitor_oreb'];
    $getVisitordreb = $_POST['visitor_dreb'];
    $getVisitortreb = $_POST['visitor_treb'];
    $getVisitorpf = $_POST['visitor_pf'];
    $getVisitortf = $_POST['visitor_tf'];
    $getVisitorast = $_POST['visitor_ast'];
    $getVisitorto = $_POST['visitor_to'];
    $getVisitorblk = $_POST['visitor_blk'];
    $getVisitorstl = $_POST['visitor_stl'];
    $getVisitormin = $_POST['visitor_min'];

	// If Game Details not exists for the game, add one entry to the table
	if(empty($game_id)) {        
		
		$insertPlayer = $conn->prepare("INSERT INTO game_details(xml_game_id, date, time, season, home_team_code, visitor_team_code, home_abbriviation, visitor_abbriviation, home_name, visitor_name, location) VALUES (:xml_game_id, :date, :time, :season, :home_team_code, :visitor_team_code, :home_abbriviation, :visitor_abbriviation, :home_name, :visitor_name, :location)");    

		$insertPlayerArr = array(":xml_game_id"=>$xml_game_id, ":date"=>$detail_game_date, ":time"=>$game_time, ":season"=>$game_season, ":home_team_code"=>$home_team_code, ":visitor_team_code"=>$visitor_team_code, ":home_abbriviation"=>$ArrTeamDetails[$home_team_code]["abbrevation"], ":visitor_abbriviation"=>$ArrTeamDetails[$visitor_team_code]["abbrevation"], ":home_name"=>$ArrTeamDetails[$home_team_code]["team_name"], ":visitor_name"=>$ArrTeamDetails[$visitor_team_code]["team_name"], ":location"=>$game_location);
		$insertres = $insertPlayer->execute($insertPlayerArr);
		$game_id = $conn->lastInsertId();
		$newgameflag = true;
	}
    
    for ($i=0;$i<count($getVisitorId);$i++){   
		$playercode = $getVisitorId[$i];

        $checkVisitormin[$i] = $getVisitormin[$playercode];
        if (empty($checkVisitormin[$i])) {
            $checkVisitormin[$i] = '';
        } else {
            $checkVisitormin[$i];
        }		

        $SelectPlayer = $conn->prepare("SELECT * FROM individual_player_stats where playercode='".$playercode."' and gamecode='".$game_id."' and teamcode='".$visitor_team_code."' and season='".$game_season."'");
        $SelectPlayePrepareExe = $SelectPlayer->execute();
        $CntPlayer = $SelectPlayer->rowCount();
        if ($CntPlayer == 0) {
			/*Check playername already exists or not*/
			if(empty($ArrIndPlayerDetails[$playercode]["checkname"])) {
				$playerNameQry = $conn->prepare("SELECT firstname,lastname FROM player_info where id='".$playercode."'");
				$SelectPlayePrepareExe = $playerNameQry->execute();
				$FetchplayerName = $playerNameQry->fetch(PDO::FETCH_ASSOC);
				$checkname = $FetchplayerName["lastname"].",".$FetchplayerName["firstname"];
				$ArrIndPlayerDetails[$playercode]["checkname"] = $checkname;
			}
			else
				$checkname = $ArrIndPlayerDetails[$playercode]["checkname"];

            $insertPlayer = $conn->prepare("INSERT INTO individual_player_stats(playercode, year, season, checkname, gamecode, teamcode, gp, gs, fgm, fga, fgm3, fga3, ftm, fta, tp, oreb, dreb, treb, pf, tf, ast, to1, blk, stl, min) VALUES (:playercode, :year, :season, :checkname, :gamecode, :teamcode, :gp, :gs, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min)");    
            $insertPlayerArr = array(":playercode"=>$playercode, ":year"=>$game_year, ":season"=>$game_season, ":checkname"=>$checkname, ":gamecode"=>$game_id, ":teamcode"=>$visitor_team_code, ":gp"=>$getVisitorGp[$playercode], ":gs"=>$getVisitorGs[$playercode], ":fgm"=>$getVisitor2ptmade[$playercode], ":fga"=>$getVisitor2ptatt[$playercode], ":fgm3"=>$getVisitor3fgm[$playercode], ":fga3"=>$getVisitor3fga[$playercode], ":ftm"=>$getVisitorftm[$playercode], ":fta"=>$getVisitorfta[$playercode], ":tp"=>$getVisitortp[$playercode], ":oreb"=>$getVisitororeb[$playercode], ":dreb"=>$getVisitordreb[$playercode], ":treb"=>$getVisitortreb[$playercode], ":pf"=>$getVisitorpf[$playercode], ":tf"=>$getVisitortf[$playercode], ":ast"=>$getVisitorast[$playercode], ":to1"=>$getVisitorto[$playercode], ":blk"=>$getVisitorblk[$playercode], ":stl"=>$getVisitorstl[$playercode], ":min"=>$checkVisitormin[$i]);
            $insertres = $insertPlayer->execute($insertPlayerArr);            

        } else {

            $VisitorUpdateQry = "UPDATE individual_player_stats SET gp ='".$getVisitorGp[$playercode]."',gs='".$getVisitorGs[$playercode]."',fgm='".$getVisitor2ptmade[$playercode]
            ."',fga='".$getVisitor2ptatt[$playercode]."',fgm3='".$getVisitor3fgm[$playercode]."',fga3='".$getVisitor3fga[$playercode]
            ."',ftm='".$getVisitorftm[$playercode]."',fta='".$getVisitorfta[$playercode]."',tp='".$getVisitortp[$playercode]
            ."',oreb='".$getVisitororeb[$playercode]  ."',dreb='".$getVisitordreb[$playercode]."',treb='".$getVisitortreb[$playercode]
            ."',pf='".$getVisitorpf[$playercode]."',tf='".$getVisitortf[$playercode]."',ast='".$getVisitorast[$playercode]."',to1='".$getVisitorto[$playercode]
            ."',blk='".$getVisitorblk[$playercode]."',stl='".$getVisitorstl[$playercode]."',min='".$checkVisitormin[$i]
            ."' where playercode='".$playercode."' and gamecode='".$game_id."' and teamcode='".$visitor_team_code."' and season='".$game_season."'";  
            $VisitorUpdatePrepare = $conn->prepare($VisitorUpdateQry);
            $VisitorUpdateExe = $VisitorUpdatePrepare->execute(); 
        }

        //Insert/Update into player_stats_bb        
        $year = $game_year;
        $season = $game_season;
		$checkname = $ArrIndPlayerDetails[$playercode]["checkname"];

        $sqlQry = "SELECT SUM(gp) as gp,SUM(gs) as gs,SUM(fgm) as fgm,SUM(fga) as fga,SUM(fgm3) as fgm3,SUM(fga3) as fga3, 
        SUM(ftm) as ftm,SUM(fta) as fta,SUM(tp) as tp,SUM(oreb) as oreb,SUM(dreb) as dreb,SUM(treb) as treb, 
        SUM(pf) as pf,SUM(tf) as tf,SUM(ast) as ast,SUM(to1) as to1, SUM(blk) as blk,SUM(stl) as stl,SUM(min) as min
        FROM individual_player_stats where playercode = ".$playercode." and season = ".$season." and teamcode = ".$visitor_team_code ;
        $sqldata = $conn->prepare($sqlQry);
        $sqldata->execute();
        if($sqldata->rowCount()>0){
            //Check player&team&year&season already exists or not
            $fetchdata = $sqldata->fetch(PDO::FETCH_ASSOC);
            $gp=$fetchdata["gp"];
            $gs=$fetchdata["gs"];
            $fgm=$fetchdata["fgm"];
            $fga=$fetchdata["fga"];
            $fgm3=$fetchdata["fgm3"];
            $fga3=$fetchdata["fga3"];
            $ftm=$fetchdata["ftm"];
            $fta=$fetchdata["fta"];
            $tp=$fetchdata["tp"];
            $oreb=$fetchdata["oreb"];
            $dreb=$fetchdata["dreb"];
            $treb=$fetchdata["treb"];
            $pf=$fetchdata["pf"];
            $tf=$fetchdata["tf"];
            $ast=$fetchdata["ast"];
            $to1=$fetchdata["to1"];
            $blk=$fetchdata["blk"];
            $stl=$fetchdata["stl"];
            $min=$fetchdata["min"];

            $sqlQry2 = "SELECT * FROM player_stats_bb where playercode = ".$playercode." and season = ".$season." and teamcode = ".$visitor_team_code;
            $sqldata2 = $conn->prepare($sqlQry2);
            $sqldata2->execute();
            if($sqldata2->rowCount()>0){
                //Update player_stats_bb

                $updateqry="update player_stats_bb set gp ='".$gp."',gs='".$gs."',fgm='".$fgm."',fga='".$fga."',fgm3='".$fgm3."',fga3='".$fga3."',
                ftm='".$ftm."',fta='".$fta."',tp='".$tp."',oreb='".$oreb."',dreb='".$dreb."',treb='".$treb."',pf='".$pf."',tf='".$tf."',
                ast='".$ast."',to1='".$to1."',blk='".$blk."',stl='".$stl."',min='".$min."',checkname='".$checkname."' where playercode = ".$playercode." and season = ".$season." and teamcode = ".$visitor_team_code;
                $PrepareQry = $conn->prepare($updateqry);
                $PrepareQry->execute(); 
            } else {
                //Insert player_stats_bb
                $insertqry="insert into player_stats_bb (checkname,customer_id,gp,gs,fgm,fga,fgm3,fga3,ftm,fta,tp,oreb,dreb,treb,pf,tf,ast,to1,blk,stl,min,playercode,year,season,teamcode) 
                values ('".$checkname."','".$cid."','".$gp."','".$gs."','".$fgm."','".$fga."','".$fgm3."','".$fga3."','".$ftm."','".$fta."','".$tp."','".$oreb."','".$dreb."','".$treb."','".$pf."','".$tf."','".$ast."','".$to1."','".$blk."','".$stl."','".$min."','".$playercode."','".$year."','".$season."','".$visitor_team_code."')";
                $PrepareQry = $conn->prepare($insertqry);
                $PrepareQry->execute(); 
            }
        }
        
    }
}
//Home team
if (isset($_POST['home_gp'])){
    $gethomeId = $_POST['home_id']; 
    $gethomeGp = $_POST['home_gp']; 
    $gethomeGs = $_POST['home_gs'];
    $gethome2ptmade = $_POST['home_2ptmade'];
    $gethome2ptatt = $_POST['home_2ptatt'];
    $gethome3fgm = $_POST['home_3fgm'];
    $gethome3fga = $_POST['home_3fga'];
    $gethomeftm = $_POST['home_ftm'];
    $gethomefta = $_POST['home_fta'];
    $gethometp = $_POST['home_tp'];
    $gethomeoreb = $_POST['home_oreb'];
    $gethomedreb = $_POST['home_dreb'];
    $gethometreb = $_POST['home_treb'];
    $gethomepf = $_POST['home_pf'];
    $gethometf = $_POST['home_tf'];
    $gethomeast = $_POST['home_ast'];
    $gethometo = $_POST['home_to'];
    $gethomeblk = $_POST['home_blk'];
    $gethomestl = $_POST['home_stl'];
    $gethomemin = $_POST['home_min'];   
    
    for ($i=0;$i<count($gethomeId);$i++){   
		$playercode = $gethomeId[$i];
        $checkhomemin[$i] = $gethomemin[$playercode];
        if(empty($checkhomemin[$i])){
            $checkhomemin[$i] = '';
        } else{
            $checkhomemin[$i];
        }       

        $SelectPlayer = $conn->prepare("SELECT * FROM individual_player_stats where playercode='".$playercode."' and gamecode='".$game_id."' and teamcode='".$home_team_code."' and season='".$game_season."'");
        $SelectPlayePrepareExe = $SelectPlayer->execute();
        $CntPlayer = $SelectPlayer->rowCount();
        if ($CntPlayer == 0) {

			/*Check playername already exists or not*/
			if(empty($ArrIndPlayerDetails[$playercode]["checkname"])) {
				$playerNameQry = $conn->prepare("SELECT firstname,lastname FROM player_info where id='".$playercode."'");
				$SelectPlayePrepareExe = $playerNameQry->execute();
				$FetchplayerName = $playerNameQry->fetch(PDO::FETCH_ASSOC);
				$checkname = $FetchplayerName["lastname"].",".$FetchplayerName["firstname"];
				$ArrIndPlayerDetails[$playercode]["checkname"] = $checkname;
			}
			else
				$checkname = $ArrIndPlayerDetails[$playercode]["checkname"];

            $insertPlayer = $conn->prepare("INSERT INTO individual_player_stats(playercode, year, season, checkname, gamecode, teamcode, gp, gs, fgm, fga, fgm3, fga3, ftm, fta, tp, oreb, dreb, treb, pf, tf, ast, to1, blk, stl, min) VALUES (:playercode, :year, :season, :checkname, :gamecode, :teamcode, :gp, :gs, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min)");
            $insertPlayerArr = array(":playercode"=>$playercode, ":year"=>$game_year, ":season"=>$game_season, ":checkname"=>$checkname, ":gamecode"=>$game_id, ":teamcode"=>$home_team_code, ":gp"=>$gethomeGp[$playercode], ":gs"=>$gethomeGs[$playercode], ":fgm"=>$gethome2ptmade[$playercode], ":fga"=>$gethome2ptatt[$playercode], ":fgm3"=>$gethome3fgm[$playercode], ":fga3"=>$gethome3fga[$playercode], ":ftm"=>$gethomeftm[$playercode], ":fta"=>$gethomefta[$playercode], ":tp"=>$gethometp[$playercode], ":oreb"=>$gethomeoreb[$playercode], ":dreb"=>$gethomedreb[$playercode], ":treb"=>$gethometreb[$playercode], ":pf"=>$gethomepf[$playercode], ":tf"=>$gethometf[$playercode], ":ast"=>$gethomeast[$playercode], ":to1"=>$gethometo[$playercode], ":blk"=>$gethomeblk[$playercode], ":stl"=>$gethomestl[$playercode], ":min"=>$checkhomemin[$i]);
            $insertres = $insertPlayer->execute($insertPlayerArr);            
            
        } else {    
            $homeUpdateQry = "UPDATE individual_player_stats SET gp ='".$gethomeGp[$playercode]."',gs='".$gethomeGs[$playercode]."',fgm='".$gethome2ptmade[$playercode]
            ."',fga='".$gethome2ptatt[$playercode]."',fgm3='".$gethome3fgm[$playercode]."',fga3='".$gethome3fga[$playercode]
            ."',ftm='".$gethomeftm[$playercode]."',fta='".$gethomefta[$playercode]."',tp='".$gethometp[$playercode]
            ."',oreb='".$gethomeoreb[$playercode]."',dreb='".$gethomedreb[$playercode]."',treb='".$gethometreb[$playercode]
            ."',pf='".$gethomepf[$playercode]."',tf='".$gethometf[$playercode]."',ast='".$gethomeast[$playercode]."',to1='".$gethometo[$playercode]
            ."',blk='".$gethomeblk[$playercode]."',stl='".$gethomestl[$playercode]."',min='".$checkhomemin[$i]
            ."' where playercode='".$playercode."' and gamecode='".$game_id."' and teamcode='".$home_team_code."' and season='".$game_season."'"; 
            $homeUpdatePrepare = $conn->prepare($homeUpdateQry);
            $homeUpdateExe = $homeUpdatePrepare->execute();
        }     

        //Insert/Update into player_stats_bb
        
        $year = $game_year;
        $season = $game_season;
		$checkname = $ArrIndPlayerDetails[$playercode]["checkname"];

        $sqlQry = "SELECT checkname,SUM(gp) as gp,SUM(gs) as gs,SUM(fgm) as fgm,SUM(fga) as fga,SUM(fgm3) as fgm3,SUM(fga3) as fga3, 
        SUM(ftm) as ftm,SUM(fta) as fta,SUM(tp) as tp,SUM(oreb) as oreb,SUM(dreb) as dreb,SUM(treb) as treb, 
        SUM(pf) as pf,SUM(tf) as tf,SUM(ast) as ast,SUM(to1) as to1, SUM(blk) as blk,SUM(stl) as stl,SUM(min) as min
        FROM individual_player_stats where playercode = ".$playercode." and season = ".$season." and teamcode = ".$home_team_code;
        $sqldata = $conn->prepare($sqlQry);
        $sqldata->execute();
        if($sqldata->rowCount()>0){         
            //Check player&team&year&season already exists or not
            $fetchdata = $sqldata->fetch(PDO::FETCH_ASSOC);
            $gp=$fetchdata["gp"];
            $gs=$fetchdata["gs"];
            $fgm=$fetchdata["fgm"];
            $fga=$fetchdata["fga"];
            $fgm3=$fetchdata["fgm3"];
            $fga3=$fetchdata["fga3"];
            $ftm=$fetchdata["ftm"];
            $fta=$fetchdata["fta"];
            $tp=$fetchdata["tp"];
            $oreb=$fetchdata["oreb"];
            $dreb=$fetchdata["dreb"];
            $treb=$fetchdata["treb"];
            $pf=$fetchdata["pf"];
            $tf=$fetchdata["tf"];
            $ast=$fetchdata["ast"];
            $to1=$fetchdata["to1"];
            $blk=$fetchdata["blk"];
            $stl=$fetchdata["stl"];
            $min=$fetchdata["min"];

            $sqlQry2 = "SELECT * FROM player_stats_bb where playercode = ".$playercode." and season = ".$season." and teamcode = ".$home_team_code;
            $sqldata2 = $conn->prepare($sqlQry2);
            $sqldata2->execute();
            if($sqldata2->rowCount()>0){
                //Update player_stats_bb
                $updateqry="update player_stats_bb set gp ='".$gp."',gs='".$gs."',fgm='".$fgm."',fga='".$fga."',fgm3='".$fgm3."',fga3='".$fga3."',
                ftm='".$ftm."',fta='".$fta."',tp='".$tp."',oreb='".$oreb."',dreb='".$dreb."',treb='".$treb."',pf='".$pf."',tf='".$tf."',
                ast='".$ast."',to1='".$to1."',blk='".$blk."',stl='".$stl."',min='".$min."',checkname='".$checkname."' where playercode = ".$playercode." and season = ".$season." and teamcode = ".$home_team_code;
                $PrepareQry = $conn->prepare($updateqry);
                $PrepareQry->execute(); 
            }
            else{
                //Insert player_stats_bb
                $insertqry="insert into player_stats_bb (checkname,customer_id,gp,gs,fgm,fga,fgm3,fga3,ftm,fta,tp,oreb,dreb,treb,pf,tf,ast,to1,blk,stl,min,playercode,year,season,teamcode) 
                values ('".$checkname."','".$cid."','".$gp."','".$gs."','".$fgm."','".$fga."','".$fgm3."','".$fga3."','".$ftm."','".$fta."','".$tp."','".$oreb."','".$dreb."','".$treb."','".$pf."','".$tf."','".$ast."','".$to1."','".$blk."','".$stl."','".$min."','".$playercode."','".$year."','".$season."','".$home_team_code."')";             
                $PrepareQry = $conn->prepare($insertqry);
                $PrepareQry->execute(); 
            }
        }        
                
    }    
}

//Visitor team Pts_Stats
if (isset($_POST['visitor_pts_bench'])){

	$getvisitor_gp_total = $_POST['visitor_gp_total'][0];
    // $getvisitor_gs_total = $_POST['visitor_gs_total'];  
    $getvisitor_2ptmade_total = $_POST['visitor_2ptmade_total'];
    $getvisitor_2ptatt_total = $_POST['visitor_2ptatt_total'];
    $getvisitor_3fgm_total = $_POST['visitor_3fgm_total'];
    $getvisitor_3fga_total = $_POST['visitor_3fga_total'];
    $getvisitor_ftm_total = $_POST['visitor_ftm_total'];
    $getvisitor_fta_total = $_POST['visitor_fta_total'];
    $getvisitor_tp_total = $_POST['visitor_tp_total'];
    $getvisitor_oreb_total = $_POST['visitor_oreb_total'];
    $getvisitor_dreb_total = $_POST['visitor_dreb_total'];
    $getvisitor_treb_total = $_POST['visitor_treb_total'];
    $getvisitor_pf_total = $_POST['visitor_pf_total'];
    $getvisitor_tf_total = $_POST['visitor_tf_total'];
    $getvisitor_ast_total = $_POST['visitor_ast_total'];
    $getvisitor_to_total = $_POST['visitor_to_total'];
    $getvisitor_blk_total = $_POST['visitor_blk_total'];
    $getvisitor_stl_total = $_POST['visitor_stl_total'];
    $getvisitor_min_total = $_POST['visitor_min_total'];

    $getvisitor_pts_bench = $_POST['visitor_pts_bench'];
    $getvisitor_pts_to = $_POST['visitor_pts_to'];  
    $getvisitor_pts_paint = $_POST['visitor_pts_paint'];
    $getvisitor_pts_fastb = $_POST['visitor_pts_fastb'];
    $getvisitor_pts_ch2 = $_POST['visitor_pts_ch2'];
    $getvisitor_pts_w = $_POST['visitor_pts_w'];
    $getvisitor_pts_l = $_POST['visitor_pts_l'];

    $month = $game_month;       
    $year = $game_year;    
    $season = $game_season;

    $SelectTeam = $conn->prepare("SELECT * FROM individual_team_stats where teamcode='".$visitor_team_code."' and gamecode='$game_id' and season='$season'");
    $SelectTeamPrepareExe = $SelectTeam->execute();
    $CntTeam = $SelectTeam->rowCount();

    if ($CntTeam == 0) {

        $insertTeam = $conn->prepare("INSERT INTO individual_team_stats(gp, fgm, fga, fgm3, fga3, ftm, fta, tp, oreb, dreb, treb, pf, tf, ast, to1, blk, stl, min, Pts_bench, Pts_to, Pts_paint, Pts_fastb, Pts_ch2, W, L, teamcode, gamecode, season, month, year) VALUES (:gp, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min, :Pts_bench, :Pts_to, :Pts_paint, :Pts_fastb, :Pts_ch2, :W, :L, :teamcode, :gamecode, :season, :month, :year)");
        $insertTeamArr = array(":gp"=>$getvisitor_gp_total, ":fgm"=>$getvisitor_2ptmade_total, ":fga"=>$getvisitor_2ptatt_total, ":fgm3"=>$getvisitor_3fgm_total, ":fga3"=>$getvisitor_3fga_total, ":ftm"=>$getvisitor_ftm_total, ":fta"=>$getvisitor_fta_total, ":tp"=>$getvisitor_tp_total, ":oreb"=>$getvisitor_oreb_total, ":dreb"=>$getvisitor_dreb_total, ":treb"=>$getvisitor_treb_total, ":pf"=>$getvisitor_pf_total, ":tf"=>$getvisitor_tf_total, ":ast"=>$getvisitor_ast_total, ":to1"=>$getvisitor_to_total, ":blk"=>$getvisitor_blk_total, ":stl"=>$getvisitor_stl_total, ":min"=>$getvisitor_min_total, ":Pts_bench"=>$getvisitor_pts_bench, ":Pts_to"=>$getvisitor_pts_to, ":Pts_paint"=>$getvisitor_pts_paint, ":Pts_fastb"=>$getvisitor_pts_fastb, ":Pts_ch2"=>$getvisitor_pts_ch2, ":W"=>$getvisitor_pts_w, ":L"=>$getvisitor_pts_l, ":teamcode"=>$visitor_team_code, ":gamecode"=>$game_id, ":season"=>$season, ":month"=>$month, ":year"=>$year);
        $insertres = $insertTeam->execute($insertTeamArr); 
        
    } else {      

        $visitorTeamStatsUpdateQry = $conn->prepare("UPDATE individual_team_stats SET gp =:gp, fgm =:fgm, fga =:fga, fgm3 =:fgm3, fga3 =:fga3, ftm =:ftm, fta =:fta, tp =:tp, oreb =:oreb, dreb =:dreb, treb =:treb, pf =:pf, tf =:tf, ast =:ast, to1 =:to1, blk =:blk, stl =:stl, min =:min, Pts_bench =:Pts_bench, Pts_to =:Pts_to, Pts_paint =:Pts_paint, Pts_fastb =:Pts_fastb, Pts_ch2 =:Pts_ch2, W =:W, L =:L where teamcode=:teamcode and gamecode=:gamecode and season=:season");
        $visitorTeamStatsUpdateQryArr = array(":gp"=>$getvisitor_gp_total, ":fgm"=>$getvisitor_2ptmade_total, ":fga"=>$getvisitor_2ptatt_total, ":fgm3"=>$getvisitor_3fgm_total, ":fga3"=>$getvisitor_3fga_total, ":ftm"=>$getvisitor_ftm_total, ":fta"=>$getvisitor_fta_total, ":tp"=>$getvisitor_tp_total, ":oreb"=>$getvisitor_oreb_total, ":dreb"=>$getvisitor_dreb_total, ":treb"=>$getvisitor_treb_total, ":pf"=>$getvisitor_pf_total, ":tf"=>$getvisitor_tf_total, ":ast"=>$getvisitor_ast_total, ":to1"=>$getvisitor_to_total, ":blk"=>$getvisitor_blk_total, ":stl"=>$getvisitor_stl_total, ":min"=>$getvisitor_min_total, ":Pts_bench"=>$getvisitor_pts_bench, ":Pts_to"=>$getvisitor_pts_to, ":Pts_paint"=>$getvisitor_pts_paint, ":Pts_fastb"=>$getvisitor_pts_fastb, ":Pts_ch2"=>$getvisitor_pts_ch2, ":W"=>$getvisitor_pts_w, ":L"=>$getvisitor_pts_l, ":teamcode"=>$visitor_team_code, ":gamecode"=>$game_id, ":season"=>$season);
        $visitorTeamStatsUpdateExe = $visitorTeamStatsUpdateQry->execute($visitorTeamStatsUpdateQryArr); 
    }

    $TeamStssqlQry = "SELECT SUM(gp) as gp,SUM(fgm) as fgm,SUM(fga) as fga,SUM(fgm3) as fgm3,SUM(fga3) as fga3, 
    SUM(ftm) as ftm,SUM(fta) as fta,SUM(tp) as tp,SUM(oreb) as oreb,SUM(dreb) as dreb,SUM(treb) as treb, 
    SUM(pf) as pf,SUM(tf) as tf,SUM(ast) as ast,SUM(to1) as to1, SUM(blk) as blk,SUM(stl) as stl,SUM(min) as min,SUM(Pts_bench) as Pts_bench,
    SUM(Pts_to) as Pts_to,SUM(Pts_paint) as Pts_paint,SUM(Pts_fastb) as Pts_fastb,SUM(Pts_ch2) as Pts_ch2,SUM(W) as W,SUM(L) as L
    FROM individual_team_stats where month = ".$month." and year = ".$year." and season = ".$season." and teamcode = ".$visitor_team_code;
    $sqldata = $conn->prepare($TeamStssqlQry);
    $sqldata->execute();
    if($sqldata->rowCount()>0){         
        //Check player&team&year&season already exists or not
        $fetchdata = $sqldata->fetch(PDO::FETCH_ASSOC);
        $gp=$fetchdata["gp"];           
        $fgm=$fetchdata["fgm"];
        $fga=$fetchdata["fga"];
        $fgm3=$fetchdata["fgm3"];
        $fga3=$fetchdata["fga3"];
        $ftm=$fetchdata["ftm"];
        $fta=$fetchdata["fta"];
        $tp=$fetchdata["tp"];
        $oreb=$fetchdata["oreb"];
        $dreb=$fetchdata["dreb"];
        $treb=$fetchdata["treb"];
        $pf=$fetchdata["pf"];
        $tf=$fetchdata["tf"];
        $ast=$fetchdata["ast"];
        $to1=$fetchdata["to1"];
        $blk=$fetchdata["blk"];
        $stl=$fetchdata["stl"];
        $min=$fetchdata["min"];

        $Pts_bench=$fetchdata["Pts_bench"];
        $Pts_to=$fetchdata["Pts_to"];
        $Pts_paint=$fetchdata["Pts_paint"];
        $Pts_fastb=$fetchdata["Pts_fastb"];
        $Pts_ch2=$fetchdata["Pts_ch2"];
        $W=$fetchdata["W"];
        $L=$fetchdata["L"];

        $sqlQry2 = "SELECT * FROM team_stats_bb where month = ".$month." and year = ".$year." and season = ".$season." and teamcode = ".$visitor_team_code;
        $sqldata2 = $conn->prepare($sqlQry2);
        $sqldata2->execute();
        if($sqldata2->rowCount()>0){
            //Update team_stats_bb
            $updateqry="update team_stats_bb set gp ='".$gp."',fgm='".$fgm."',fga='".$fga."',fgm3='".$fgm3."',fga3='".$fga3."',
            ftm='".$ftm."',fta='".$fta."',tp='".$tp."',oreb='".$oreb."',dreb='".$dreb."',treb='".$treb."',pf='".$pf."',tf='".$tf."',
            ast='".$ast."',to1='".$to1."',blk='".$blk."',stl='".$stl."',min='".$min."',to1='".$to1."',Pts_bench='".$Pts_bench."',
            Pts_to='".$Pts_to."',Pts_paint='".$Pts_paint."',Pts_fastb='".$Pts_fastb."',Pts_ch2='".$Pts_ch2."',W='".$W."'
            ,L='".$L."' where month = ".$month." 
            and year = ".$year." and season = ".$season." and teamcode = ".$visitor_team_code;

            $PrepareQry = $conn->prepare($updateqry);
            $PrepareQry->execute(); 
        }
        else{
            //Insert team_stats_bb
            $insertqry="insert into team_stats_bb (customer_id,gp,fgm,fga,fgm3,fga3,ftm,fta,tp,oreb,dreb,treb,pf,tf,ast,to1,blk,stl,
                min,month,year,season,teamcode,Pts_bench,Pts_to,Pts_paint,Pts_fastb,Pts_ch2,W,L) 
            values ('".$cid."','".$gp."','".$fgm."','".$fga."','".$fgm3."','".$fga3."','".$ftm."','".$fta."','".$tp."','".$oreb."','".$dreb."',
                '".$treb."','".$pf."','".$tf."','".$ast."','".$to1."','".$blk."','".$stl."','".$min."','".$month."','".$year."',
                '".$season."','".$visitor_team_code."','".$Pts_bench."','".$Pts_to."','".$Pts_paint."','".$Pts_fastb."','".$Pts_ch2."','".$W."','".$L."')";
            $PrepareQry = $conn->prepare($insertqry);
            $PrepareQry->execute(); 
        }
    }           
    
}
//Home team Pts_Stats
if (isset($_POST['home_pts_bench'])){

	$gethome_gp_total = $_POST['home_gp_total'][0];
    // $gethome_gs_total = $_POST['home_gs_total'];    
    $gethome_2ptmade_total = $_POST['home_2ptmade_total'];
    $gethome_2ptatt_total = $_POST['home_2ptatt_total'];
    $gethome_3fgm_total = $_POST['home_3fgm_total'];
    $gethome_3fga_total = $_POST['home_3fga_total'];
    $gethome_ftm_total = $_POST['home_ftm_total'];
    $gethome_fta_total = $_POST['home_fta_total'];
    $gethome_tp_total = $_POST['home_tp_total'];
    $gethome_oreb_total = $_POST['home_oreb_total'];
    $gethome_dreb_total = $_POST['home_dreb_total'];
    $gethome_treb_total = $_POST['home_treb_total'];
    $gethome_pf_total = $_POST['home_pf_total'];
    $gethome_tf_total = $_POST['home_tf_total'];
    $gethome_ast_total = $_POST['home_ast_total'];
    $gethome_to_total = $_POST['home_to_total'];
    $gethome_blk_total = $_POST['home_blk_total'];
    $gethome_stl_total = $_POST['home_stl_total'];
    $gethome_min_total = $_POST['home_min_total'];

    $gethome_pts_bench = $_POST['home_pts_bench'];
    $gethome_pts_to = $_POST['home_pts_to'];    
    $gethome_pts_paint = $_POST['home_pts_paint'];
    $gethome_pts_fastb = $_POST['home_pts_fastb'];
    $gethome_pts_ch2 = $_POST['home_pts_ch2'];
    $gethome_pts_w = $_POST['home_pts_w'];
    $gethome_pts_l = $_POST['home_pts_l'];

    $month = $game_month;       
    $year = $game_year;    
    $season = $game_season;

    $SelectTeam = $conn->prepare("SELECT * FROM individual_team_stats where teamcode='".$home_team_code."' and gamecode='$game_id' and season='$season'");
    $SelectTeamPrepareExe = $SelectTeam->execute();
    $CntTeam = $SelectTeam->rowCount();

    if ($CntTeam == 0) {

        $insertTeam = $conn->prepare("INSERT INTO individual_team_stats(gp, fgm, fga, fgm3, fga3, ftm, fta, tp, oreb, dreb, treb, pf, tf, ast, to1, blk, stl, min, Pts_bench, Pts_to, Pts_paint, Pts_fastb, Pts_ch2, W, L, teamcode, gamecode, season, month, year) VALUES (:gp, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min, :Pts_bench, :Pts_to, :Pts_paint, :Pts_fastb, :Pts_ch2, :W, :L, :teamcode, :gamecode, :season, :month, :year)");
        $insertTeamArr = array(":gp"=>$gethome_gp_total, ":fgm"=>$gethome_2ptmade_total, ":fga"=>$gethome_2ptatt_total, ":fgm3"=>$gethome_3fgm_total, ":fga3"=>$gethome_3fga_total, ":ftm"=>$gethome_ftm_total, ":fta"=>$gethome_fta_total, ":tp"=>$gethome_tp_total, ":oreb"=>$gethome_oreb_total, ":dreb"=>$gethome_dreb_total, ":treb"=>$gethome_treb_total, ":pf"=>$gethome_pf_total, ":tf"=>$gethome_tf_total, ":ast"=>$gethome_ast_total, ":to1"=>$gethome_to_total, ":blk"=>$gethome_blk_total, ":stl"=>$gethome_stl_total, ":min"=>$gethome_min_total, ":Pts_bench"=>$gethome_pts_bench, ":Pts_to"=>$gethome_pts_to, ":Pts_paint"=>$gethome_pts_paint, ":Pts_fastb"=>$gethome_pts_fastb, ":Pts_ch2"=>$gethome_pts_ch2, ":W"=>$gethome_pts_w, ":L"=>$gethome_pts_l, ":teamcode"=>$home_team_code, ":gamecode"=>$game_id, ":season"=>$season, ":month"=>$month, ":year"=>$year);
        $insertres = $insertTeam->execute($insertTeamArr);
        
    } else {            
               

        $homeTeamStatsUpdateQry = $conn->prepare("UPDATE individual_team_stats SET gp =:gp, fgm =:fgm, fga =:fga, fgm3 =:fgm3, fga3 =:fga3, ftm =:ftm, fta =:fta, tp =:tp, oreb =:oreb, dreb =:dreb, treb =:treb, pf =:pf, tf =:tf, ast =:ast, to1 =:to1, blk =:blk, stl =:stl, min =:min, Pts_bench =:Pts_bench, Pts_to =:Pts_to, Pts_paint =:Pts_paint, Pts_fastb =:Pts_fastb, Pts_ch2 =:Pts_ch2, W =:W, L =:L where teamcode=:teamcode and gamecode=:gamecode and season=:season");
        $homeTeamStatsUpdateQryArr = array(":gp"=>$gethome_gp_total, ":fgm"=>$gethome_2ptmade_total, ":fga"=>$gethome_2ptatt_total, ":fgm3"=>$gethome_3fgm_total, ":fga3"=>$gethome_3fga_total, ":ftm"=>$gethome_ftm_total, ":fta"=>$gethome_fta_total, ":tp"=>$gethome_tp_total, ":oreb"=>$gethome_oreb_total, ":dreb"=>$gethome_dreb_total, ":treb"=>$gethome_treb_total, ":pf"=>$gethome_pf_total, ":tf"=>$gethome_tf_total, ":ast"=>$gethome_ast_total, ":to1"=>$gethome_to_total, ":blk"=>$gethome_blk_total, ":stl"=>$gethome_stl_total, ":min"=>$gethome_min_total, ":Pts_bench"=>$gethome_pts_bench, ":Pts_to"=>$gethome_pts_to, ":Pts_paint"=>$gethome_pts_paint, ":Pts_fastb"=>$gethome_pts_fastb, ":Pts_ch2"=>$gethome_pts_ch2, ":W"=>$gethome_pts_w, ":L"=>$gethome_pts_l, ":teamcode"=>$home_team_code, ":gamecode"=>$game_id, ":season"=>$season);
        $homeTeamStatsUpdateExe = $homeTeamStatsUpdateQry->execute($homeTeamStatsUpdateQryArr);
        
        // if ($homeTeamStatsUpdateExe){
        //     $msgPopup = "Success";
        // }
    }

    $TeamStssqlQry = "SELECT SUM(gp) as gp,SUM(fgm) as fgm,SUM(fga) as fga,SUM(fgm3) as fgm3,SUM(fga3) as fga3, 
    SUM(ftm) as ftm,SUM(fta) as fta,SUM(tp) as tp,SUM(oreb) as oreb,SUM(dreb) as dreb,SUM(treb) as treb, 
    SUM(pf) as pf,SUM(tf) as tf,SUM(ast) as ast,SUM(to1) as to1, SUM(blk) as blk,SUM(stl) as stl,SUM(min) as min,SUM(Pts_bench) as Pts_bench,
    SUM(Pts_to) as Pts_to,SUM(Pts_paint) as Pts_paint,SUM(Pts_fastb) as Pts_fastb,SUM(Pts_ch2) as Pts_ch2,SUM(W) as W,SUM(L) as L
    FROM individual_team_stats where month = ".$month." and year = ".$year." and season = ".$season." and teamcode = ".$home_team_code;
    $sqldata = $conn->prepare($TeamStssqlQry);
    $sqldata->execute();
    if($sqldata->rowCount()>0){         
        //Check player&team&year&season already exists or not
        $fetchdata = $sqldata->fetch(PDO::FETCH_ASSOC);
        $gp=$fetchdata["gp"];           
        $fgm=$fetchdata["fgm"];
        $fga=$fetchdata["fga"];
        $fgm3=$fetchdata["fgm3"];
        $fga3=$fetchdata["fga3"];
        $ftm=$fetchdata["ftm"];
        $fta=$fetchdata["fta"];
        $tp=$fetchdata["tp"];
        $oreb=$fetchdata["oreb"];
        $dreb=$fetchdata["dreb"];
        $treb=$fetchdata["treb"];
        $pf=$fetchdata["pf"];
        $tf=$fetchdata["tf"];
        $ast=$fetchdata["ast"];
        $to1=$fetchdata["to1"];
        $blk=$fetchdata["blk"];
        $stl=$fetchdata["stl"];
        $min=$fetchdata["min"];
        $Pts_bench=$fetchdata["Pts_bench"];     
        $Pts_to=$fetchdata["Pts_to"];
        $Pts_paint=$fetchdata["Pts_paint"];
        $Pts_fastb=$fetchdata["Pts_fastb"];
        $Pts_ch2=$fetchdata["Pts_ch2"];
        $W=$fetchdata["W"];
        $L=$fetchdata["L"];

        $sqlQry2 = "SELECT * FROM team_stats_bb where month = ".$month." and year = ".$year." and season = ".$season." and teamcode = ".$home_team_code;
        $sqldata2 = $conn->prepare($sqlQry2);
        $sqldata2->execute();
        if ($sqldata2->rowCount()>0) {
            //Update team_stats_bb
            $updateqry="update team_stats_bb set gp ='".$gp."',fgm='".$fgm."',fga='".$fga."',fgm3='".$fgm3."',fga3='".$fga3."',
            ftm='".$ftm."',fta='".$fta."',tp='".$tp."',oreb='".$oreb."',dreb='".$dreb."',treb='".$treb."',pf='".$pf."',tf='".$tf."',
            ast='".$ast."',to1='".$to1."',blk='".$blk."',stl='".$stl."',min='".$min."',to1='".$to1."',Pts_bench='".$Pts_bench."',
            Pts_to='".$Pts_to."',Pts_paint='".$Pts_paint."',Pts_fastb='".$Pts_fastb."',Pts_ch2='".$Pts_ch2."',W='".$W."'
            ,L='".$L."' where month = ".$month." 
            and year = ".$year." and season = ".$season." and teamcode = ".$home_team_code;

            $PrepareQry = $conn->prepare($updateqry);
            $PrepareQry->execute(); 
        } else {
            //Insert team_stats_bb
            $insertqry="insert into team_stats_bb (customer_id,gp,fgm,fga,fgm3,fga3,ftm,fta,tp,oreb,dreb,treb,pf,tf,ast,to1,blk,stl,
                min,month,year,season,teamcode,Pts_bench,Pts_to,Pts_paint,Pts_fastb,Pts_ch2,W,L) 
            values ('".$cid."','".$gp."','".$fgm."','".$fga."','".$fgm3."','".$fga3."','".$ftm."','".$fta."','".$tp."','".$oreb."','".$dreb."',
                '".$treb."','".$pf."','".$tf."','".$ast."','".$to1."','".$blk."','".$stl."','".$min."','".$month."','".$year."',
                '".$season."','".$home_team_code."','".$Pts_bench."','".$Pts_to."','".$Pts_paint."','".$Pts_fastb."','".$Pts_ch2."','".$W."','".$L."')";            
            $PrepareQry = $conn->prepare($insertqry);
            $PrepareQry->execute(); 
        }
    }

    //Update team score and xmlname in gamedetails table
    $Qry = $conn->prepare("SELECT teamcode, tp FROM individual_team_stats WHERE teamcode IN (:visitor_team_code, :home_team_code) AND gamecode=:game_id AND season=:game_season");
    $QryArr = array(":visitor_team_code"=>$visitor_team_code, ":home_team_code"=>$home_team_code, ":game_id"=>$game_id, ":game_season"=>$game_season);
    $Qry->execute($QryArr);
    $CntRcds = $Qry->rowCount();
    $TeamScoreArr = "";
    if ($CntRcds > 0) {
        $FetchTeamScores = $Qry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($FetchTeamScores as $TeamScorerows) {
            $TeamScoreArr[$TeamScorerows["teamcode"]] = $TeamScorerows["tp"];
        }
        $UpdateGdetailQry = $conn->prepare("UPDATE game_details SET vscore=:vscore, hscore=:hscore, xml_name=:xml_name WHERE id=:gamecode");
        $UpdateGdetailQryArr = array(":vscore"=>$TeamScoreArr[$visitor_team_code], ":hscore"=>$TeamScoreArr[$home_team_code], ":xml_name"=>$xml_name, ":gamecode"=>$game_id);
        $UpdateGdetailQry->execute($UpdateGdetailQryArr);

        header('Location:game_list.php?msg=5');
        exit;
        // if ($homeTeamStatsUpdateExe){
            // $msgPopup = "Success";
        // }
    }
}



include_once('header.php'); ?>
<link href="assets/custom/css/gamestats.css" rel="stylesheet" type="text/css" />
<?php 
$teamloginId = "";
if ($_SESSION["usertype"] == "team_manager") {    
    $teamloginId = $_SESSION["team_manager_id"];
}
?>
    <input type="hidden" id="teamloginid" value="<?php echo $teamloginId; ?>">
    <div class="page-content-wrapper">
        <div class="page-content">            
            <div class="row match_statspage">
                <form id="match_statsform" method="POST"  enctype="multipart/form-data">
                    <div class="col-md-12 left-right-padding">
                        <div class="col-md-12 ">
                            <?php if ($msgPopup) { ?>
                                <div class="alert alert-success fade in popupstyle">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                    <strong>Games data updated successfully!</strong> 
                                </div>
                            <?php } ?>
                            <div class="portlet light info-caption pagetitle">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"> Match Stats</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 left-padding">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <div class="teamarea_<?php echo $visitor_team_code;?>">
                                        <div class="form-body bottom-padding mangeteam-form-bottom">
                                            <table class="table table-hover table-striped table-bordered table-highlight-head text-center team-details">
                                                <tr>
                                                    <th>Visitor</th>
                                                    <th><?php echo $visitor_team_name; ?></th>
                                                </tr>
                                            </table>                  
                                        </div>
                                        
                                        <div class="visitor-team-add-player">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 hidden-xs"></div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <a><button type="button" class="btn btn-dark addexistsplayer " hiddengameid="<?php echo $home_gameid; ?>" hiddenteamtype="visitorteam"  hiddenteamid="<?php echo $visitor_team_code;?>"  >Assign player</button></a>
                                                    <a><button type="button" class="btn btn-dark addplayer" data-toggle="modal" data-target="#addplayermodal" game-id="<?php echo $home_gameid; ?>" data-name="visitorteam" teamname="<?php echo $visitor_team_name ;?>" data-id="<?php echo $visitor_team_code; ?>">Add new player</button></a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="table-responsive matchstats_membertbl">                         
                                            <table class="table table-hover table-striped table-bordered table-highlight-head text-center search-view matchstats_tble" id="search_result">                              
                                                <tr>
                                                    <th nowrap>#</th>                                             
                                                    <th>gp</th>
                                                    <th>gs</th>
                                                    <th>fgm</th>
                                                    <th>fga</th>                    
                                                    <th>3fgm</th>
                                                    <th>3fga</th>
                                                    <th>ftm</th>
                                                    <th>fta</th>
                                                    <th>tp</th>
                                                    <th>oreb</th>
                                                    <th>dreb</th>
                                                    <th>treb</th>
                                                    <th>pf</th>
                                                    <th>tf</th>
                                                    <th>ast</th>
                                                    <th>to</th>
                                                    <th>blk</th>
                                                    <th>stl</th>
                                                    <th>min</th>                                
                                                </tr>
                                                <?php

                                                    $visitorQry = "SELECT * FROM individual_player_stats where gamecode = ".$home_gameid." and teamcode = ".$visitor_team_code;
                                                    $visitor_data = $conn->prepare($visitorQry);
                                                    $visitor_data->execute();
                                                    $get_visit_rowCount = $visitor_data->rowCount();
                                                    if ($get_visit_rowCount > 0){
                                                        $fetch_Visitdata = $visitor_data->fetchAll(PDO::FETCH_ASSOC);                   
                                                        $sno = 1;                   
                                                        foreach ($fetch_Visitdata as $fetchvisitvalue) {
															//$visitor_id = $fetchvisitvalue['id'];
															$visitor_id = $fetchvisitvalue['playercode'];
															$visitor_memberName = $fetchvisitvalue['checkname'];
															$visitor_gp = $fetchvisitvalue['gp'];
															$visitor_gs = $fetchvisitvalue['gs'];
															$visitor_2ptmade = $fetchvisitvalue['fgm'];
															$visitor_2ptatt = $fetchvisitvalue['fga'];
															$visitor_3fgm = $fetchvisitvalue['fgm3'];
															$visitor_3fga = $fetchvisitvalue['fga3'];
															$visitor_ftm = $fetchvisitvalue['ftm'];
															$visitor_fta = $fetchvisitvalue['fta'];
															$visitor_tp = $fetchvisitvalue['tp'];
															$visitor_oreb = $fetchvisitvalue['oreb'];
															$visitor_dreb = $fetchvisitvalue['dreb'];
															$visitor_treb = $fetchvisitvalue['treb'];
															$visitor_pf = $fetchvisitvalue['pf'];
															$visitor_tf = $fetchvisitvalue['tf'];
															$visitor_ast = $fetchvisitvalue['ast'];
															$visitor_to = $fetchvisitvalue['to1'];
															$visitor_blk = $fetchvisitvalue['blk'];
															$visitor_stl = $fetchvisitvalue['stl'];
															$visitor_min = $fetchvisitvalue['min'];
                                                            ?>
                                                            <input type="hidden" name="visitor_id[]" value="<?php echo $visitor_id;?>">
                                                            <tr>
                                                                <td nowrap><?php echo ucwords($visitor_memberName); ?></td>                                                        
                                                                <td><input type="text" class="visitor_gp" data-val='int' name="visitor_gp[<?php echo $visitor_id;?>]" value="<?php echo $visitor_gp;?>"></td>                           
                                                                <td><input type="text" class="visitor_gs" data-val='int' name="visitor_gs[<?php echo $visitor_id;?>]" value="<?php echo $visitor_gs;?>"></td>                           
                                                                <td><input type="text" class="visitor_2ptmade" data-val='int' name="visitor_2ptmade[<?php echo $visitor_id;?>]" value="<?php echo $visitor_2ptmade;?>"></td>                            
                                                                <td><input type="text" class="visitor_2ptatt" data-val='int' name="visitor_2ptatt[<?php echo $visitor_id;?>]" value="<?php echo $visitor_2ptatt;?>"></td>                                                                                   
                                                                <td><input type="text" class="visitor_3fgm" data-val='int' name="visitor_3fgm[<?php echo $visitor_id;?>]" value="<?php echo $visitor_3fgm;?>"></td>                         
                                                                <td><input type="text" class="visitor_3fga" data-val='int' name="visitor_3fga[<?php echo $visitor_id;?>]" value="<?php echo $visitor_3fga;?>"></td>                         
                                                                <td><input type="text" class="visitor_ftm" data-val='int' name="visitor_ftm[<?php echo $visitor_id;?>]" value="<?php echo $visitor_ftm;?>"></td>                            
                                                                <td><input type="text" class="visitor_fta" data-val='int' name="visitor_fta[<?php echo $visitor_id;?>]" value="<?php echo $visitor_fta;?>"></td>                            
                                                                <td><input type="text" class="visitor_tp" data-val='int' name="visitor_tp[<?php echo $visitor_id;?>]" value="<?php echo $visitor_tp;?>"></td>                           
                                                                <td><input type="text" class="visitor_oreb" data-val='int' name="visitor_oreb[<?php echo $visitor_id;?>]" value="<?php echo $visitor_oreb;?>"></td>                         
                                                                <td><input type="text" class="visitor_dreb" data-val='int' name="visitor_dreb[<?php echo $visitor_id;?>]" value="<?php echo $visitor_dreb;?>"></td>                         
                                                                <td><input type="text" class="visitor_treb" data-val='int' name="visitor_treb[<?php echo $visitor_id;?>]" value="<?php echo $visitor_treb;?>"></td>                         
                                                                <td><input type="text" class="visitor_pf" data-val='int' name="visitor_pf[<?php echo $visitor_id;?>]" value="<?php echo $visitor_pf;?>"></td>                                   
                                                                <td><input type="text" class="visitor_tf" data-val='int' name="visitor_tf[<?php echo $visitor_id;?>]" value="<?php echo $visitor_tf;?>"></td>                       
                                                                <td><input type="text" class="visitor_ast" data-val='int' name="visitor_ast[<?php echo $visitor_id;?>]" value="<?php echo $visitor_ast;?>"></td>                            
                                                                <td><input type="text" class="visitor_to" data-val='int' name="visitor_to[<?php echo $visitor_id;?>]" value="<?php echo $visitor_to;?>"></td>                           
                                                                <td><input type="text" class="visitor_blk" data-val='int' name="visitor_blk[<?php echo $visitor_id;?>]" value="<?php echo $visitor_blk;?>"></td>                            
                                                                <td><input type="text" class="visitor_stl" data-val='int' name="visitor_stl[<?php echo $visitor_id;?>]" value="<?php echo $visitor_stl;?>"></td>                            
                                                                <td><input type="text" class="visitor_min" data-val='int' name="visitor_min[<?php echo $visitor_id;?>]" value="<?php echo $visitor_min;?>"></td>                                                                                                            
                                                            </tr>                                               
                                                        <?php 
                                                            $sno++;
                                                        }
                                                    ?>                  
                                                    <?php                           
                                                    } else {
                                                        // echo "<td colspan='22' class='nogames'>No Team Members found</td>";
                                                        $visitorQry = "SELECT * FROM player_info where team_id = ".$visitor_team_code;
                                                        $visitor_data = $conn->prepare($visitorQry);
                                                        $visitor_data->execute();
                                                        $get_visit_rowCount = $visitor_data->rowCount();
                                                        if ($get_visit_rowCount > 0) {
                                                            $fetch_Visitdata = $visitor_data->fetchAll(PDO::FETCH_ASSOC);

                                                            $sno = 1;                   
                                                            foreach ($fetch_Visitdata as $fetchvisitvalue) {
                                                                $visitor_player_id = $fetchvisitvalue['id'];
                                                                $visitor_memberName = $fetchvisitvalue['lastname'].", ".$fetchvisitvalue['firstname'];                              
                                                                ?>
                                                                <input type="hidden" name="visitor_id[]" value="<?php echo $visitor_player_id;?>">
                                                                <tr>
                                                                    <td nowrap><?php echo ucwords($visitor_memberName); ?></td>                                                        
                                                                    <td><input type="text" class="visitor_gp" data-val='int' name="visitor_gp[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_gs" data-val='int' name="visitor_gs[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_2ptmade" data-val='int' name="visitor_2ptmade[<?php echo $visitor_player_id;?>]" value=""></td>                            
                                                                    <td><input type="text" class="visitor_2ptatt" data-val='int' name="visitor_2ptatt[<?php echo $visitor_player_id;?>]" value=""></td>               
                                                                    <td><input type="text" class="visitor_3fgm" data-val='int' name="visitor_3fgm[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_3fga" data-val='int' name="visitor_3fga[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_ftm" data-val='int' name="visitor_ftm[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_fta" data-val='int' name="visitor_fta[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_tp" data-val='int' name="visitor_tp[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_oreb" data-val='int' name="visitor_oreb[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_dreb" data-val='int' name="visitor_dreb[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_treb" data-val='int' name="visitor_treb[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_pf" data-val='int' name="visitor_pf[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_tf" data-val='int' name="visitor_tf[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_ast" data-val='int' name="visitor_ast[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_to" data-val='int' name="visitor_to[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_blk" data-val='int' name="visitor_blk[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_stl" data-val='int' name="visitor_stl[<?php echo $visitor_player_id;?>]" value=""></td>
                                                                    <td><input type="text" class="visitor_min" data-val='int' name="visitor_min[<?php echo $visitor_player_id;?>]" value=""></td>                                          
                                                                </tr>                                               
                                                            <?php 
                                                                $sno++;
                                                            }
                                                        } else {
                                                            echo "<td colspan='22' class='nogames'>No Team Members found</td>";
                                                        }
                                                    }
                                                if ($get_visit_rowCount > 0){               
                                                ?>
                                                <tr class="tot-row">
                                                    <td>Team Total</td>                                             
                                                    <td><input type="text" id="visitor_gp_total" name="visitor_gp_total[]" value="1" readonly></td>                         
                                                    <td></td>                           
                                                    <td><input type="text" id="visitor_2ptmade_total" name="visitor_2ptmade_total" readonly></td>                           
                                                    <td><input type="text" id="visitor_2ptatt_total" name="visitor_2ptatt_total" readonly></td>                                             
                                                    <td><input type="text" id="visitor_3fgm_total" name="visitor_3fgm_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_3fga_total" name="visitor_3fga_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_ftm_total" name="visitor_ftm_total" readonly></td>                           
                                                    <td><input type="text" id="visitor_fta_total" name="visitor_fta_total" readonly></td>                           
                                                    <td><input type="text" id="visitor_tp_total" name="visitor_tp_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_oreb_total" name="visitor_oreb_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_dreb_total" name="visitor_dreb_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_treb_total" name="visitor_treb_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_pf_total" name="visitor_pf_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_tf_total" name="visitor_tf_total" readonly></td>                                 
                                                    <td><input type="text" id="visitor_ast_total" name="visitor_ast_total" readonly></td>                           
                                                    <td><input type="text" id="visitor_to_total" name="visitor_to_total" readonly></td>                         
                                                    <td><input type="text" id="visitor_blk_total" name="visitor_blk_total" readonly></td>                           
                                                    <td><input type="text" id="visitor_stl_total" name="visitor_stl_total" readonly></td>                           
                                                    <td><input type="text" id="visitor_min_total" name="visitor_min_total" readonly></td>                                                                                                                               
                                                </tr>
                                                <?php } ?>      
                                            </table>                                            
                                        </div>
                                        <?php if ($fetch_Visitdata) { ?>        
                                            <div class="table-responsive matchstats_membertbl">
                                                <table class="table table-hover table-striped table-bordered table-highlight-head text-center search-view team-pts-stats">
                                                    <tr>
                                                        <th>w</th>
                                                        <th>l</th>
                                                        <th>pts bench</th>
                                                        <th>pts to</th>
                                                        <th>pts paint</th>
                                                        <th>pts fastb</th>
                                                        <th>pts ch2</th>                    
                                                    </tr>
                                                    <?php
                                                    $visitorTeamStatsrQry = "SELECT * FROM individual_team_stats where gamecode = ".$home_gameid." and teamcode = ".$visitor_team_code;
                                                    $visitTeamPrepare = $conn->prepare($visitorTeamStatsrQry);
                                                    $visitTeamPrepare->execute();
                                                    $get_visitTeam_rowCount = $visitTeamPrepare->rowCount();
                                                    if ($get_visitTeam_rowCount > 0) {
                                                        $fetch_VisitTeamStats = $visitTeamPrepare->fetchAll(PDO::FETCH_ASSOC);
                                                            $sno = 1;
                                                            foreach ($fetch_VisitTeamStats as $visitorTeamStatsVal){
                                                                $visitorPts_bench = $visitorTeamStatsVal['Pts_bench'];
                                                                $visitorPts_to = $visitorTeamStatsVal['Pts_to'];
                                                                $visitorPts_paint = $visitorTeamStatsVal['Pts_paint'];
                                                                $visitorPts_fastb = $visitorTeamStatsVal['Pts_fastb'];
                                                                $visitorPts_ch2 = $visitorTeamStatsVal['Pts_ch2'];
                                                                $visitorPts_W = $visitorTeamStatsVal['W'];
                                                                $visitorPts_L = $visitorTeamStatsVal['L'];
                                                                ?>
                                                                <tr>
                                                                    <td><input type="text" class="visitor_pts_w" name="visitor_pts_w" data-val='int' maxlength="1" value="<?php echo $visitorPts_W;?>"></td>                                                        
                                                                    <td><input type="text" class="visitor_pts_l" name="visitor_pts_l" data-val='int' value="<?php echo $visitorPts_L;?>" readonly ></td>                                                                                    
                                                                    <td><input type="text" class="visitor_pts_bench" name="visitor_pts_bench" data-val='int' value="<?php echo $visitorPts_bench;?>"></td>                          
                                                                    <td><input type="text" class="visitor_pts_to" name="visitor_pts_to" data-val='int' value="<?php echo $visitorPts_to;?>"></td>                           
                                                                    <td><input type="text" class="visitor_pts_paint" name="visitor_pts_paint" data-val='int' value="<?php echo $visitorPts_paint;?>"></td>                          
                                                                    <td><input type="text" class="visitor_pts_fastb" name="visitor_pts_fastb" data-val='int' value="<?php echo $visitorPts_fastb;?>"></td>                                                                                  
                                                                    <td><input type="text" class="visitor_pts_ch2" name="visitor_pts_ch2" data-val='int' value="<?php echo $visitorPts_ch2;?>"></td>
                                                                </tr>
                                                                <?php 
                                                                        $sno++;
                                                                    }
                                                                ?>
                                                        <?php                   
                                                        } else { 
                                                            // echo "<td colspan='7' class='nogames'>No Team Stats found</td>";
                                                            ?>
                                                            <tr>
                                                                <td><input type="text" class="visitor_pts_w" name="visitor_pts_w" data-val='int' maxlength="1" value=""></td>
                                                                <td><input type="text" class="visitor_pts_l" name="visitor_pts_l" data-val='int' value="" readonly ></td>                  
                                                                <td><input type="text" class="visitor_pts_bench" name="visitor_pts_bench" data-val='int' value=""></td>
                                                                <td><input type="text" class="visitor_pts_to" name="visitor_pts_to" data-val='int' value=""></td>
                                                                <td><input type="text" class="visitor_pts_paint" name="visitor_pts_paint" data-val='int' value=""></td>
                                                                <td><input type="text" class="visitor_pts_fastb" name="visitor_pts_fastb" data-val='int' value=""></td>
                                                                <td><input type="text" class="visitor_pts_ch2" name="visitor_pts_ch2" data-val='int' value=""></td>
                                                            </tr>
                                                        <?php }
                                                        ?>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="teamarea_<?php echo $home_team_code;?>">
                                        <div>           
                                            <table class="table table-hover table-striped table-bordered table-highlight-head text-center team-details">
                                                <tr>
                                                    <th>Home</th>
                                                    <th><?php echo $home_team_name ;?></th>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="home-team-add-player">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8 hidden-xs"></div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <a><button type="button" class="btn btn-dark addexistsplayer" hiddengameid="<?php echo $home_gameid; ?>" hiddenteamtype="hometeam"  hiddenteamid="<?php echo $home_team_code;?>"  >Assign player</button></a>
                                                    <a><button type="button" class="btn btn-dark addplayer" data-toggle="modal" data-target="#addplayermodal" data-name="hometeam" game-id="<?php echo $home_gameid; ?>" teamname="<?php echo $home_team_name ;?>" data-id="<?php echo $home_team_code; ?>">Add new player</button></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive matchstats_membertbl">                 
                                            <table class="table table-hover table-striped table-bordered table-highlight-head text-center search-view matchstats_tble" id="search_result_home">                                
                                                <tr>
                                                    <th nowrap>#</th>                                                          
                                                    <th>gp</th>
                                                    <th>gs</th>
                                                    <th>fgm</th>                    
                                                    <th>fga</th>
                                                    <th>3fgm</th>
                                                    <th>3fga</th>
                                                    <th>ftm</th>
                                                    <th>fta</th>
                                                    <th>tp</th>
                                                    <th>oreb</th>
                                                    <th>dreb</th>
                                                    <th>treb</th>
                                                    <th>pf</th>
                                                    <th>tf</th>
                                                    <th>ast</th>
                                                    <th>to</th>
                                                    <th>blk</th>
                                                    <th>stl</th>
                                                    <th>min</th>                                
                                                </tr>
                                                <?php                   
                                                    $homeQry = "SELECT * FROM individual_player_stats where gamecode = ".$home_gameid." and teamcode = ".$home_team_code;
                                                    $home_data = $conn->prepare($homeQry);
                                                    $home_data->execute();
                                                    $get_home_rowCount = $home_data->rowCount();
                                                    if ($get_home_rowCount > 0) {
                                                        $fetch_Hmedata = $home_data->fetchAll(PDO::FETCH_ASSOC);                    
                                                        $sno = 1;                   
                                                        foreach ($fetch_Hmedata as $fetchhomevalue) {
                                                            // $home_id = $fetchhomevalue['id'];
                                                            $home_id = $fetchhomevalue['playercode'];                          
                                                            $home_memberName = $fetchhomevalue['checkname'];
                                                            $home_gp = $fetchhomevalue['gp'];
                                                            $home_gs = $fetchhomevalue['gs'];
                                                            $home_2ptmade = $fetchhomevalue['fgm'];
                                                            $home_2ptatt = $fetchhomevalue['fga'];
                                                            $home_3fgm = $fetchhomevalue['fgm3'];
                                                            $home_3fga = $fetchhomevalue['fga3'];
                                                            $home_ftm = $fetchhomevalue['ftm'];
                                                            $home_fta = $fetchhomevalue['fta'];
                                                            $home_tp = $fetchhomevalue['tp'];
                                                            $home_oreb = $fetchhomevalue['oreb'];
                                                            $home_dreb = $fetchhomevalue['dreb'];
                                                            $home_treb = $fetchhomevalue['treb'];
                                                            $home_pf = $fetchhomevalue['pf'];
                                                            $home_tf = $fetchhomevalue['tf'];
                                                            $home_ast = $fetchhomevalue['ast'];
                                                            $home_to = $fetchhomevalue['to1'];
                                                            $home_blk = $fetchhomevalue['blk'];
                                                            $home_stl = $fetchhomevalue['stl'];
                                                            $home_min = $fetchhomevalue['min'];
                                                            ?>                          
                                                            <tr>
                                                                <td nowrap><input type="hidden" name="home_id[]" value="<?php echo $home_id;?>"><?php echo ucwords($home_memberName); ?></td>                                                          
                                                                <td><input type="text" class="home_gp" name="home_gp[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_gp;?>"></td>                           
                                                                <td><input type="text" class="home_gs" name="home_gs[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_gs;?>"></td>                           
                                                                <td><input type="text" class="home_2ptmade" name="home_2ptmade[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_2ptmade;?>"></td>                            
                                                                <td><input type="text" class="home_2ptatt" name="home_2ptatt[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_2ptatt;?>"></td>                                                       
                                                                <td><input type="text" class="home_3fgm" name="home_3fgm[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_3fgm;?>"></td>                         
                                                                <td><input type="text" class="home_3fga" name="home_3fga[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_3fga;?>"></td>                         
                                                                <td><input type="text" class="home_ftm" name="home_ftm[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_ftm;?>"></td>                            
                                                                <td><input type="text" class="home_fta" name="home_fta[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_fta;?>"></td>                            
                                                                <td><input type="text" class="home_tp" name="home_tp[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_tp;?>"></td>                           
                                                                <td><input type="text" class="home_oreb" name="home_oreb[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_oreb;?>"></td>                         
                                                                <td><input type="text" class="home_dreb" name="home_dreb[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_dreb;?>"></td>                         
                                                                <td><input type="text" class="home_treb" name="home_treb[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_treb;?>"></td>                         
                                                                <td><input type="text" class="home_pf" name="home_pf[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_pf;?>"></td>                                   
                                                                <td><input type="text" class="home_tf" name="home_tf[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_tf;?>"></td>                       
                                                                <td><input type="text" class="home_ast" name="home_ast[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_ast;?>"></td>                            
                                                                <td><input type="text" class="home_to" name="home_to[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_to;?>"></td>                           
                                                                <td><input type="text" class="home_blk" name="home_blk[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_blk;?>"></td>                            
                                                                <td><input type="text" class="home_stl" name="home_stl[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_stl;?>"></td>                            
                                                                <td><input type="text" class="home_min" name="home_min[<?php echo $home_id;?>]" data-val='int' value="<?php echo $home_min;?>"></td>                                                                                                            
                                                            </tr>                                               
                                                        <?php 
                                                            $sno++;
                                                        }
                                                    ?>                  
                                                    <?php                           
                                                    } else {
                                                        // echo "<td colspan='22' class='nogames'>No Team Members found</td>";
                                                        $homeQry = "SELECT * FROM player_info where team_id = ".$home_team_code;
                                                        $home_data = $conn->prepare($homeQry);
                                                        $home_data->execute();
                                                        $get_home_rowCount = $home_data->rowCount();
                                                        if ($get_home_rowCount > 0) {
                                                            $fetch_Hmedata = $home_data->fetchAll(PDO::FETCH_ASSOC);                    
                                                            $sno = 1;                   
                                                            foreach ($fetch_Hmedata as $fetchhomevalue) {
                                                                $home_player_id = $fetchhomevalue['id'];                         
                                                                $home_memberName = $fetchhomevalue['lastname'].", ".$fetchhomevalue['firstname'];
                                                                
                                                                ?>                          
                                                                <tr>
                                                                    <td nowrap><input type="hidden" name="home_id[]" value="<?php echo $home_player_id;?>"><?php echo ucwords($home_memberName); ?></td>                                             
                                                                    <td><input type="text" class="home_gp" name="home_gp[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_gs" name="home_gs[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_2ptmade" name="home_2ptmade[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                            
                                                                    <td><input type="text" class="home_2ptatt" name="home_2ptatt[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                                                       
                                                                    <td><input type="text" class="home_3fgm" name="home_3fgm[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                         
                                                                    <td><input type="text" class="home_3fga" name="home_3fga[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                         
                                                                    <td><input type="text" class="home_ftm" name="home_ftm[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                            
                                                                    <td><input type="text" class="home_fta" name="home_fta[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                            
                                                                    <td><input type="text" class="home_tp" name="home_tp[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                           
                                                                    <td><input type="text" class="home_oreb" name="home_oreb[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                         
                                                                    <td><input type="text" class="home_dreb" name="home_dreb[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                         
                                                                    <td><input type="text" class="home_treb" name="home_treb[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                         
                                                                    <td><input type="text" class="home_pf" name="home_pf[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_tf" name="home_tf[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_ast" name="home_ast[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_to" name="home_to[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_blk" name="home_blk[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_stl" name="home_stl[<?php echo $home_player_id;?>]" data-val='int' value=""></td>
                                                                    <td><input type="text" class="home_min" name="home_min[<?php echo $home_player_id;?>]" data-val='int' value=""></td>                                                                                                            
                                                                </tr>                                               
                                                            <?php 
                                                                $sno++;
                                                            }
                                                        } else {
                                                            echo "<td colspan='22' class='nogames'>No Team Members found</td>";
                                                        }
                                                    }
                                                if ($get_home_rowCount > 0) {                                       
                                                ?>
                                                <tr class="tot-row">
                                                    <td>Team Total</td>                                         
                                                    <td><input type="text" id="home_gp_total" name="home_gp_total[]" value="1" readonly></td>                           
                                                    <td></td>                           
                                                    <td><input type="text" id="home_2ptmade_total" name="home_2ptmade_total" readonly></td>                         
                                                    <td><input type="text" id="home_2ptatt_total" name="home_2ptatt_total" readonly></td>                                           
                                                    <td><input type="text" id="home_3fgm_total" name="home_3fgm_total" readonly></td>                           
                                                    <td><input type="text" id="home_3fga_total" name="home_3fga_total" readonly></td>                           
                                                    <td><input type="text" id="home_ftm_total" name="home_ftm_total" readonly></td>                         
                                                    <td><input type="text" id="home_fta_total" name="home_fta_total" readonly></td>                         
                                                    <td><input type="text" id="home_tp_total" name="home_tp_total" readonly></td>                           
                                                    <td><input type="text" id="home_oreb_total" name="home_oreb_total" readonly></td>                           
                                                    <td><input type="text" id="home_dreb_total" name="home_dreb_total" readonly></td>                           
                                                    <td><input type="text" id="home_treb_total" name="home_treb_total" readonly></td>                           
                                                    <td><input type="text" id="home_pf_total" name="home_pf_total" readonly></td>                           
                                                    <td><input type="text" id="home_tf_total" name="home_tf_total" readonly></td>                                   
                                                    <td><input type="text" id="home_ast_total" name="home_ast_total" readonly></td>                         
                                                    <td><input type="text" id="home_to_total" name="home_to_total" readonly></td>                           
                                                    <td><input type="text" id="home_blk_total" name="home_blk_total" readonly></td>                         
                                                    <td><input type="text" id="home_stl_total" name="home_stl_total" readonly></td>                         
                                                    <td><input type="text" id="home_min_total" name="home_min_total" readonly></td>                                                                                                         
                                                </tr>
                                                <?php } ?>      
                                            </table>                                                
                                        </div>
                                        <?php if ($fetch_Visitdata) { ?>
                                            <div class="table-responsive matchstats_membertbl">
                                                <table class="table table-hover table-striped table-bordered table-highlight-head text-center search-view team-pts-stats">
                                                    <tr>
                                                        <th>w</th>
                                                        <th>l</th>
                                                        <th>pts bench</th>
                                                        <th>pts to</th>
                                                        <th>pts paint</th>
                                                        <th>pts fastb</th>
                                                        <th>pts ch2</th>                    
                                                    </tr>
                                                    <?php
                                                    $homeTeamStatsrQry = "SELECT * FROM individual_team_stats where gamecode = ".$home_gameid." and teamcode = ".$home_team_code;
                                                    $visitTeamPrepare = $conn->prepare($homeTeamStatsrQry);
                                                    $visitTeamPrepare->execute();
                                                    $get_visitTeam_rowCount = $visitTeamPrepare->rowCount();
                                                    if ($get_visitTeam_rowCount > 0) {
                                                        $fetch_VisitTeamStats = $visitTeamPrepare->fetchAll(PDO::FETCH_ASSOC);
                                                        $sno = 1;
                                                        foreach ($fetch_VisitTeamStats as $homeTeamStatsVal){
                                                            $homePts_bench = $homeTeamStatsVal['Pts_bench'];
                                                            $homePts_to = $homeTeamStatsVal['Pts_to'];
                                                            $homePts_paint = $homeTeamStatsVal['Pts_paint'];
                                                            $homePts_fastb = $homeTeamStatsVal['Pts_fastb'];
                                                            $homePts_ch2 = $homeTeamStatsVal['Pts_ch2'];
                                                            $homePts_W = $homeTeamStatsVal['W'];
                                                            $homePts_L = $homeTeamStatsVal['L'];
                                                            ?>
                                                            <tr>
                                                                <td><input type="text" class="home_pts_w" name="home_pts_w" data-val='int' value="<?php echo $homePts_W;?>" readonly ></td>                                                     
                                                                <td><input type="text" class="home_pts_l" name="home_pts_l" data-val='int' value="<?php echo $homePts_L;?>" readonly ></td>                                                                                 
                                                                <td><input type="text" class="home_pts_bench" name="home_pts_bench" data-val='int' value="<?php echo $homePts_bench;?>"></td>                           
                                                                <td><input type="text" class="home_pts_to" name="home_pts_to" data-val='int' value="<?php echo $homePts_to;?>"></td>                            
                                                                <td><input type="text" class="home_pts_paint" name="home_pts_paint" data-val='int' value="<?php echo $homePts_paint;?>"></td>                           
                                                                <td><input type="text" class="home_pts_fastb" name="home_pts_fastb" data-val='int' value="<?php echo $homePts_fastb;?>"></td>                                                                                   
                                                                <td><input type="text" class="home_pts_ch2" name="home_pts_ch2" data-val='int' value="<?php echo $homePts_ch2;?>"></td>                     
                                                            </tr>
                                                            <?php 
                                                                    $sno++;
                                                                }
                                                            ?>
                                                    <?php                   
                                                    } else {
                                                        // echo "<td colspan='7' class='nogames'>No Team Stats found</td>";
                                                        ?>
                                                        <tr>
                                                            <td><input type="text" class="home_pts_w" name="home_pts_w" data-val='int' value="" readonly ></td>                      
                                                            <td><input type="text" class="home_pts_l" name="home_pts_l" data-val='int' value="" readonly ></td>            
                                                            <td><input type="text" class="home_pts_bench" name="home_pts_bench" data-val='int' value=""></td>                           
                                                            <td><input type="text" class="home_pts_to" name="home_pts_to" data-val='int' value=""></td>                            
                                                            <td><input type="text" class="home_pts_paint" name="home_pts_paint" data-val='int' value=""></td>                           
                                                            <td><input type="text" class="home_pts_fastb" name="home_pts_fastb" data-val='int' value=""></td>              
                                                            <td><input type="text" class="home_pts_ch2" name="home_pts_ch2" data-val='int' value=""></td>                     
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="modify_game matchstats_buttons">
                                        <input type="submit" class="btn btn-success customgreenbtn"  value="modify game data" >
                                        <a href="game_list.php"><button type="button" class="btn customredbtn">Back</button></a>
                                    </div>
                                </div>
                            </div>                    
                        </div>                                               
                    </div>                    
                </form>
            </div>            
        </div>
    </div>
</div>
<div class="modal fade" id="addExistsPlayerModal" role="dialog">
    <?php include_once("add_exists_player.php");?>
</div>
<?php 
$SportQry = $conn->prepare("SELECT subsports.sport_id,sports.sport_name FROM customer_subscribed_sports as subsports left join sports on subsports.sport_id=sports.sportcode where customer_id=:customer_id");
$SportQryArr = array(":customer_id"=>$cid);
$SportQry->execute($SportQryArr);
$FetchSportName = $SportQry->fetch(PDO::FETCH_ASSOC);
$sportname = $FetchSportName["sport_name"];

?>
<div class="modal fade" id="addplayermodal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content gamestats-modal-content">            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add new player :: <span class="teamtitle"></span></h4>
            </div>
            <form id="gamestatsaddplayer" method="POST">
                <input type="hidden" name="hiddengameid" id="hiddengameid" >
                <input type="hidden" name="hiddenteamid" id="hiddenteamid" >
                <input type="hidden" name="hiddenteamname" id="hiddenteamname" >
                <input type="hidden" name="hiddenteamtype" id="hiddenteamtype" >
                <div class="modal-body gameformbody">
                    <div class="">
                        <div class="playerexist">                            
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 bothpaddingremove">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>First Name <span class="error">*</span></label>
                                        <input class="form-control " type="text" name="firstname" id="firstname" placeholder="First Name" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Last Name <span class="error">*</span></label>
                                        <input class="form-control " type="text" name="lastname" id="lastname" placeholder="Last Name" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- <div class="form-group">
                            <label>First Name <span class="error">*</span></label>
                            <input class="form-control " type="text" name="firstname" id="firstname" placeholder="First Name" value="" aria-required="true" aria-invalid="true">
                        </div>
                        <div class="form-group">
                            <label>Last Name <span class="error">*</span></label>
                            <input class="form-control " type="text" name="lastname" id="lastname" placeholder="Last Name" value="" aria-required="true" aria-invalid="true">
                        </div> -->

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 bothpaddingremove">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Uniform No <span class="error">*</span></label>
                                        <input class="form-control " type="text" name="uniformno" id="uniformno" placeholder="Uniform No" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <select class="form-control" name="position" id="position">

                                            <option value="">Select</option>
                                            <?php if($sportname == 'basketball'){ ?>
                                            <option value="G">G</option>
                                            <option value="C">C</option>
                                            <option value="F">F</option>
                                            <option value="SG">SG</option>
                                            <option value="GF">GF</option>
                                            <option value="PG">PG</option>
                                            <option value="PF">PF</option>
                                            <option value="SF">SF</option> <?php } ?>
                                            <?php if($sportname == 'baseball' || $sportname == 'softball'){ ?>
                                            <option value="Extra fldr">Extra fldr</option>
                                            <option value="Pitcher">Pitcher</option>
                                            <option value="Catcher">Catcher</option>
                                            <option value="First Base">First Base</option>
                                            <option value="Second Base">Second Base</option>
                                            <option value="Third Base">Third Base</option>
                                            <option value="Shortstop">Shortstop</option>
                                            <option value="Left Field">Left Field</option>
                                            <option value="Center Field">Center Field</option>
                                            <option value="Right Field">Right Field</option>
                                            <option value="Des Hitter">Des Hitter</option>
                                            <option value="Des Player">Des Player</option>
                                            <option value="Pinch Hit">Pinch Hit</option>
                                            <option value="Pinch Run">Pinch Run</option>
                                            <option value="Infielder">Infielder</option>
                                            <option value="Outfielder">Outfielder</option>
                                            <option value="Utility">Utility</option>
                                            <?php } ?>
                                            <?php if($sportname == 'football'){ ?>
                                            <option value="C">C</option>
                                            <option value="C/G">C/G</option>
                                            <option value="CB">CB</option>
                                            <option value="DB">DB</option>
                                            <option value="DE">DE</option>
                                            <option value="DL">DL</option>
                                            <option value="DT">DT</option>
                                            <option value="FB">FB</option>
                                            <option value="FS">FS</option>
                                            <option value="G">G</option>
                                            <option value="G/T">G/T</option>
                                            <option value="K">K</option>
                                            <option value="KR">KR</option>
                                            <option value="LB">LB</option>
                                            <option value="NT">NT</option>
                                            <option value="OL">OL</option>
                                            <option value="P">P</option>
                                            <option value="PR">PR</option>
                                            <option value="QB">QB</option>
                                            <option value="RB">RB</option>
                                            <option value="S">S</option>
                                            <option value="SS">SS</option>
                                            <option value="T">T</option>
                                            <option value="TE">TE</option>
                                            <option value="WR">WR</option>
                                            <?php } ?>
                                            <?php if($sportname=='soccer'){ ?>
                                            <option value="D">D</option>
                                            <option value="F">F</option>
                                            <option value="G">G</option>
                                            <option value="MF">MF</option>
                                            <?php } ?>
                                            <?php if($sportname=='volleyball'){ ?>
                                            <option value="DS">DS</option>
                                            <option value="LIBERO">LIBERO</option>
                                            <option value="LSH">LSH</option>
                                            <option value="MB">MB</option>
                                            <option value="MH">MH</option>
                                            <option value="OH">OH</option>
                                            <option value="OPPOSITE">OPPOSITE</option>
                                            <option value="PASSER">PASSER</option>
                                            <option value="RSH">RSH</option>
                                            <option value="SETTER">SETTER</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 bothpaddingremove">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>School</label>
                                        <input class="form-control " type="text" name="school" id="school" placeholder="School" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Previous School(if applicable)</label>
                                        <input class="form-control " type="text" name="prevschool" id="prevschool" placeholder="Previous School" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label>Uniform No <span class="error">*</span></label>
                            <input class="form-control " type="text" name="uniformno" id="uniformno" placeholder="Uniform No" value="" aria-required="true" aria-invalid="true">
                        </div>
                        <div class="form-group">
                            <label>Position</label>
                            <select class="form-control" name="position" id="position">

                                <option value="">Select</option>
                                <?php if($sportname == 'basketball'){ ?>
                                <option value="G">G</option>
                                <option value="C">C</option>
                                <option value="F">F</option>
                                <option value="SG">SG</option>
                                <option value="GF">GF</option>
                                <option value="PG">PG</option>
                                <option value="PF">PF</option>
                                <option value="SF">SF</option> <?php } ?>
                                <?php if($sportname == 'baseball' || $sportname == 'softball'){ ?>
                                <option value="Extra fldr">Extra fldr</option>
                                <option value="Pitcher">Pitcher</option>
                                <option value="Catcher">Catcher</option>
                                <option value="First Base">First Base</option>
                                <option value="Second Base">Second Base</option>
                                <option value="Third Base">Third Base</option>
                                <option value="Shortstop">Shortstop</option>
                                <option value="Left Field">Left Field</option>
                                <option value="Center Field">Center Field</option>
                                <option value="Right Field">Right Field</option>
                                <option value="Des Hitter">Des Hitter</option>
                                <option value="Des Player">Des Player</option>
                                <option value="Pinch Hit">Pinch Hit</option>
                                <option value="Pinch Run">Pinch Run</option>
                                <option value="Infielder">Infielder</option>
                                <option value="Outfielder">Outfielder</option>
                                <option value="Utility">Utility</option>
                                <?php } ?>
                                <?php if($sportname == 'football'){ ?>
                                <option value="C">C</option>
                                <option value="C/G">C/G</option>
                                <option value="CB">CB</option>
                                <option value="DB">DB</option>
                                <option value="DE">DE</option>
                                <option value="DL">DL</option>
                                <option value="DT">DT</option>
                                <option value="FB">FB</option>
                                <option value="FS">FS</option>
                                <option value="G">G</option>
                                <option value="G/T">G/T</option>
                                <option value="K">K</option>
                                <option value="KR">KR</option>
                                <option value="LB">LB</option>
                                <option value="NT">NT</option>
                                <option value="OL">OL</option>
                                <option value="P">P</option>
                                <option value="PR">PR</option>
                                <option value="QB">QB</option>
                                <option value="RB">RB</option>
                                <option value="S">S</option>
                                <option value="SS">SS</option>
                                <option value="T">T</option>
                                <option value="TE">TE</option>
                                <option value="WR">WR</option>
                                <?php } ?>
                                <?php if($sportname=='soccer'){ ?>
                                <option value="D">D</option>
                                <option value="F">F</option>
                                <option value="G">G</option>
                                <option value="MF">MF</option>
                                <?php } ?>
                                <?php if($sportname=='volleyball'){ ?>
                                <option value="DS">DS</option>
                                <option value="LIBERO">LIBERO</option>
                                <option value="LSH">LSH</option>
                                <option value="MB">MB</option>
                                <option value="MH">MH</option>
                                <option value="OH">OH</option>
                                <option value="OPPOSITE">OPPOSITE</option>
                                <option value="PASSER">PASSER</option>
                                <option value="RSH">RSH</option>
                                <option value="SETTER">SETTER</option>
                                <?php } ?>
                            </select>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 bothpaddingremove">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select class="form-control" name="gender" id="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Height </label>
                                        <input class="form-control " type="text" name="height" id="height" placeholder="Height" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input class="form-control " type="text" name="weight" id="weight" placeholder="Weight" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 bothpaddingremove">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Date of Birth</label>
                                        <input class="form-control " type="text" name="dob" id="dob" placeholder="mm/dd/yyyy" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input class="form-control " type="text" name="age" id="age" placeholder="Age" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Grade</label>
                                        <input class="form-control " type="text" name="grade" id="grade" placeholder="Grade" value="" aria-required="true" aria-invalid="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label>Date of Birth</label>
                            <input class="form-control " type="text" name="dob" id="dob" placeholder="Date of Birth" value="" aria-required="true" aria-invalid="true">                            
                        </div>
                        <div class="form-group">
                            <label>Age</label>
                            <input class="form-control " type="text" name="age" id="age" placeholder="Age" value="" aria-required="true" aria-invalid="true">
                        </div> -->
                                                
                        <!-- <div class="form-group">
                            <label>Grade</label>
                            <input class="form-control " type="text" name="grade" id="grade" placeholder="Grade" value="" aria-required="true" aria-invalid="true">
                        </div> -->
                        <!-- <div class="form-group">
                            <label>Previous School(if applicable)</label>
                            <input class="form-control " type="text" name="prevschool" id="prevschool" placeholder="Previous School" value="" aria-required="true" aria-invalid="true">
                        </div> -->
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn customgreenbtn" id="game-stats-addplayer" >Save</button>
                    <button type="button" class="btn customredbtn" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>        
    </div>
</div>
<?php include_once('footer.php'); ?>
<script src="assets/custom/js/gamestats.js" type="text/javascript"></script>
<script>
$( document ).ready(function() {

    $( "#dob" ).datepicker({
        format: "mm/dd/yyyy",
        autoclose: true,
    });   

    var teamloginID = $("#teamloginid").val();
    if (teamloginID != "") {
        $(".teamarea_"+teamloginID+"").addClass("disabledarea");
    }

    $('.addexistsplayer').click(function(){
        var hiddengameid = $(this).attr('hiddengameid');
        var hiddenteamid = $(this).attr('hiddenteamid');
        var hiddenteamtype = $(this).attr('hiddenteamtype');

        $.ajax({
            url:"add_exists_player.php",
            method: "GET",
            cache:false,
            data: {gameid:hiddengameid, teamid: hiddenteamid, teamtype: hiddenteamtype},
            success:function(result){
            $("#addExistsPlayerModal").html(result);
            $('#addExistsPlayerModal').modal('show');
        }});
    });

    $(".addplayer").click(function ()
    {       
        $('#gamestatsaddplayer input').val('');
        $(".playerexist").empty();

        var team_id = $(this).attr("data-id");
        var team_name = $(this).attr("data-name");
        var truename = $(this).attr("teamname");
        var gameid = $(this).attr("game-id");
        $("#hiddenteamid").val(team_id);
        $("#hiddenteamname").val(team_name);
        $("#addplayermodal .teamtitle").text(truename);
        $("#hiddengameid").val(gameid);
        
    });
    
});
</script>




