<?php 
include_once('connect.php');
include_once('session_check.php');
include_once('common_functions.php');
include_once('usertype_check.php');

// session empty
if($_SESSION['loginid']=='')  {
 header("Location:login.php");
 exit;
}


$cid = $Cid;
if ($_SESSION['team_manager_id']) {
    $teamlogin_id = $_SESSION['team_manager_id'];
    $chk_team_id= " AND id=".$teamlogin_id;
} else {
    $teamlogin_id = '';
}
$team_manager_id = '';
if ($_SESSION['signin'] == 'team_manager') {
    $team_manager_id = " and team_id='$teamlogin_id'";
}

if(isset($_POST['playerid'])&& !empty($_POST['seasons'])){
    $PlayerID     = $_POST['playerid'];
    $SeasonsArr   = $_POST['seasons'];
    $Inc = 0;
    foreach($SeasonsArr as $SeasonId){      
        $SeasonIds      = "##".implode("##",$SeasonId)."##";
        $TeamQry        = "select * from player_info where id=:PlayerID";
        $getTeamQry      =   $conn->prepare($TeamQry);
        $getTeamQry->execute(array(":PlayerID"=>$PlayerID));
        $getTeamCount      =  $getTeamQry->rowCount();
        $TeamRes     =   $getTeamQry->fetch();
        $TeamId         = $TeamRes['team_id'];
        $created_date   = date("Y-m-d H:i:s");

        $tmpFilePath = $_FILES['upload']['tmp_name'][$Inc];
        if ($tmpFilePath != ""){
            $FileName  = date('YmdHis').$_FILES['upload']['name'][$Inc];
            $newFilePath = "uploads/players/".$FileName;
            move_uploaded_file($tmpFilePath, $newFilePath);
            $data = array(
             'width' => "200",
             'height' =>"200",
             'image_source' =>SYSTEM_ROOT_PATH."/uploads/players/".$FileName,
             'destination_folder' =>SYSTEM_ROOT_PATH."/uploads/players/thumb/",
             'image_name' => $FileName
           );     
           $cropsucessfully = cropImage($data); 
            
        }
        
        $insert_results=array(
			":PlayerID"=>$PlayerID, 
			":cid"=>$cid, 
			":SeasonIds"=>$SeasonIds, 
			":TeamId"=>$TeamId, 
			":FileName"=>$FileName, 
			":created_date"=>$created_date, 
			":modified_date"=>$created_date
		);
        $insertqry="insert into player_images(player_id, customer_id,season_id,team_id,profile_imgs,created_date,modified_date)values(:PlayerID, :cid, :SeasonIds, :TeamId, :FileName, :created_date, :modified_date)";
        $prepinsertqry=$conn->prepare($insertqry);
        $insertRes=$prepinsertqry->execute($insert_results);
        $insertplayer = "INSERT INTO player_images(player_id, customer_id,season_id,team_id,profile_imgs,created_date,modified_date) VALUES ('$PlayerID','$cid','$SeasonIds','$TeamId','$FileName','$created_date','$created_date')";
        $FileName = $newFilePath='';
        $Inc++;
    }
    
    header("Location:player_list.php?msg=4");
    exit;
}

$StatusMsg = '';


// Error msg Start Here
if(isset($_GET["msg"])){
  $msg            =   $_GET["msg"];
} else {
    $msg          =  "";
}
if($msg==1){
    $message    =   "Player has been added successfully.";
}
elseif($msg==2){
    $message    =   "Player has been updated successfully.";
}
elseif($msg==3){
    $message    =   "Player has been deleted successfully.";
}
elseif($msg==4){
    $message    =   "Player season image has been added successfully.";
}
if(isset($_GET["sport"])){
   $sportname     =   $_GET["sport"]; 
} else{
 $sportname     ="";   
}

if(isset($_GET["pid"]) && isset($_GET["sport"])){
  $pid            =   base64_decode($_GET["pid"]);
  $sport_name     =   $_GET["sport"];
} else {
    $pid          =  "";
}
// Error msg Start Here

// Delete Player Start Here
if($pid!="" && !empty($sport_name)){

if($sport_name=='softball' || $sport_name=='baseball')
    {
    $table="player_stats_ba";
    $itable="individual_player_stats_ba";
    }

    if($sport_name=='basketball')
    {
    $table="player_stats";
    $itable="individual_player_stats";
    }

    if($sport_name=='football')
    {
    $table="player_stats_fb";
    $itable="individual_player_stats_fb";
    }
    
    // delete from table player_stats_fb,player_stats,player_stats_ba
    $updateQry      = "update $table set customer_id='1', teamcode='1' where playercode=:id";
    $prepupdateQry  = $conn->prepare($updateQry);
    $update_results = array(":id"=>$pid );
    $updateRes      = $prepupdateQry->execute($update_results);

    // delete from itable individual_player_stats_ba, individual_player_stats,individual_player_stats_fb
    $updateindividualStatusQry   = "update $itable set customer_id='1', teamcode='1' where playercode=:id";
    $prepupdateIndividualQry = $conn->prepare($updateindividualStatusQry);
    $update_Individual_results = array(":id"=>$pid );
    $updateResIndividual     = $prepupdateIndividualQry->execute($update_Individual_results);

    // delete from player_info table
    $updateplayer_infoQry     = "update player_info  set customer_id='1', team_id='1' where id=:id";
    $prepupdatePlayerInfoQry = $conn->prepare($updateplayer_infoQry);

    $update_Player_results = array(":id"=>$pid);
     //print_r($update_Player_results);exit;
    $updateResPlayer       = $prepupdatePlayerInfoQry->execute($update_Player_results);
    
    if($sport_name!=""){
        header("Location:player_list.php?msg=3&sport=".$sport_name);
        exit;
    }
    else{
        header("Location:player_list.php");
        exit;
    }
}
// Delete Player End Here

//Get customer_subscribed_sports data Start here

$sports[]=array();
$sportslists = "select * from customer_subscribed_sports where customer_id=:cid";
$sportslistsqry = $conn->prepare($sportslists);
$sportslistsqry->execute(array(":cid"=>$cid));
$soprts_Count = $sportslistsqry->rowCount();
if($soprts_Count>0){
    $getResSports     =   $sportslistsqry->fetch();
    foreach($getResSports as $sportlist)
    {
        $sports[]= $getResSports['sport_id']; 
    }
}
//Get customer_subscribed_sports data End here

//Get Sport details start here
if(isset($_GET['sport'])){
   $sportname= $_GET['sport'];
   $sport_qry_str = "select * from sports where sport_name like :sportname";
   $get_sport_qry = $conn->prepare($sport_qry_str);
   $get_sport_qry->execute(array(":sportname"=>$sportname."%"));
   $get_soprts_Count = $get_sport_qry->rowCount();
   if($get_soprts_Count>0){
      $getSportsRow=$get_sport_qry->fetch();
      $sportid= $getSportsRow['sportcode'];
    }
}
else{
    $sportid= "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446" ;
}
// Get sports End Here

if(isset($_POST["hnd_team_id"])){
    $hdn_team_id = $_POST["hnd_team_id"];
}

if(isset($_POST["hnd_season_id"])){
    $hdn_season_id = $_POST["hnd_season_id"];
}

if(isset($_POST["hnd_status"])){
    $hnd_status = $_POST["hnd_status"];

} else{
    $status1="and player_info.isActive='1'";
    $status="and isActive='1'";
}

if (isset($_POST['team']) || isset($_POST['hnd_team_id']) ) {

   $team_id = ( isset($_POST['hnd_team_id']) )?$_POST['hnd_team_id']:$_POST['team'];
   $season_id = ( isset($_POST["hnd_season_id"]) )?$_POST['hnd_season_id']:"";
   $hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"active";
        if($hdn_status=="active"){
            $status= "and player_info.isActive=1";
            $status1= "and isActive=1";
        } if($hdn_status=="Inactive" ) {
            $status= "and player_info.isActive=0";
            $status1= "and isActive='0'";
        }
        if($hdn_status=="all") {
            $status= "and player_info.isActive=''";
            $status1="and isActive!=''";
        }
        if ($team_id!="all") {

            if($team_id!="" && $season_id!=""){
            $DbQry1="SELECT * FROM player_info JOIN player_stats_fb ON player_stats_fb.playercode = player_info.id WHERE player_stats_fb.customer_id = '$cid' AND player_stats_fb.season =  '$season_id' and player_info.team_id='$team_id' and player_info.lastname!='TEAM' $status order by player_stats_fb.playercode";
            }
            else if($team_id=="" && $season_id!=""){
              $DbQry1="SELECT * FROM player_info JOIN player_stats_fb ON player_stats_fb.playercode = player_info.id WHERE player_stats_fb.customer_id = '$cid' AND player_stats_fb.season = '$season_id' and player_info.lastname!='TEAM' $status order by player_stats_fb.playercode";
            }
            else if($team_id!=""){
              $DbQry1 = "select * from player_info where customer_id='$cid' $team_manager_id and team_id='$team_id'  and lastname!='TEAM' '$status1' order by lastname";
           }else {
            $DbQry1 = "select * from player_info where customer_id='$cid' $team_manager_id and lastname!='TEAM' $status1 and (sport_id='$sportid')";

           }
        } else {

           $DbQry1 = "select * from player_info where customer_id='$cid' $team_manager_id and lastname!='TEAM' $status1 and (sport_id='$sportid')";
        }
} else {
  
  $DbQry1 = "select * from player_info where customer_id='$cid' $team_manager_id and lastname!='TEAM' $status and (sport_id='$sportid')";
}

if ($_SESSION['master'] == 1) { 
    $children = array($_SESSION['childrens']);
    $ids = $_SESSION['loginid'].",".join(',',$children);  
    $team_id = ( isset($_POST['hnd_team_id']) )?$_POST['hnd_team_id']:$_POST['team'];
    $hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"";
        if($hdn_status=="active"){
            $status= "and isActive='1'";
        } if($hdn_status=="Inactive") {

            $status= "and isActive='0'";
        }
    if($team_id!="all"){
         if($team_id!=""){
            $DbQry1 = "select * from player_info where team_id='$team_id' and lastname!='TEAM' $status order by lastname";
        } else {
           $DbQry1 = "select * from player_info where customer_id in ($ids) $team_manager_id $team_manager_id  and lastname!='TEAM' $status  and (sport_id='$sportid')";

        }

    } else {
    $DbQry1 = "select * from player_info where customer_id in ($ids) $team_manager_id $team_manager_id  $status and lastname!='TEAM'  and (sport_id='$sportid')";
    }
}

//Get Http url
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$urlArr= explode('=',$actual_link);

/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
/*End of paging*/

include_once('header.php'); ?>
<link href="assets/custom/css/playerlist.css" rel="stylesheet" type="text/css" />
<style type="text/css">

table.dataTable.no-footer {
    border-bottom: 0px solid #111; 
}
table.dataTable{
    border-collapse: collapse;
}
.loadingplayersection {
    display: none;
    text-align: center;
    min-height: 400px;
    background-color: #ffffff;
    padding-top: 175px;
}
</style>
<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                
                <div class="col-md-12">
                    <form action="" method="post" id="searchplayedform">
                        <input type="hidden" name="customerid" id="customerid" value="<?php echo ($_SESSION['master'] == 1) ? $ids : $cid ?>">
                        <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">
                        <?php 
                        if($_SESSION['signin'] != 'team_manager'){
                            if($_SESSION['master'] ==1 ) { ?>
                            <div class="portlet light col-md-8" style="padding: 20px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: right;margin-bottom:10px;">
                                <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                    <div class="col-md-12">
                                        <div class="caption font-red-sunglo col-md-2 " style="margin-top: 7px;">
                                            <i class="icon-search font-red-sunglo"></i>
                                            <span class="caption-subject bold uppercase">Search</span>
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-5" style="padding: 0px;">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group ">
                                                    <div class="form-group">
                                                        <select class="form-control  player_form border-radius" name="team" id="searchbynameid" onchange="filter_aba_players_by_team();"> 
                                                            <option value="all">All Team</option>
                                                            <?php
                                                                $children = array($_SESSION['childrens']);
                                                                $ids = $_SESSION['loginid'].",".join(',',$children);
                                                                $tres="select * from teams_info where customer_id in ($ids) and sport_id='$sportid'";

                                                                $treslistsqry = $conn->prepare($tres);
                                                                $treslistsqry->execute(array(":cid"=>$cid));
                                                                $trowRes     =   $treslistsqry->fetchAll();
                                                                foreach ($trowRes as $trow) {
                                                                    if(!empty($trow['team_name'])){
                                                                ?> 
                                                                    <option value="<?php echo $trow['id']; ?>"><?php echo $trow['team_name']; ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                         <?php if($hdn_team_id!=""){ $teamids=$hdn_team_id;} else if($team_id!="") {$teamids=$team_id;} else {$teamids="all";} ?>
                                                        <script>$("#searchbynameid").val("<?php echo $teamids;?>")</script>
                                                    </div>
                                                </div> 
                                            </div>                                         
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-5" style="padding: 0px;">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group ">
                                                    <div class="form-group">
                                                        <select class="form-control  player_form border-radius" name="active_status" id="searchbyabastatus" onchange="filter_aba_players_by_team();"> 
                                                            <option value="all">All Player</option>
                                                            <option value="active">Active</option>
                                                            <option value="Inactive">InActive</option>
                                                        </select>
                                                        <?php if($hdn_status!="" ){$active=$hdn_status;} else{$active="active";} ?>
                                                       <script>$("#searchbyabastatus").val("<?php echo $active;?>")</script> 
                                                


                                                    </div>
                                                </div> 
                                            </div>                                         
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="portlet light col-md-10" style="padding: 20px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: right;margin-bottom:10px;">
                                <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                    <div class="col-md-12">
                                        <div class="caption font-red-sunglo col-md-2 col-sm-2 col-xs-12" style="margin-top: 7px;">
                                            <i class="icon-search font-red-sunglo"></i>
                                            <span class="caption-subject bold uppercase">Search</span>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-12" style="padding: 0px;">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="form-group ">
                                                    <div class="form-group">
                                                        <select class="form-control  player_form border-radius" name="team" id="searchbynameid" onchange="filter_players_by_team();"> 
                                                            <option value="all">All Team</option>
                                                            <?php
                                                                $tres="select * from teams_info where customer_id='$cid' and (sport_id='$sportid')";
                                                                $treslistsqry = $conn->prepare($tres);
                                                                $treslistsqry->execute(array(":cid"=>$cid));
                                                                $trowRes     =   $treslistsqry->fetchAll();
                                                                foreach ($trowRes as $trow) {
                                                                    if(!empty($trow['team_name'])){
                                                                ?> 

                                                                    <option value="<?php echo $trow['id']; ?>"><?php echo $trow['team_name']; ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                        <?php if($team_id!="" ){$teamids=$team_id;} else if($hnd_team_id!="") {$teamids=$hnd_team_id;} else {$teamids="all";} ?>
                                                         <script>$("#searchbynameid").val("<?php echo $teamids;?>")</script>
                                                    </div>
                                                </div> 
                                            </div> 
                                             <?php
                                                if($soprts_Count==1)
                                                {
                                                    if($sports[1]=='4444') { $ls='player_stats_bb'; } 
                                                    if($sports[1]=='4442' || $sports[1]=='4441') { $ls='player_stats_ba'; } 
                                                    if($sports[1]=='4443') { $ls='player_stats_fb'; } 
                                                }
                                                $seasonres="select distinct(season) from $ls where customer_id=:cid";
                                                $seasonresqry = $conn->prepare($seasonres);
                                                $seasonresqry->execute(array(":cid"=>$cid));
                                                $getSeasonRes     =   $seasonresqry->fetchAll();
                                            ?>
                                            <div class="col-md-4 col-sm-4 col-xs-12" style="padding-right: 0px;">
                                                <div class="form-group ">
                                                    <div class="form-group">
                                                        <input type="hidden" name="lsid" id="tableid" value="<?php echo $ls ?>">
                                                        <select class="form-control player_form border-radius" name="searchbyseason" id="searchbyseasonid" onchange="filter_players_by_team();">
                                                            <option value="">Select Season</option>
                                                            <?php foreach($getSeasonRes as $seasonrow) {
                                                            if (!empty($seasonrow['season'])) { 
                                                            $snrow="select name from customer_season where id=".$seasonrow[season];
                                                            $snrowqry = $conn->prepare($snrow);
                                                            $snrowqry->execute();
                                                            $getsnrowRes     =   $snrowqry->fetch();
                                                            ?>
                                                            <option value="<?php echo $seasonrow['season'] ?>"><?php echo $getsnrowRes['name'] ?></option>
                                                            <?php } }?>
                                                        </select>
                                                         <?php if($hdn_season_id!="")
                                                         {$seasonid=$hdn_season_id;} else {$seasonid=$season_id;}?>
                                                        <script>$("#searchbyseasonid").val("<?php echo $seasonid;?>")</script>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12" style="padding-right: 0px;">
                                                <div class="form-group ">
                                                    <div class="form-group">
                                                        <select class="form-control  player_form border-radius" name="active_status" id="searchbyteamstatus" onchange="filter_players_by_team();"> 
                                                            <option value="all">All Player</option>
                                                            <option  value="active">Active</option>
                                                            <option value="Inactive">InActive</option>
                                                        </select>
                                                        <?php if($hdn_status!=""){$active=$hdn_status;} else{$active="active";} ?>
                                                        <script>$("#searchbyteamstatus").val("<?php echo $active;?>")</script>
                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } }?>
                    </form>
                 </div><!--Col-md-12 -->
            </div> <!--row -->
            <?php
                if(!empty($message)){
                ?>
                <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">x</a>
                <?php echo $message;?>
                </div>
                <?php
                }
                ?>
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="    clear: both;">
                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF PLAYER                       
                            </h3>
                            <div class="pull-right">                                
                               <?php if(isset($_GET['sport'])){ ?>

                            <button type="button" class="player_btn " style="margin-right: 10px;border-radius: 4px !important;" onclick="document.location='manage_player.php?sport=<?php echo $sportname; ?>'">Add Player</button> 
                            <button type="button" class="player_btn " style="margin-right: 10px;border-radius: 4px !important;text-transform:capitalize;" onclick="document.location='add_bulkentry_player.php?sport=<?php echo $sportname; ?>'"> Add bulk player</button>
                            <input type="hidden" name="sportname" id="sportname" value="<?php echo $sportname; ?>">
                            <?php } else{ ?> <?php
                            if($soprts_Count==1)
                            {
                            if($sports[1]=='4444') { $ls='basketball'; } 
                            if($sports[1]=='4442') { $ls='softball'; } 
                            if($sports[1]=='4441') { $ls='baseball'; } 
                            if($sports[1]=='4443') { $ls='football'; } 
                            ?>

                            <button type="button" class="player_btn " style="margin-right: 10px;border-radius: 4px !important;" onclick="document.location='manage_player.php?sport=<?php echo $ls; ?>'">Add Player</button> 
                            <button type="button" class="player_btn " style="margin-right: 10px;border-radius: 4px !important;text-transform:capitalize;" onclick="document.location='add_bulkentry_player.php?sport=<?php echo $ls; ?>'"> Add bulk player</button>
                            <input type="hidden" name="sportname" id="sportname" value="<?php echo $ls; ?>">
                            <?php } ?>

                            <?php } ?>
                                
                            </div>
                        </div>
                        <div class="portlet-body">
                         <div class="loadingplayersection" style="display:none;">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                         </div>
                        <div class="table-responsive" id="sample" >
                            <form id="frm_player_list" name="frm_player_list" method="post" action="player_list.php" enctype="multipart/form-data">
                            <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                            <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                            <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                            <input type="hidden" name="customerid" id="customerid" value="<?php echo $cid ?>">
                            <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">
                            <input type="hidden" name="hnd_team_id" id="hnd_team_id" value="<?php echo $hdn_team_id ?>">
                            <input type="hidden" name="hnd_season_id" id="hnd_season_id" value="<?php echo $season_id;?>">
                            <input type="hidden" name="hnd_status" id="hnd_status" value="<?php echo $hnd_status;?>">

                            
                            <table class="table table-striped table-bordered table-hover no-footer dataTable " id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
                                <thead>
                                <tr>
                                    <th nowrap class="tbl_center" > Player&nbsp;Id </th>
                                    <th nowrap class="tbl_center" > First&nbsp;Name </th>
                                    <th nowrap class="tbl_center" > Last&nbsp;Name </th>
                                    <th nowrap class="tbl_center" > Uniform&nbsp;No </th>
                                    <th nowrap class="tbl_center" > Position </th>
                                    <th nowrap class="tbl_center" > Team&nbsp;Name </th>
                                    <th nowrap class="tbl_center" > Player&nbsp;Image </th>
                                    <th nowrap class="tbl_center" > Player&nbsp;Image&nbsp;by&nbsp;Season</th>
                                    <th nowrap class="tbl_center" > isActive </th>
                                    <th nowrap class="tbl_center" > Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $dbQry = $DbQry1; 
                                        $getResQry      =   $conn->prepare($dbQry);
                                        $getResQry->execute();
                                        $getResCnt      =   $getResQry->rowCount();
                                        $getResQry->closeCursor();
                                        if($getResCnt>0){
                                            $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                            $Start=($Page-1)*$RecordsPerPage;
                                            $sno=$Start+1;
                                                
                                            $dbQry.=" limit $Start,$RecordsPerPage";
                                                    
                                            $getResQry      =   $conn->prepare($dbQry);
                                            $getResQry->execute();
                                            $getResCnt      =   $getResQry->rowCount();
                                        if($getResCnt>0){
                                            $getResRows     =   $getResQry->fetchAll();
                                            $getResQry->closeCursor();
                                            $s=1;
                                                foreach($getResRows as $player_info){
                                                    $player_no  =   $player_info["id"];
                                                    $first_name =   $player_info["firstname"];
                                                    $last_name  =   $player_info["lastname"];
                                                    $uniform_no =   $player_info["uniform_no"];
                                                    $position   =   $player_info["position"];
                                                    $home_town  =   $player_info["hometown"];
                                                    $image      =   $player_info["image"]; 
                                                    if($image!=""){
                                                       $img="uploads/players/thumb/".$image;
                                                        $img1="uploads/players/".$image;
                                                    } else {
                                                       $img="uploads/players/thumb/download.jpg";
                                                        $img1="";
                                                    }
                                                    $player_note      =   $player_info["player_note"];
                                                    $teamid=$player_info['team_id'];
                                                    $res2="select * from teams_info where id='$teamid' and customer_id='".$player_info["customer_id"]."' and (sport_id='$sportid')";
                                                    $getResQry2      =   $conn->prepare($res2);
                                                    $getResQry2->execute();
                                                    $getResCnt2      =   $getResQry2->rowCount();
                                                    $getResRow     =   $getResQry2->fetch();
                                                    $teamname=$getResRow ['team_name'];    
                                    ?>
                                    <tr class="odd gradeX">
                                        <td nowrap class="text-center"><?php echo $player_no; ?></td>
                                        <td nowrap class="tbl_center_td"><a href="manage_player.php?p_id=<?php echo base64_encode($player_no);?>" ><?php echo $first_name; ?></a></td>
                                        <td nowrap class="tbl_center_td"><?php echo $last_name; ?></td>
                                        <td nowrap class="tbl_center_td"><?php echo $uniform_no; ?></td>
                                        <td nowrap class="tbl_center_td"><?php echo $position; ?></td>
                                        <td nowrap class="tbl_center_td"><?php echo $teamname; ?> </td>
                                        <td nowrap class="tbl_center_td">
                                            <?php if($img1!=""){ ?>
                                            <a class="btn_round_green  btn-circle btn-icon-only tooltips btn_round_green" style="line-height: 1.00;"  data-toggle="modal" data-id="<?php echo $player_no; ?>" href="#small<?php echo $player_no; ?>"><i class="fa fa-photo"></i></a>
                                            <div class="modal fade bs-modal-sm" id="small<?php echo $player_no; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Player Image</h4>
                                                        </div>
                                                        <div class="modal-body" style="padding-left: 0px;padding-right: 0px;"> <img width="150" height="150" alt="" src="<?php echo $img1; ?>"</div>
                                                        <div class="modal-footer" style="margin-top:10px;">
                                                            <button type="button" class="btn red btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                            <?php }?>
                                        </td>
                                        <td nowrap class=""><button type="button" class="btn purple btn-sm btn_purple" data-toggle="modal" data-target="#myModal<?php echo $player_no; ?>">Season Image</button>

                                        <div class="modal fade draggable-modal" id="myModal<?php echo $player_no; ?>" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Season Profile Images</h4>
                                                    </div>
                                                    <div class="modal-body"> 
                                                      <form class="profileimgform " method="POST" enctype="multipart/form-data">                 
                                                 <input name="playerid" type="hidden" value="<?php echo $player_no; ?>" class="hidplayerid"/>
                                                    <?php
                                                        $PlayerImgIds = "select * from player_images where player_id=:PlayerId";
                                                        $getPlayerQry    =   $conn->prepare($PlayerImgIds);
                                                        $getPlayerQry->execute(array(":PlayerId"=>$player_no));
                                                        $ProfileCnt      =   $getPlayerQry->rowCount();
                                            
                                                    ?>
                                                    <div class="col-md-12 playerimgcont">
                                                        <div class="alert" id="deletestatus"></div>
                                                        <table class="table table-bordered" style="border: 1px solid #ccc;">
                                                        <tr><th class="imghead">Action</th><th class="imghead">Season</th><th class="imghead">Profile Image</th><?php if ($ProfileCnt){ echo '<th class="imghead">Remove</th>';}?></tr>
                                                        <?php                                               
                                                        $SeasonIdsArr = array();
                                                         if ($ProfileCnt){
                                                            $getPlayersRows     =   $getPlayerQry->fetchAll();
                                                                foreach($getPlayersRows as $ImgList){
                                                                    $ProfileImgId= $ImgList['player_img_id'];
                                                                    $SeasonIdArr  = array_filter(explode("##",$ImgList['season_id']));
                                                                    $SeasonIdsArr  = array_merge($SeasonIdsArr,$SeasonIdArr);

                                                                    $SeasonId    = implode(",",$SeasonIdArr);
                                                                    
                                                                    $SelectedSeason = '<select class="profileimgsedit" multiple="multiple">';                                   
                                                                     $sealist="select * from customer_season where custid=:cid";
                                                                     $getseaQry      =   $conn->prepare($sealist);
                                                                     $getseaQry->execute(array(":cid"=>$cid));
                                                                     $getseaCount      =   $getseaQry->rowCount();
                                                                     $getseaRows     =   $getseaQry->fetchAll();
                                                                    foreach($getseaRows as $sowlist){
                                                                        $Selected  = (in_array($sowlist["id"],$SeasonIdArr))?'selected':'';
                                                                        
                                                                        $SelectedSeason .='<option value="'.$sowlist["id"].'" '.$Selected.'>'.$sowlist["name"].'</option>';
                                                                     } 
                                                                    $SelectedSeason .='</select>';  

                                                                    $ProfileImg  = $ImgList['profile_imgs'];
                                                                    $ProfImgHtml = '';

                                                                    if($ProfileImg!="" && is_file("uploads/players/".$ProfileImg)){ 
                                                                        $ProfImgHtml ="<img src='uploads/players/$ProfileImg' alt='' width='40' height='40'/>";
                                                                    } 
                                                                    $Seasons     = "select name from customer_season where id in($SeasonId)";
                                                                     $getSeasonsQry      =   $conn->prepare($Seasons);
                                                                     $getSeasonsQry->execute();
                                                                     $getSeasonsCount      =   $getSeasonsQry->rowCount();
                                                                    echo '<tr><td style="vertical-align:middle;"><span date-imgid='.$ProfileImgId.' class="editplayimg">Edit</span><p class="loading_img"><img src="images/loading-publish.gif"></p></td><td class="text-center seasonsnames profileimgswrap">';
                                                                     $getSeasonsRows     =   $getSeasonsQry->fetchAll();
                                                                    foreach( $getSeasonsRows as $SeasonsList ){                      
                                                                        echo '<p style="margin-top:0px">'.$SeasonsList['name'].'</p>';                             
                                                                    }
                                                                    echo $SelectedSeason;
                                                                    echo '</td>';
                                                                    echo "<td class='text-center profileimgdips'>$ProfImgHtml</td>";
                                                                    echo "<td class='text-center deleteimgicons'><img src='images/delete.png' class='imguploadicons deleteprofileimg' date-imgid='$ProfileImgId'></td></tr>";
                                                             }  
                                                         }else{
                                                            echo '<tr><td colspan="3" class="imgslistbody">Season image not uploaded</td></tr>';
                                                         }?>        
                                                         </table>
                                                    </div>
                                                    <div class="col-md-12 playerimguploadwrap imguploadcont"> 
                                                        <h4 class='addnewheading text-left' style="color: #000;">Add image to season</h4>
                                                        <div class="col-md-5 col-sm-12">                        
                                                            <div class="col-md-12 profileimgswrap bottomadddmore" >    
                                                                              
                                                                <select name="seasons[0][]" class="profileimgs" multiple="multiple">
                                                                    <?php
                                                                    $sealistQry="select * from customer_season where custid=:cid";
                                                                    $getsealistResQry      =   $conn->prepare($sealistQry);
                                                                    $getsealistResQry->execute(array(":cid"=>$cid));
                                                                    $getRowCount      =   $getsealistResQry->rowCount();
                                                                    $getseasonListRows     =   $getsealistResQry->fetchAll();
                                                                    foreach ($getseasonListRows as $sowlist) {
                                                                    $SelDsbld   = (in_array($sowlist["id"],$SeasonIdsArr))?" disabled":"";
                                                                        echo '<option value="'.$sowlist["id"].'" '.$SelDsbld.'>'.$sowlist["name"].'</option>';

                                                                    } ?>
                                                                </select>
                                                        
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-sm-12 imgsuploaderfile">                       
                                                            <div class="form-group col-md-12">
                                                                <input type="file" name="upload[]"  multiple="multiple">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-12 imgsuploader">                       
                                                            <div class="imgiconcont">
                                                                <img src="images/addmoreimg.png" class='imguploadicons addplusicon'>
                                                            </div>
                                                        </div>                                          
                                                    </div>
                                                    <div id="moreuploadwrap">
                                                    </div>
                                                    <div class="col-md-12 col-sm-12" style='margin-top:10px;padding-bottom: 25px;'>
                                                        <input class="btn btn-success uploadimgbtn" type="button" name="addsubmit" value="Update/Save" >&nbsp;&nbsp;<input class="btn btn-danger" type="button" value="Cancel" id="cancelbtn" data-dismiss="modal">
                                                    </div>
                                                    
                                                  </form>



                                                    </div>
                                                    <div class="modal-footer" style="border-top: 0px solid #e5e5e5; ">
                                        
                                                </div> 
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </td>
                                    <td nowrap>
                                        <?php 
                                        $isactive=$player_info['isActive'];
                                        if($isactive=="1")
                                        {
                                        $status_val='<img src="./images/active_player.png"  class="tooltips" data-original-title="Click here to Deactive" ></img>';
                                        $change_status='Deactive';
                                        }else
                                        {
                                        $status_val='<img src="./images/deactive_player.png"  class="tooltips" data-original-title="Click here to Active"></img>';
                                        $change_status='Active';
                                        }
                                        ?>
                                        <a href="javascript:void(0);"  id="<?php echo $player_info['id']?>" rel="<?php echo $change_status;?>"   class="status_change tooltips"  ><?php echo $status_val;?></a>
                                    </td>
                                    <td nowrap class="tbl_center_td">
                                            <?php if($sportname!=""){ $sportname=$sportname;} else {
                                                $sportname=$ls;
                                            }?>
                                            <a class="btn_round  btn-circle btn-icon-only btn-default red tooltips btn-danger" style="line-height: 1.00;" type="button " onclick="return confirmation('<?php echo base64_encode($player_no); ?>','<?php echo $sportname; ?>');" style="cursor:pointer;" data-original-title="Delete Player" /><i class="icon-trash trash_btn" ></i></a>
                                    </td>

                                    </tr>
                                    <?php
                                        $s++;
                                       }
                                    } else{
                                       echo "<td colspan='8' style='text-align:center;'>No Player(s) found.</td>";
                                    }
                                    }
                                    else{
                                        echo "<tr><td colspan='8' style='text-align:center;'>No Player(s) found.</td></tr>";
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                            <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='12' valign='middle' class='pagination'>";
                                $FormName = "frm_player_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                           ?>
                        </div>
                    </div>
                    <!-- </form> -->
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->
           
            
                            <!-- END SAMPLE TABLE PORTLET-->
            </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT --> 
              
</div>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/playerlist.js" ></script>

<script>
$(document).ready(function() {
    $('#sample_1').DataTable({
        "retrieve": true,
        "paging": false,
        "bInfo": false,
       "bFilter":false,
        "bLengthChange":false,
        "bPaginate":false,
       //"aaSorting": [[0,'desc']],
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [6,7,8,9] } ], 
    });
});

function loading_sorting(){
    $(document).ready(function() { 
        $('#sample_1').DataTable({
             "retrieve": true,
            "paging": false,
            "bInfo": false,
           "bFilter":false,
            "bLengthChange":false,
            "bPaginate":false,
            //"aaSorting": [[0,'desc']],
            "aoColumnDefs": [ { "bSortable": false, "aTargets": [6,7,8,9] } ], 
        });
        
        $('.profileimgs').multiselect({
            includeSelectAllOption: false,
            numberDisplayed: 1,
            nonSelectedText: "Select season",
        });
         $('.profileimgsedit').multiselect({
            includeSelectAllOption: false,
            numberDisplayed: 1,
            nonSelectedText: "Select season",
        });
    });
}

 var MultiSelectCnt = 1;
$(document).ready(function() {
    $('.profileimgs').multiselect({
        includeSelectAllOption: false,
        numberDisplayed: 1,
        nonSelectedText: "Select season",
    });
     $('.profileimgsedit').multiselect({
        includeSelectAllOption: false,
        numberDisplayed: 1,
        nonSelectedText: "Select season",
    });
});

 function filter_aba_players_by_team() {

            var team_manager_id = '<?php echo $team_manager_id; ?>';
            var Searchbyname   = $('#searchbynameid').val();
            var Searchbystatus   = $('#searchbyabastatus').val();
            $("#searchbyabastatus").val(Searchbystatus);
            var CustomerID     = $('#customerid').val();    
            var SportID        = $('#sportid').val(); 
            var ls             = $("#tableid").val();
            var sportname      = $("#sportname").val();
            var HdnPage        = $("#HdnPage").val();
            var HdnMode        = $("#HdnMode").val();
            var RecordsPerPage = $("#RecordsPerPage").val();
            $(".loadingplayersection").show();
            $("#sample").hide();
            $.ajax({
                url:"filter_aba_players.php",  
                method:'GET',
                data:"team_manager_id="+team_manager_id+"&sportid="+SportID+"&searchbyname="+Searchbyname+"&cid="+CustomerID+"&sportname="+sportname+"&HdnPage="+HdnPage+"&HdnMode="+HdnMode+"&PerPage="+RecordsPerPage+"&Searchbystatus="+Searchbystatus,
                success:function(data) { 
                    $(".loadingplayersection").hide();
                    $("#sample").show();                        
                    document.getElementById('sample').innerHTML = data;                   
                    $('.table-header').remove();
                    loading_sorting();
                }
              });                
            
        }
</script>

