<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('common_functions.php');
include_once('usertype_check.php');

if ($_SESSION['team_manager_id']) {
    $teamlogin_id = $_SESSION['team_manager_id'];
    
} else {
    $teamlogin_id = '';
}
$team_id = '';
if ($_SESSION['signin'] == 'team_manager') {
    $team_id = " AND (visitor_team_id=$teamlogin_id OR home_team_id=$teamlogin_id)";
}

$Sports = array();
$SportListArr = array();
$SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
$SportListArr = array(":customer_id"=>$Cid);
$SportsLists->execute($SportListArr);
$CntSportsLists = $SportsLists->rowCount();
if ($CntSportsLists > 0) {
    $SporstRes = $SportsLists->fetchAll(PDO::FETCH_ASSOC);
    foreach ($SporstRes as $SporstRow) {
        $Sports[]= $SporstRow['sport_id']; 
    }
}
// if (isset($_SESSION['sportname'])) {
if (isset($_GET['sport'])) {

    // $SportName = $_SESSION['sportname'];
    $SportName = $_GET['sport'];
    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        if ($_SESSION['master'] !=1) {
            foreach ($QrySportRow  as $QrySportVal) {
                $SportId = $QrySportVal['sportcode']; 
            }
        } else {
           foreach ($QrySportRow  as $QrySportVal) {
                $SportId = " AND sport_id=".$QrySportVal['sportcode']; 
            } 
        }
    }    
} else {
    if ($_SESSION['master'] !=1) {
        $SportId = "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446";
    } else {
        $SportId = "AND sport_id IN (4441,4442,4443,4444,4445,4446)";
    }
}

if ($_SESSION['master'] == 1) {
    $children = $_SESSION['loginid'].",".$_SESSION['childrens'];
    $DefaultSeasonDivQry = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($children) order by season desc limit 0, 1");
} else {
    // $DefaultSeasonDivQry = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($Cid) order by season desc limit 0, 1");
    $DefaultSeasonDivQry = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($Cid) order by season asc limit 0, 1");
}

$DefaultSeasonDivQry->execute();
$FetchDefaultSeasonDiv = $DefaultSeasonDivQry->fetch(PDO::FETCH_ASSOC);
$DefaultSeasonDiv =  $FetchDefaultSeasonDiv['season'];
if (!empty($DefaultSeasonDiv)) {
    if ($_SESSION['signin'] != 'team_manager') {
        $DefaultSeasonDivCondn = ($_SESSION['master'] != 1) ? " and season='$DefaultSeasonDiv'" : " and season='$DefaultSeasonDiv'" ;
    } else {
        $DefaultSeasonDivCondn = "";
    }
} else {
    $DefaultSeasonDivCondn = "";
}
if (isset($_GET['search'])) {

    $searchelem = $_GET['search'];
    $divname = $searchelem;
    $resdiv = $conn->prepare("select * from customer_division where name like '{$divname}%'");
    $resdiv->execute();
    $FetchResDiv = $resdiv->fetchAll(PDO::FETCH_ASSOC);
    foreach ($FetchResDiv as $rowdiv) {
        $divid = $rowdiv['id'];
    }    

    $res = "select * from games_info where home_customer_id='$Cid' $sportid  and division='$divid' $team_id $DefaultSeasonDivCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time";
} else {
    $res = "select * from games_info where (home_customer_id='$Cid' or visitor_customer_id='$Cid') $team_id $DefaultSeasonDivCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time";
    if ($_SESSION['master']==1) {
        $children = $_SESSION['loginid'].",".$_SESSION['childrens'];
        $res = "select * from games_info where (home_customer_id IN ($children) or visitor_customer_id IN ($children)) $team_id $DefaultSeasonDivCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time";
    }

}
// echo $res;
$ListingGameQry = $conn->prepare($res);
$ListingQryArr = array(":cid"=>$Cid, ":sportid"=>$SportId);
$ListingGameQry->execute($ListingQryArr);
$CntListingGame = $ListingGameQry->rowCount();
if ($CntListingGame > 0) {
    $GameRes = $ListingGameQry->fetchAll(PDO::FETCH_ASSOC);    
    $GameDetail = array();
    $j =0;
    foreach ($GameRes as $GameRow) {
        $GameYear = date("Y", strtotime($GameRow['date']));
        $GameMonth = date("m", strtotime($GameRow['date']));
        $MatchDetail = $GameRow['visitor_team_id']. " vs ".$GameRow['home_team_id'];
        $GameDetail[$GameYear][$GameMonth][$j] = $GameRow;
        $j++;
    }   
}
// echo "<pre>";
// print_r($GameDetail);exit;
$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "Added new game successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 2) {
        $alert_message = "Duplicate game!!";
        $alert_class = "alert-danger";
    } else if($_GET['msg'] == 3) {
        $alert_message = "Game updated successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 4) {
        $alert_message = "Game deleted successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 5) {
        $alert_message = "Games data updated successfully!";
        $alert_class = "alert-success";
    }
}

include_once('header.php');
?>
<link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="assets/custom/css/gamelist.css" rel="stylesheet" type="text/css" />
    <div class="page-content-wrapper">
        <div class="page-content gamelisting">

            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } 

            if ($_SESSION['signin'] != 'team_manager') { ?>
            <div class="row searchheder">                
                <?php 
                if ($_SESSION['master'] != 1) {
                    
                    $ParenClass = "col-md-7";
                    $SearchBoxClass = "col-md-5 col-sm-5 col-xs-12";
                    $SearchBtnClass = "col-md-2 col-sm-2 col-xs-12";
                    $Inlinestyle = "";
                    $Inlinestyle2 = "padding-left:11px;";

                } else {
                    $ParenClass = "col-md-7";
                    $SearchBoxClass = "col-md-5 col-sm-5 col-xs-12";
                    $SearchBtnClass = "col-md-2 col-sm-2 col-xs-12";
                    $Inlinestyle = "";
                    $Inlinestyle2 = "padding-left:11px;";
                }
                ?>
                <input type="hidden" name="sportid" id="sportid" value="<?php echo $SportId ?>">
                <input type="hidden" name="customerid" id="customerid" value="<?php echo $Cid ?>">
                <div class="<?php echo $ParenClass; ?> searchbarstyle" style="<?php echo $Inlinestyle; ?>">
                    <?php 
                        if ($_SESSION['signin'] != 'team_manager') {
                            if ($_SESSION['master'] == 1) {
                                $ids = $_SESSION['loginid'].",".$_SESSION['childrens'];
                            } else {
                                $ids = $Cid;
                            }
                        // if($_SESSION['signin'] != 'team_manager'){
                        // if ($_SESSION['master'] !=1) {?>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="form-group ">
                                <!-- <input type="hidden" name="sportid" id="sportid" value="<?php echo $SportId ?>">
                                <input type="hidden" name="customerid" id="customerid" value="<?php echo $Cid ?>"> -->
                                <select class="form-control border-radius" id="searchbyseasonid" onchange="return ajaxGameList();">
                                <?php
                                // $SeasonRes = $conn->prepare("select distinct(season) from games_info where home_customer_id=:cid");
                                $SeasonRes = $conn->prepare("select distinct(season) from games_info where home_customer_id in ($ids)");
                                $SeasonResArr = array(":cid"=>$Cid);
                                $SeasonRes->execute($SeasonResArr);
                                $SeasonResCnt = $SeasonRes->rowCount();
                                if ($SeasonResCnt > 0) {
                                    $FetchSeason = $SeasonRes->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($FetchSeason as $SeasonRow) { 
                                        $SeasonName = json_decode(getSeasonName($SeasonRow['season']), true);
                                        ?>
                                    <option <?php echo ($SeasonRow['season'] == $DefaultSeasonDiv)? "selected":"" ?> value="<?php echo $SeasonRow['season']; ?>"><?php echo $SeasonName['name'] ?></option>
                                    <?php }
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    <?php }//} //}?>
                    <div class="<?php echo $SearchBoxClass; ?> removerightpadding" >
                        <form class="search-form search-form-expanded" >
                            <div class="form-group">                                
                                <input type="text" id="searchtext" class="form-control border-radius" placeholder="Search by division" name="search">                                
                                
                            </div>
                        </form>
                    </div>
                    <div class="<?php echo $SearchBtnClass; ?> searchrightpadding" style="<?php echo $Inlinestyle2; ?>">
                        <div class="form-group" style="">
                            <input type="button" id="resetbtn" class="btn btn-danger " value="Reset" name="query" >
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
            <input type="hidden" id="gamedate" value="">
            <div class="loadingsection">
                <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
            </div>
            <div id='game-carousal'>
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false" style="margin: 0 auto">
                    <div class="col-md-12 mycarousalnew">
                        <div class="col-md-4 col-sm-4 col-xs-4 logames ">
                            <i class="fa fa-list font-red-sunglo"></i><span class="caption-subject font-red-mint bold uppercase "> List of games</span>                        
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 monthrcds">
                            <div class="gamemonth">
                            <strong class=""></strong>
                            <input type="hidden" id="datepicker" value=""><i class="fa fa-calendar" id="datepicker-button"></i>
                            </div>                            
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 logamesbtn">                            
                         <?php if (isset($_GET['sport'])) { ?>
                        <a href="manage_game.php?sport=<?php echo $SportName; ?>" class="btn btn-small addcustomerbtn" >
                         Add Game </a>
                         <a href="add_bulk_game.php?sport=<?php echo $SportName; ?>" style="margin-right:10px;" class="btn btn-small addcustomerbtn" >
                         Add Bulk Game </a>
                        <?php }  else { ?><?php
                        if ($CntSportsLists == 1) {
                            if($Sports[0]=='4444') { $ls = 'basketball'; } 
                            if($Sports[0]=='4443') { $ls = 'football'; } 

                            if($Sports[0]=='4441') { $ls = 'baseball'; } 
                            if($Sports[0]=='4442') { $ls = 'softball'; } 
                        ?> <a href="manage_game.php?sport=<?php echo $ls; ?>" class="btn btn-small addcustomerbtn" >
                         Add Game </a> 
                         <a href="add_bulk_game.php?sport=<?php echo $ls; ?>" style="margin-right:10px;" class="btn btn-small addcustomerbtn" >
                         Add Bulk Game </a>
                        <?php } } ?>
                        </div>
                    </div>
                    <div class="carousel-inner" role="listbox">                    
                    <?php
                    if ($CntListingGame > 0) { 
                    foreach ($GameDetail as $gameDetailYear => $GameDetailYearVal) {                        
                        foreach ($GameDetailYearVal as $GameDetailMonth => $GameDetailMonthInfo) {
                        $first_key = key($GameDetailMonthInfo); 
                        $month = date("F", mktime(0,0,0,$GameDetailMonth+1,0,0));
                        ?>
                        <div class="item <?php if ($first_key == 0) {echo "active"; }?>" month-name="<?php echo $month.", ".$gameDetailYear; ?>" >
                        <input type="hidden" id="hiddenmonth" value="<?php echo date('F', strtotime($GameDetailMonth)); ?>">
                        <?php 
                            foreach ($GameDetailMonthInfo as $Keys => $Values) { 
                                $VisitName = json_decode(getTeamName($Values['visitor_team_id']), true);
                                $HomeName = json_decode(getTeamName($Values['home_team_id']), true);
                                $DivisionName = json_decode(getDivisionName($Values['division']), true);
                                $GameType = json_decode(getGameType($Values['isLeagueGame']), true);
                                $TournamentName = ($Values['isLeagueGame']) ? $Values['isLeagueGame'] : " " ;

                                $enableModify = "display:none;";
                                if ($_SESSION["usertype"] == "team_manager") {
                                    $start_date = new DateTime(date("Y-m-d H:i:s", strtotime($Values["date"]." ".$Values["time"])));
                                    $end_date = new DateTime(date("Y-m-d H:i:s"));
                                    $interval = $start_date->diff($end_date);
                                    if ($interval->y == 0 && $interval->m == 0 && $interval->d <= 3) {
                                        $diffDay = $interval->d;
                                        $enableModify = "";
                                    }
                                }

                                $SportQry = $conn->prepare("SELECT subsports.sport_id,sports.sport_name FROM customer_subscribed_sports as subsports left join sports on subsports.sport_id=sports.sportcode where customer_id=:customer_id");
                                $SportQryArr = array(":customer_id"=>$cid);
                                $SportQry->execute($SportQryArr);
                                $FetchSportName = $SportQry->fetch(PDO::FETCH_ASSOC);
                                $sportname = $FetchSportName["sport_name"];

                                ?>

                                    <div class="panel-body rc-padding <?php echo $Values['date'];?>">
                                        <div class="row rm-margin opencloseportletcaption" >                                
                                            <div class="col-md-12 rm-padding">
                                                <div class="portlet box grey portletdown">
                                                    <div class="portlet-title">
                                                        <div class="caption captionstyle tools">
                                                            <a href="javascript:;" class="expand" id="shortgamedetail">
                                                            <?php echo $VisitName." vs ".$HomeName." "; ?><small><?php echo "&nbsp;&nbsp;&nbsp;".date("F d, Y", strtotime($Values['date'])); echo " ".$Values['time']; ?></small>
                                                            </a>
                                                        </div>
                                                        <div class="tools">
                                                            <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body portlet-custom expand opencloseportlet" >
                                                        <div class="slimScrollDiv" >
                                                            <div class="scroller removerightpadding"  data-always-visible="1" data-rail-visible="1" data-rail-color="blue" data-handle-color="red" data-initialized="1">
                                                                <div class="col-md-12 rm-padding table-responsive">
                                                                    <table class="table table-hover table-bordered gameinfotable">
                                                                        <tr>
                                                                            <th nowrap ><strong>Game Id</strong></th>
                                                                            <th nowrap><strong>Game Name</strong></th>
                                                                            <th nowrap><strong>Visitor Team</strong></th>
                                                                            <th nowrap><strong>Home Team</strong></th>
                                                                            <th nowrap><strong>Division</strong></th>
                                                                            <th nowrap><strong>Game Type</strong></th>
                                                                            <th nowrap><strong>Location</strong></th>
                                                                            <th nowrap><strong>Tournament</strong></th>
                                                                            <th nowrap><strong>Action</strong></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td nowrap><?php echo $Values['id']; ?></td>
                                                                            <td nowrap><?php echo $Values['game_name']; ?></td>
                                                                            <td nowrap><?php echo $VisitName; ?></td>
                                                                            <td nowrap><?php echo $HomeName; ?></td>
                                                                            <td nowrap><?php echo $DivisionName; ?></td>
                                                                            <td nowrap><?php echo $GameType; ?></td>
                                                                            <td nowrap><?php echo $Values['game_location']; ?></td>
                                                                            <td nowrap><?php echo $Values['tournament']; ?></td>
                                                                            <?php 
                                                                            $GameQryArr = array();
                                                                            $gameid = $Values['id'];
                                                                            $GameQry = $conn->prepare("select * from games_info where id=:gameid");
                                                                            $GameQryArr = array(":gameid"=>$gameid);
                                                                            $GameQry->execute($GameQryArr);
                                                                            $CntGame = $GameQry->rowCount();
                                                                            if ($CntGame > 0) {

                                                                                $GameRows = $GameQry->fetch(PDO::FETCH_ASSOC);
                                                                                $sportid = $GameRows['sport_id'];
                                                                            }

                                                                            $sportnquery = $conn->prepare("select * from sports where sportcode='$sportid'");
                                                                            $sportnquery->execute();
                                                                            $Cntsportnquery = $sportnquery->rowCount();
                                                                            if ($Cntsportnquery > 0) {
                                                                                $sportrows = $sportnquery->fetchAll(PDO::FETCH_ASSOC);
                                                                                foreach ($sportrows as $sportnames) {
                                                                                    $teamsportname = $sportnames['sport_name']; 
                                                                                    $teamsportname = strtolower($teamsportname);
                                                                                }
                                                                            } else {
                                                                                $teamsportname = "";
                                                                            }
                                                                            ?>
                                                                            <td nowrap>
                                                                                <div class="actions">
                                                                                    <a title="Edit game" class="btn btn-success" href="manage_game.php?gid=<?php echo base64_encode($Values['id']); ?>" class="btn btn-default btn-sm">
                                                                                        <i class="fa fa-pencil"></i> Edit 
                                                                                    </a>
                                                                                    <a title="Delete game" class="btn btn-danger" onclick="return deleteGame('<?php echo $Values['id']; ?>','<?php echo $teamsportname; ?>');">
                                                                                        <i class="fa fa-trash" ></i> Delete 
                                                                                    </a>

                                                                                    <?php if ($sportname == 'basketball') {
                                                                                        if ($_SESSION["usertype"] != "team_manager") {
                                                                                    ?>
                                                                                    <a href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="btn btn-warning modifystatsbtn">
                                                                                        <i class="fa fa-pencil" ></i> Modify 
                                                                                    </a>
                                                                                    <?php } else { ?>
                                                                                         <a style="<?php echo $enableModify; ?>" href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="btn btn-warning modifystatsbtn">
                                                                                        <i class="fa fa-pencil" ></i> Modify 
                                                                                        </a>

                                                                                    <?php } } ?>
                                                                                    <?php
                                                                                     $GameDetailsQry = $conn->prepare("select * from game_details where xml_game_id=:gameid");
                                                                                     $GameDetailsQryArr = array(":gameid"=>$gameid);
                                                                                     $GameDetailsQry->execute($GameDetailsQryArr);
                                                                                     $CntGameDetails = $GameDetailsQry->rowCount();
                                                                                     if($CntGameDetails >0){?>
                                                                                     <button type="button" class="btn btn-info combineplayer"  data-toggle="modal" data-target="#CombineModal"
                                                                                     data-id="<?php echo $gameid;?>">Combine Player</button> 
                                                                                     <?php }

                                                                                     ?>
                                                                                      
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </div>                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                
                                        </div>
                                    </div>
                            <?php } ?>
                        </div><?php
                        
                        }
                    }
                    } else {  ?> 
                        <!-- <div class="panel-body rc-padding carousel-inner ajaxcarousel-inner">
                            <div class="row">                                
                                <div class="col-md-12">
                                    <div class="portlet box grey portletdown">
                                        <div class="portlet-title">
                                            <div class="caption captionstyle">
                                                No games Found</div>                                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="panel-body rc-padding carousel-inner ajaxcarousel-inner">
                                    <div class="row">                                
                                        <div class="col-md-12">
                                            <div class="portlet box grey portletdown">
                                                <div class="portlet-title">
                                                    <div class="caption captionstyle nogamescaption" style="color:#000;background-image:none;display: block !important;width: 100%">
                                                        No games Found</div>
                                                                        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <a class="left carousel-control carouselstle" href="#myCarousel" role="button" data-slide="prev">
                        <i class='fa fa-chevron-left'></i>
                        <span class="sr-only">Previous</span>

                    </a>
                    <a class="right carousel-control carouselstle" href="#myCarousel" data-container="body" role="button" data-slide="next">
                        <i class='fa fa-chevron-right' ></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                        
            </div>
        </div>
    </div>
</div>
 <div id="CombineModal" class="modal fade" role="dialog">
  <div class="modal-dialog">                            
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Combine Player </h4>
      </div>
      <div class="modal-body">
      <form name="combineplayer" id="combineplayerfrm" method="POST" class="form-horizontal" novalidate="novalidate" action="">
       <div class="" style='margin:auto;float:none;'>
       <input type="hidden" name="combine_gameid" id="combine_gameid" value="">
            <div class="col-md-4 " style="float:none;margin:auto;">
              <table  class="table" style="margin-bottom:0em;"  align="center">
                 <tr>
                    <td style="vertical-align:middle;border:none;"><label><b>Game ID<b></label></td>
                    <td style="vertical-align:middle;border:none;font-weight:bold;" id="game_view_id"></td>
                 </tr>
               </table>                
            </div>
             <div class="col-md-10" style="float:none;margin:auto;">
                <div class="form-group col-md-12 ">
                    <label>Actual Player ID<span class="error">*</span></label>
                             <input class="form-control requiredcs border-radius" type="text" name="playerid" id="playerid" placeholder="Actual Palyer ID" />               
                </div>  
                <div class="form-group col-md-12 ">
                   
                    <label>Duplicate Player ID<span class="error">*</span></label>
                             <input class="form-control requiredcs border-radius" type="text" name="duplicateplayerid" id="duplicateplayerid" placeholder="Duplicate Palyer ID" /> 
                </div>  
                    
                <div class="form-group col-md-12 popupbtn" style="margin-top:10px">                                     
                    <input class="btn  btn-success addcombinebtn" value="Submit" type="button" style="font-size:14px;">
                    <button class="btn cancelbtn btn-danger" style="margin-left:15px;font-size:14px;" type="button" data-dismiss="modal">Cancel</button>
                </div>  
            </div> 
        
              
                               
            
        </div>
        </form>
      </div>
   </div>
  </div>
</div>
<?php include_once('footer.php'); ?>

<script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="assets/custom/css/jquery-monthpicker-ui.css" rel="stylesheet" type="text/css"> 
<script src="assets/custom/js/jquery-monthpicker-ui.js" type="text/javascript"></script>
<script src="assets/custom/js/jquery.mtz.monthpicker.js"></script>
<script src="assets/custom/js/gamelist.js" type="text/javascript"></script>
<script>
$( "#searchtext" ).keyup(function() {
    ajaxGameList()
});
</script>
