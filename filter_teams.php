<?php 
include_once('session_check.php'); 
include_once('connect.php');

if(isset($_REQUEST["HdnMode"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	$HdnMode=$_REQUEST["HdnMode"];
	$HdnPage=$_REQUEST["HdnPage"];
	$Page=1;
}
if(isset($_REQUEST['season']))
{
	$season          =  $_REQUEST['season'];
    $seasoncondn = (!empty($season)) ? " and $ls.season='$season'" : "";
	$cid             =  $_REQUEST['cid'];
	$ls              =  $_REQUEST['ls'];
	$searchbyteam    =  $_REQUEST['searchbyteam'];
    $sportid    =  $_REQUEST['sportid'];
	$searchstatus    =  $_REQUEST['status'];

?>
 <form id="team_list" name="team_list" method="post" action="">
    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
    <input type="hidden" name="hdndivid" id="hdndivid" value="">
    <input type="hidden" name="hdnsesid" id="hdnsesid" value="<?php echo $season ?>">
    <input type="hidden" name="hdnsearchteam" id="hdnsearchteam" value="<?php echo $searchbyteam ?>">
	<input type="hidden" name="hnd_status" id="hnd_status" value="<?php echo $searchstatus;?>">
        <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable customerlist-tbl" id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
            <thead>
                <tr>
                    <th nowrap> Team&nbsp;ID </th>
                    <th nowrap> Team Name </th>
                    <th nowrap> Print name </th>
                    <th nowrap> Abbr </th>
                    <th nowrap> Division </th>
                    <th nowrap> Venue </th>
					<th nowrap> Video </th>
                    <th nowrap> Team&nbsp;Logo </th>
                    <th nowrap> Action </th>
                </tr>
            </thead>
            <tbody>
            <?php 
	        if($searchstatus=="active"){
	            $status="and status='1'";
                $status1="and teams_info.status='1'";
            }
			if($searchstatus=="Inactive"){
				$status="and status='0'";
                $status1="and teams_info.status='0'";
            }
            if (!empty($season) && !empty($searchbyteam)) {   

                $dbQry = "select teams_info.* from teams_info LEFT JOIN $ls ON teams_info.id=$ls.teamcode where $ls.season='$season' and teams_info.team_name like '%$searchbyteam%' and teams_info.customer_id in ($cid) group by teams_info.id order by teams_info.team_name";
            } else if (!empty($season) && empty($searchbyteam)) {
                $dbQry = "select teams_info.* from teams_info LEFT JOIN $ls ON teams_info.id=$ls.teamcode where $ls.season='$season' and teams_info.customer_id in ($cid) group by teams_info.id order by teams_info.team_name";
            } else if (empty($season) && !empty($searchbyteam)) {
                $dbQry = "select teams_info.* from teams_info LEFT JOIN $ls ON teams_info.id=$ls.teamcode where teams_info.team_name like '%$searchbyteam%' and teams_info.customer_id in ($cid) group by teams_info.id order by teams_info.team_name";
            } else {
                $dbQry = "select teams_info.* from teams_info LEFT JOIN $ls ON teams_info.id=$ls.teamcode where teams_info.customer_id in ($cid) group by teams_info.id order by teams_info.team_name";
            }
			
            $getResQry      =   $conn->prepare($dbQry);
            $getResQry->execute();
            $getResCnt      =   $getResQry->rowCount();
            $getResQry->closeCursor();
            $TotalPages = '';
            if ($getResCnt > 0) {
                $TotalPages=ceil($getResCnt/$RecordsPerPage);
                $Start=($Page-1)*$RecordsPerPage;
                $sno=$Start+1;                                        
                $dbQry.=" limit $Start,$RecordsPerPage";
                $getResQry      =   $conn->prepare($dbQry);
                $getResQry->execute();
                $getResCnt      =   $getResQry->rowCount();

                if($getResCnt>0){
                    $getResRows     =   $getResQry->fetchAll(PDO::FETCH_ASSOC);
                    $getResQry->closeCursor();
                    $s=1;
                    foreach ($getResRows as $team) { 

                        $isSuspended=$team['isSuspended'];
                        if($isSuspended=="1"){
                            $background="background-color:#D3D3D3 !important;";

                        } else {
                            $background="";
                        }

                        ?> 

                        <tr style="<?php echo $background; ?>">
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['id'] ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['team_name']; ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['print_name'] ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['abbrevation'] ?></td>
                            <td nowrap style="<?php echo $background; ?>">
                            <?php 
                            $divid= $team['division'];
                            $orgQry = $conn->prepare("select * from customer_division where id=:divid");        
                            $QryArr = array(":divid"=>$divid);        
                            $orgQry->execute($QryArr);                                        
                            $DivName = $orgQry->fetch(PDO::FETCH_ASSOC);
                            echo $DivName['name'];
                            ?></td>
                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['stadium'] ?></td>
							<td nowrap style="<?php echo $background; ?>"><?php 
											$teamid=$team['id'];
											$custid=$Cid;
											$custvideoQry=$conn->prepare("select * from customer_tv_station where team_id=:team_id and customer_id=:customer_id");
											$custQryArr = array(":team_id"=>$teamid,":customer_id"=>$custid); 
											$custvideoQry->execute($custQryArr);                                        
                                            $Video = $custvideoQry->fetch(PDO::FETCH_ASSOC);
											$videoactive=$Video['active'];
											if($videoactive=='1'){
											$videostatus='Y';
											}else{
											$videostatus='N';
											}
											echo $videostatus;
											?></td>
                            <td nowrap style="<?php echo $background; ?>">
                            <?php if($team['team_image']!=""){ ?>
                                <img src="uploads/teams/<?php echo $team['team_image']; ?>" alt="" width="25" height="25" />
                            <?php } ?>
                            </td>
                            <td nowrap style="<?php echo $background; ?>">
                            <?php 
                            $GameQryArr = array();
                            $teamid = $team['id'];
                            $GameQry = $conn->prepare("select * from teams_info where id=:teamid");
                            $GameQryArr = array(":teamid"=>$teamid);
                            $GameQry->execute($GameQryArr);
                            $CntGame = $GameQry->rowCount();
                            if ($CntGame > 0) {

                                $GameRows = $GameQry->fetch(PDO::FETCH_ASSOC);
                                $customer_id = $GameRows['customer_id'];
                                $sportid = $GameRows['sport_id'];
                            }

                            $sportnquery = $conn->prepare("select * from sports where sportcode='$sportid'");
                            $sportnquery->execute();
                            $Cntsportnquery = $sportnquery->rowCount();
                            if ($Cntsportnquery > 0) {
                                $sportrows = $sportnquery->fetchAll(PDO::FETCH_ASSOC);
                                foreach ($sportrows as $sportnames) {
                                    $teamsportname = $sportnames['sport_name']; 
                                    $teamsportname = strtolower($teamsportname);
                                }
                            } else {
                                $teamsportname = "";
                            }
                            ?>
                                <table class="table-hover">
                                    <tr style="<?php echo $background; ?>">
                                        <td nowrap style="<?php echo $background; ?>">
                                        <a href="manage_team.php?tid=<?php echo base64_encode($team['id']); ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> Edit
                                            
                                        </a>
                                        </td >
                                        <td style="<?php echo $background; ?>">   
                                        <a  class="btn btn-xs btn-danger" onclick="return deleteTeam('<?php echo $team['id']; ?>','<?php echo $SportName; ?>');"><i class="fa fa-trash"></i> Delete
                                            
                                        </a>                                                   
                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php $s++;}
                    } 
                    else {
                        // echo "<tr><td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td></tr>";
                    }
            } else
             {
                // echo "<tr><td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td></tr>";
            }
            ?>
            </tbody>
        </table>
        
            <?php
            if($TotalPages > 1){

            echo "<tr><td style='text-align:center;' colspan='8' valign='middle' class='pagination'>";
            $FormName = "team_list";
            require_once ("paging.php");
            echo "</td></tr>";

            }
           ?>
    </div>
    </form>
<?php }?>
<script src="assets/custom/js/teamlist.js" ></script>
