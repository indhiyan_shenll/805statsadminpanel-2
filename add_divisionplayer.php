<?php 
include_once('session_check.php');
include_once('connect.php'); 
include_once('header.php'); 

$Sports = array();
$SportListArr = array();
$SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
$SportListArr = array(":customer_id"=>$Cid);
$SportsLists->execute($SportListArr);
$CntSportsLists = $SportsLists->rowCount();
if ($CntSportsLists > 0) {
    $SporstRes = $SportsLists->fetchAll(PDO::FETCH_ASSOC);
    foreach ($SporstRes as $SporstRow) {
        $Sports[]= $SporstRow['sport_id']; 
    }

    if ($Sports[0]=='4444') { $tablename='team_stats_bb'; } 
    if ($Sports[0]=='4442' || $Sports[0]=='4441') { $tablename='team_stats_ba'; } 
    if ($Sports[0]=='4443') { $tablename='team_stats_fb'; }
}

?>
<link href="assets/custom/css/addplayertoseason.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
	
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">                
                    <div class="col-md-12">
                        <div class=" left-right-padding">
                            <div class="row searchheder">                
                                <div class="col-md-12 searchbarstyle">
								<form id="addplayerform" method="POST">
									<div class="col-md-2 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group caption font-red-sunglo selecttext">
											<span class="caption-subject bold uppercase">Select season</span>
										</div>									
									</div>

									<div class="col-md-4 col-sm-3 col-xs-12">
										<div class="form-group ">											
											<select class="form-control  border-radius" name="seasonlist" id="seasonlist">
											<option value=''>Select season</option>
											<?php
											$Qry		= $conn->prepare("select * from customer_season where custid=:custid order by season_order desc");
											$Qryarr		= array(":custid"=>$customerid);
											$Qry->execute($Qryarr);
											$QryCntSeason = $Qry->rowCount();
											$DivisionWrapHtml= $AddNewSeasonTree='';
											$Inc =0;
											if ($QryCntSeason > 0) {
												while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){									
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}else{
												echo "<option value=''>No season found</option>";
											}
											?>											
											</select>
											
											<script>$("#seasonlist").val("<?php echo $_SESSION['seasonid'];?>");</script>
										</div>
									</div>

									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="conferencelist" id="conferencelist">
												<option value=''>Select conference</option>	
												<?php
													$Qry		= $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
													$Qryarr		= array(":season_id"=>$_SESSION['seasonid']);
													$Qry->execute($Qryarr);
													$QryCntSeason = $Qry->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['conference_name']."</option>";
														}
													}else{
														echo "<option value=''>No conference found</option>";
													}
												?>
											</select>
											
											<script>$("#conferencelist").val("<?php echo $_SESSION['conferenceid'];?>");</script>
										</div>									
									</div>
									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="divisionlist" id="divisionlist">
												<option value=''>Select division</option>	
												<?php
													$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
													$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid']);

													$QryExeDiv->execute($QryarrCon);
													$QryCntSeason = $QryExeDiv->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['name']."</option>";
														}
													}else{
														echo "<option value=''>No season found</option>";
													}
												?>
											</select>
											<script>$("#divisionlist").val("<?php echo $_SESSION['divisionid'];?>");</script>
										</div>									
									</div>									
									</form>
								</div>
							</div>
                        </div>
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
						<div class=" addteammainwrap">                               
							<div class="portlet-body form">
								<div class="form-body top-padding" style="padding: 0px 15px 0px 15px"> 
									<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
									<div class="row">

										<div class="col-xs-6 col-md-6" style="padding-left: 0px;">
											<div class="portlet light">
											<select name="from[]" id="undo_redo" class="form-control border-radius " size="13">
											<?php	
												$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();									
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){
														echo "<option value='".$rowTeam['team_id']."' >".$rowTeam['team_name']."</option>";
													}
												}
											?>
											</select>
											
											</div>

										</div>
										
										
										
										<div class="col-xs-6 col-md-6 rightsidewrap"  style="padding-right: 0px;">
											<div class="portlet light">
											<div id="undo_redo_to" class="form-control border-radius requiredcs" size="13" >

											</div>
											
											</div>
										</div>
									</div>
								</div> 
								
								 
							</div>
						</div>           
                    </div>                    
                
            </div>            
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT --> 
</div>

<!-- END CONTAINER -->

<!-- Division model popup -->
<div id="PlayerModal" class="modal fade large" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add Player</h4>
	  </div>
	  <div class="modal-body">
		
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>


<!-- Division model popup -->
<div id="SwitchTeamModal" class="modal fade large" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Switch Team</h4>
	  </div>
	  <div class="modal-body">
		<form id="switchteamform" method="post">
			<input type="hidden" name="playeridhid" id="playeridhid">
			<input type="hidden" name="playerexsisthid" id="playerexsistidhid" value="">

		  <div class="col-md-10" style="margin:auto;float:none;">				
				<div class="form-group col-md-12 ">
					<label>Select Team<span class="error">*</span></label>
					<select name="switchteamid" class="form-control" id="switchteamid">		
					<?php 
					if ($_SESSION['master'] == 1) { 
						$children = array($_SESSION['childrens']);					
						//$ids = join(',',$children);
						 $ids = $_SESSION['loginid'].",".join(',',$children);
						 $res = "SELECT * FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and (sport_id='$SportId') order by team_name"; 
					} else {
						$res = "select teams_info.* from teams_info  LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.customer_id in ($customerid) and team_name!='' group by teams_info.id order by teams_info.team_name";
					}
					
					$getResQry      =   $conn->prepare($res);
					$getResQry->execute();
					$getResCnt      =   $getResQry->rowCount();
					
					if ($getResCnt > 0) {
						$getResRows     =   $getResQry->fetchAll(PDO::FETCH_ASSOC);
						echo "<option value=''>Select Team</option>";
						foreach($getResRows as $team){
							echo "<option value='".$team['id']."'>".$team['team_name']."</option>";
						}
					}else{
					
					}
					?>
					</select>
					<label id="switchteamid-error" class="error" for="switchteamid">Please select team</label>
				</div>	
					
				<div class="form-group col-md-12 popupbtn">										
					<input class="btn switchteambtn btn-success" type="button" value="Save/Update">
					<button class="btn cancelbtn btn-danger" type="button" data-dismiss="modal">Cancel</button>
				</div>	
			</div>
		</form>
		 <table width='100%' id="loadingswitchtam"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Assign players to team... Please wait...</td></tr></table>
		 <table width='100%' id="teamswitchmsg"><tr><td align='center' style='font-size:15px;color:green;'>Players switched to other team successfully..</td></tr></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="SwitchUpdateModel" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn_close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Player Info</h4>
            </div>
            <div class="modal-body">
            	<div class="wrongstatus">
            	</div>
            	<form name="frm_switchteam" id="frm_switchteam" method="POST" enctype="multipart/form-data">
            		<input type="hidden" id="hnd_player_id" name="hnd_player_id"> 
            		<input type="hidden" id="hnd_teamid" name="hnd_teamid">
            		<input type="hidden" id="hnd_seasionid" name="hnd_seasionid">
            		<input type="hidden" id="hnd_divisionid" name="hnd_divisionid">
            		<input type="hidden" id="hnd_conferenceid" name="hnd_conferenceid">
            		<input type="hidden" id="hnd_image" name="hnd_image">
            		<div class="row switchupdateplayer" >
            			<div class="form-group col-md-10 form_swith">
	            			<div class="form-group col-md-6 form_uniform">
	            				<label>Uniform No</label>
	            				<input type="text" name="uniform_no" id="uniform_no" class="form-control border-radius" value="">
	            				<label id="uniformnoid-error" class="error" for="uniformnoid" style="margin-bottom:0px;"> Please enter uniform number</label>
	            				<label id="uniformid-error" class="error" for="uniformid" style="margin-bottom:0px;">Please enter number only</label>
	            			</div>
	            			<div class="col-md-6 form_position"> 
	                         	<div class="form-group">
	                             <label>Position</label>
	                             <select class='form-control border-radius' id='position' name='position'>
	                             </select>
	                             <label id="positionid-error" class="error" for="positionid" style="margin-bottom:0px;">Please select position</label>

	                           </div>
	                        </div> 
                        </div>  
            			<div class="form-group col-md-10 form_swith" style="overflow:auto">
							<label>Select Team</label>
							<select name="sel_switch" id="sel_switch" class="form-control border-radius" >
			            	</select>
						</div>
						<div class="form-group col-md-10 form_swith" style="margin-top: 15px;">
							<label>Player Image</label>
							<input type="file" name="upload_file" id="upload_file">
						</div>

						<div class="form-group col-md-10 form_swith" style="margin-top: 15px;">
                            <label>Status</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio status_radio"> Active
                                    <input type="radio" value="1" name="isactive" id="isactive-yes" checked >
                                    <span></span>
                                </label>
                                <label class="mt-radio status_radio"> In-Active
                                    <input type="radio" value="0" name="isactive" id="isactive-no">
                                    <span></span>
                                </label>
                            </div>
                        </div>
						<div class="form-group col-md-10 form_swith" style="margin-top: 10px;">			    
							<input class="btn btn-success switch_btn" type="button" value="Submit">
							<button class="btn btn-danger" type="button" data-dismiss="modal" style="margin-left: 15px;">Cancel</button>
						</div>
            		</div>
				</form>
				 <table width='100%' id="loadingplayers"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Assign players to team... Please wait...</td></tr></table>
				 <table width='100%' id="playerswichmsg"><tr><td align='center' style='font-size:15px;color:green;'>Players details updated successfully..</td></tr></table>
            </div>
			<!-- <div style="height:20px;"></div> -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<style>
#loadingplayer,#playerstsmsg,#switchteamid-error,#loadingplayers,#playerswichmsg,
#uniformnoid-error,#positionid-error, #uniformid-error{
	display:none;
}
.modal-body{
	overflow:auto;
}
#switchteamid{
	border-radius:4px !important;
}
.form_position{
	padding-right:0px;
	
} 
.form_uniform{
	padding-left:0px;
}
.switchteambtn {
	color: #ffffff;
	background-color: #549E39;
	background-image: -moz-linear-gradient(top, #A1C873, #70A62F);
	background-image: -ms-linear-gradient(top, #A1C873, #70A62F);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#A1C873), to(#70A62F));
	background-image: -webkit-linear-gradient(top, #A1C873, #70A62F);
	background-image: -o-linear-gradient(top, #A1C873, #70A62F);
	background-image: linear-gradient(top, #A1C873, #70A62F);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#A1C873', endColorstr='#70A62F', GradientType=0);
	border-color: #619126;
	border-radius: 4px !important;
	border-color: rgba(0, 0, 0, 0.25) rgba(0, 0, 0, 0.35) rgba(0, 0, 0, 0.35) rgba(0, 0, 0, 0.25);
}
#switchteamid-error,#uniformnoid-error,#positionid-error,#positionid-error,#uniformid-error{
	color:red;
}
.playerimgtbl img{
	    border-radius: 50px !Important;
}
.playerimgtbl{
    padding: 5px !important;
}
.assignplayertbl tr:nth-child(even) {
	    background-color: rgba(158, 158, 158, 0.1);
		height:50px;
}
.assignplayertbl tr:nth-child(odd) {
	        background-color: rgba(158, 158, 158, 0.04);
			height:50px;
}
.status_radio{
    margin-bottom: 0px !important;
}
.btn_managesea{
    margin-bottom: 6px !important;
}
.alert-danger {
    background-color: #fbe1e3;
    border-color: #fbe1e3;
    color: #e73d4a;
    padding: 10px;
    border-radius: 5px !important;
}

.form_swith{
	margin:auto;
	float:none;
}
label {
    font-weight: 400;
    font-weight: 600;
    margin-bottom: 10px;
}

</style>
<?php include_once('footer.php'); ?>

<script type="text/javascript" src="assets/global/plugins/multiselect.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#multiselect').multiselect({submitAllRight:false, });
});
</script>
<script>
jQuery(document).ready(function($) {

	$(document).on('click','.switchteambtn', function(evt) {
		var SwitchteamId	= $("#switchteamid").val();		
		var PlayerId	    = $("#playeridhid").val();	
		var ExsistTeamId    = $("#playerexsistidhid").val();	

		if(SwitchteamId==''){			
			$("#switchteamid-error").show();
			return false;
		}else{
			
			$("#loadingswitchtam").show();
			$("#teamswitchmsg, #switchteamform").hide();


			$.ajax({
			 type:"POST",
			 async:false,
			 url:"switchteam.php",
			 data:{teamid:SwitchteamId,playerid:PlayerId,exsistteamid:ExsistTeamId},
			 dataType:"text",
			 success: function(data) {				
				if(data=='success'){
					$("#loadingswitchtam").hide();
					$("#teamswitchmsg").show();
					$("#player_"+PlayerId).remove();
				}
			},
			error: function(data) {
				
			},
		 });	
		
		}

	});

	$(document).on('change','#switchteamid', function(evt) {
		if($(this).val()==''){
			$("#switchteamid-error").show();
			return false;
		}else{
			$("#switchteamid-error").hide();
			return false;
		}
	});

	
});

$(document).on("click",'.switchteam',function(){
	$("#loadingswitchtam").hide();
	$("#teamswitchmsg").hide();
	$("#switchteamform").show();
	$("#switchteamid-error").hide();
	$("#playeridhid").val($(this).attr("data-playerid"));
	$("#playerexsistidhid").val($(this).attr("teamid"));

	var SelectedTeamId = $("#undo_redo").val();
	var SelectedHtml   = $("#undo_redo").html();
	//$("#switchteamid").empty().append("<option value=''>Select Team</option>"+SelectedHtml);	
	$("#switchteamid option[value=" + SelectedTeamId + "]").attr("disabled",true);
	//$("#switchteamid option[value=" + SelectedTeamId + "]").attr("selected",'selected');
	$("#SwitchTeamModal").modal("show");
});


$(document).on("click",'.addnewplayerbtn',function(){ 
	$("#multiselect_to option").prop("selected", "selected");
	var TeamId			= $("#teamidhidden").val();
	var SeasonId		= $("#seasonlist").val();
	var ConferenceId	= $("#conferencelist").val();
	var DivisionId		= $("#divisionlist").val();
	var SelectedPlayer	= $("#multiselect_to").val();
	$("#PlayerModal .modal-dialog").animate({width: "40%"}, 500 );

	if(SelectedPlayer==''){
		alert("Please add player");
		return false;
	}
	$("#loadingplayer").show();
	$(".formcontainer").hide();

	var result=$.ajax({
		 type:"POST",		
		 url:"saveplayerlist.php",
		 data:{"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId,PlayersId:SelectedPlayer}, 
		 success: function(data) {
			$("#undo_redo_to").empty().append(data);
			$("#loadingplayer").hide();
			$("#playerstsmsg").show();
		},
		error: function(data) {
			alert("something wrong");
		},
	 });	
			
	
});

$(document).on("click",'.addplayerbtntop',function(){ 
	var TeamId		 = $('#undo_redo').val();
	var SeasonId	 = $("#seasonlist").val();
	var ConferenceId = $("#conferencelist").val();
	var DivisionId	 = $("#divisionlist").val();
	var post_type    = 'selectplayeroption';

	$("#PlayerModal .modal-dialog").animate({width: "70%"}, 300 );

	if(SeasonId==''){
		alert("Please select seasaon");
		return false;
	}else if(ConferenceId==''){
		alert("Please select conference");
		return false;
	}else if(DivisionId==''){
		alert("Please select division");
		return false;
	}else if((TeamId=='')||(TeamId==null)){
		alert("Please select team");
		return false;
	}

	$("#loadingplayer,#playerstsmsg").hide();
	$(".formcontainer").show();
	
	var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectplayerlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId},
		 dataType:"text",
		 success: function(data) {
			$("#multiselect_to").empty().append(data);		
			$("#PlayerModal").modal("show");
		},
		error: function(data) {
			alert("something wrong");
		},
	 });
		
});
$(document).on("change",'#undo_redo',function(){ 
    var TeamId		 = $(this).val();
	var SeasonId	 = $("#seasonlist").val();
	var ConferenceId = $("#conferencelist").val();
	var DivisionId	 = $("#divisionlist").val();
	
	if (!$("#addplayerform").validate()) { 		
		return false;
	} 	
	$("#undo_redo_to").empty().append("<img src='images/loading-publish.gif'>");

	var post_type='selectplayertbl';

	var result=$.ajax({
		 type:"POST",
		 //async:false,
		 url:"selectplayerlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId},
		 
		 success: function(data) {
		   var DataArr = [];		
		   DataArr = data.split('#####');
		  
		   $("#undo_redo_to").empty().append(DataArr[0]);
		   $("#PlayerModal .modal-body").empty().append(DataArr[1]);
		 },
		 error: function(data) {
			alert("something wrong");
			
		 },
	 }); 	
		
	});


$(document).ready(function() {
	$('#addplayer').multiselect({
		search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>',
        },
	});

	$('#undo_redo').multiselect({
		sort:false,
        search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>',
        },
		moveToRight:function($left, $right, $options) { skipStack:false;},
		moveToLeft: function($left, $right, $options) {skipStack:false; }
    });


});

$(document).on('change','#seasonlist,#conferencelist,#divisionlist', function(evt) {
		var $this          =  $(this);	
		var SelectAttrid   =  $this.attr("id");
		var post_type      =  '';
		var SeasonId       =  '';	
		var ConferenceId   =  '';
		var DivisionId     =  '';

		if(SelectAttrid=="seasonlist"){
			if($(this).val()!=''){
				post_type      =  "seasonlist";
				SeasonId       =  $('#seasonlist').val();		
			}
		}else if(SelectAttrid=="conferencelist"){
			post_type      =  "conferencelist";
			SeasonId       =  $('#seasonlist').val();
			ConferenceId   =  $('#conferencelist').val();
			
		}else if(SelectAttrid=="divisionlist"){
			post_type      =  "divisionlist";
			SeasonId       =  $('#seasonlist').val();	
			ConferenceId   =  $('#conferencelist').val();
			DivisionId     =  $('#divisionlist').val();			
		}
		
		if(SeasonId!=''){
			var result=$.ajax({
				 type:"POST",				 
				 url:"selectteamlist.php",
				 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},		 
				 success: function(data) {
				   if(post_type=="seasonlist"){					
						$('#conferencelist').empty().append(result.responseText);
						$('#divisionlist').empty().append("<option value=''>Select</option>");
					}else if(post_type=="conferencelist"){					
						$('#divisionlist').empty().append(result.responseText);
						
					}else{
						$('.addteammainwrap').empty().append(result.responseText);
					}	
				 },
				 error: function(data) {
					alert("something wrong");
					
				 },
			 }); 			
		}
	
});


$(document).on('click','.removeplayerteam', function(evt) {
	if(!confirm("Are you sure want remove this player?")){
		return false;	
	}else{
		var $this = $(this);
		var PlayerId  = $(this).attr("date-playerid");
		var TeamId     = $(this).attr("teamid");
		$.ajax({
			 type:"POST",				 
			 url:"removeassignedplayer.php",
			 data:{"playerid":PlayerId,"teamid":TeamId},				 
			 success: function(data) {
				$this.closest("tr").remove();
				return false;
			 },
			 error: function(data) {
				alert("something wrong");
				return false;
			 },
		 }); 
	}
});


$(document).on("click",".updateplayer",function(){
    
    var selectedOption=$("#undo_redo").val();
	var selectedhtml =$("#undo_redo").html();
    var PlayerId  = $(this).attr("data-playerid");
    var teamid     = $(this).attr("teamid");
    var seasionid  = $(this).attr("seasionid");
    var divisionid  = $(this).attr("divisionid");
    var conferenceid  = $(this).attr("conferenceid");
    if(PlayerId!=""){
        $.ajax({
            url:"updateswitch_playerinfo.php",  
            method:'POST',
            data:{PlayerId:PlayerId,post_type:'updateswitch'},
            success:function(data) {
             var DataArr  = data.split("##^^##");
	            if(DataArr[0]=='Success'){ 
	              	var uniform_no=DataArr[1];
	              	var postion=DataArr[2];
	              	var postion_val=DataArr[3];
	              	var image_url=DataArr[4];
					$("#uniform_no").val(uniform_no);
					$("#position").html(postion);
					$("#hnd_player_id").val(PlayerId);
					$("#hnd_teamid").val(teamid);
					$("#hnd_seasionid").val(seasionid);
					$("#hnd_divisionid").val(divisionid);
					$("#hnd_conferenceid").val(conferenceid);
					$("#sel_switch").empty().html(selectedhtml);
					$('#sel_switch').prop('disabled', true);
					$("#hnd_image").val(image_url);
                    $("#sel_switch").val(selectedOption);
					$("#SwitchUpdateModel").modal("show");
	            } 
	            else {
                	$('.wrongstatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close" aria-hidden="true">&times;</a><strong>Something wrong</strong>');
                }
	        }
          });
    }
	
});
$(document).on("click",".switch_btn",function(){
	var $this  = $(this);
	var form_data	   =   $("#frm_switchteam").serialize();
	var player_id 	   =   $('input[name="hnd_player_id"]').val();
	var uniform_no 	   =   $('input[name="uniform_no"]').val();
	var postion 	   =   $('#postion').val();
	var upload_file_db  =	$('input[name="hnd_image"]').val();
	var info_form_data =   new FormData();
	var info_file_data =   $('#upload_file').prop('files')[0]; 
	info_form_data.append('info_file', info_file_data);
	info_form_data.append('form_data', form_data);
	info_form_data.append('post_type', 'manage_upadte_switch');
	info_form_data.append('upload_file_db', upload_file_db);
	info_form_data.append('player_id', player_id);
	$(".switchupdateplayer").show();
	$("#playerswichmsg").hide();
	$(".playerswichmsg").css("display","none");
	var regex = /^[0-9\b]+$/; 
	if(uniform_no==''){			
		$("#uniformnoid-error").show();
		return false;
	} else if(!regex.test(uniform_no)){
		$("#uniformid-error").show();
		return false;
	}
	else if(postion==''){
		$("#uniformnoid-error").hide();
		$("#positionid-error").show();
			return false;
	} else {
		$("#uniformnoid-error").hide();
		$("#uniformnid-error").hide();
		$("#positionid-error").hide();
	}
	jQuery.ajax({
			url:'updateswitch_playerinfo.php',
			type: "post",
			cache: false,
			contentType: false,
			processData: false,
			data: info_form_data,                         
			async:false,
			success: function(response){
				//alert(response);
				var responseArr = response.split("###");
				if(responseArr[0] == "success"){
					$("#player_"+responseArr[2]).find(".playerimgtbl").empty().append(responseArr[1]);
					$("#loadingplayers").hide();
		            $("#playerswichmsg").show();
		            $(".switchupdateplayer").hide();
		            $("#frm_switchteam")[0].reset();
		            if(responseArr[3]=="0"){
		            	$("#player_"+responseArr[2]).remove();
		            }
                } else {
                	$('.wrongstatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something wrong</strong>');
                }
			},
			error:function(){
				$("#frm_switchteam")[0].reset();
			}                
		});       
	

});
$(document).on("click",".btn_close",function(){
   $("#playerswichmsg").hide();
   $(".switchupdateplayer").show();
});

$(document).on('change','#uniform_no', function(evt) {
	    var regex = /^[0-9\b]+$/; 
	    var uniform=$(this).val();
		if($(this).val()==''){
			$("#uniformnoid-error").show();
			$("#uniformid-error").hide();
			return false;
		} else if (!regex.test(uniform)){
			$("#uniformnoid-error").hide();
			$("#uniformid-error").show();
		    return false;
		}else{
			$("#uniformnoid-error").hide();
			$("#uniformid-error").hide();
			return false;
		}
});
$(document).on('change','#position', function(evt) {

		if($(this).val()==''){
			$("#positionid-error").show();
			return false;
		}else{
			$("#positionid-error").hide();
			return false;
		}
});
</script>
   