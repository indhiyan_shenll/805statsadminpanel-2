<?php 
include_once('session_check.php');
include_once('connect.php'); 
include_once('header.php'); 

?>
<link href="assets/custom/css/addplayertoseason.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
	
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">                
                    <div class="col-md-12">
                        <div class=" left-right-padding">
                            <div class="row searchheder">                
                                <div class="col-md-12 searchbarstyle">
								<form id="addplayerform" method="POST">
									<div class="col-md-2 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group caption font-red-sunglo selecttext">
											<span class="caption-subject bold uppercase">Select season</span>
										</div>									
									</div>

									<div class="col-md-4 col-sm-3 col-xs-12">
										<div class="form-group ">											
											<select class="form-control  border-radius" name="seasonlist" id="seasonlist">
											<option value=''>Select season</option>
											<?php
											$Qry		= $conn->prepare("select * from customer_season where custid=:custid order by season_order desc");
											$Qryarr		= array(":custid"=>$customerid);
											$Qry->execute($Qryarr);
											$QryCntSeason = $Qry->rowCount();
											$DivisionWrapHtml= $AddNewSeasonTree='';
											$Inc =0;
											if ($QryCntSeason > 0) {
												while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){									
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}else{
												echo "<option value=''>No season found</option>";
											}
											?>											
											</select>
											
											<script>$("#seasonlist").val("<?php echo $_SESSION['seasonid'];?>");</script>
										</div>
									</div>

									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="conferencelist" id="conferencelist">
												<option value=''>Select conference</option>	
												<?php
													$Qry		= $conn->prepare("select * from cust_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
													$Qryarr		= array(":season_id"=>$_SESSION['seasonid']);
													$Qry->execute($Qryarr);
													$QryCntSeason = $Qry->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['conference_name']."</option>";
														}
													}else{
														echo "<option value=''>No conference found</option>";
													}
												?>
											</select>
											
											<script>$("#conferencelist").val("<?php echo $_SESSION['conferenceid'];?>");</script>
										</div>									
									</div>
									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="divisionlist" id="divisionlist">
												<option value=''>Select division</option>	
												<?php
													$QryExeDiv = $conn->prepare("select * from cust_season_conf_div as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
													$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid']);

													$QryExeDiv->execute($QryarrCon);
													$QryCntSeason = $QryExeDiv->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['name']."</option>";
														}
													}else{
														echo "<option value=''>No season found</option>";
													}
												?>
											</select>
											<script>$("#divisionlist").val("<?php echo $_SESSION['divisionid'];?>");</script>
										</div>									
									</div>									
									</form>
								</div>
							</div>
                        </div>
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
						<div class=" addteammainwrap">                               
							<div class="portlet-body form">
								<div class="form-body top-padding" style="padding: 15px 15px 0px 15px"> 
									<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
									<div class="row">

										<div class="col-xs-6 col-md-6" style="padding-left: 0px;">
											<div class="portlet light">
											<select name="from[]" id="undo_redo" class="form-control border-radius " size="13">
											<?php	
												$QryExeTeam = $conn->prepare("select * from cust_season_conf_div_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();									
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){
														echo "<option value='".$rowTeam['team_id']."' >".$rowTeam['team_name']."</option>";
													}
												}
											?>
											</select>
											
											</div>

										</div>
										
										
										
										<div class="col-xs-6 col-md-6 rightsidewrap"  style="padding-right: 0px;">
											<div class="portlet light">
											<div id="undo_redo_to" class="form-control border-radius requiredcs" size="13" multiple="multiple">
											
											<?php											

												$QryExeTeam = $conn->prepare("select * from cust_season_conf_div_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();										
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){				
														//echo "<option value='".$rowTeam['id']."'>".$rowTeam['team_name']."</option>";
													}
												}
											?>

											</div>
											
											</div>
										</div>
									</div>
								</div> 
								
								 
							</div>
						</div>           
                    </div>                    
                
            </div>            
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT --> 
</div>

<!-- END CONTAINER -->

<!-- Division model popup -->
<div id="PlayerModal" class="modal fade large" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add Player</h4>
	  </div>
	  <div class="modal-body">
	  <form name="adddivision" id="playerfrm" method="POST" class="form-horizontal" novalidate="novalidate">
		<input type="hidden" name="teamid" id="teamidhidden" /> 
		<div class="col-md-12 formcontainer" style='margin:auto;float:none;'>
			<div class="row">
				<div class="col-sm-5">
					<select name="from[]" id="multiselect" class="form-control" size="8" multiple="multiple">
						<?php
							$Qry		= $conn->prepare("select * from player_info where customer_id=:custid and isActive=1 and lastname!='TEAM'");
							$Qryarr		= array(":custid"=>$customerid);
							$Qry->execute($Qryarr);
							$QryCntSeason = $Qry->rowCount();							
							$Inc =0;
							if ($QryCntSeason > 0) {
								while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){									
									echo "<option value='".$row['id']."'>".$row['firstname']." ".$row['lastname']."</option>";
								}
							}else{
								echo "<option value=''>No season found</option>";
							}
						?>	
					</select>
				</div>
				
				<div class="col-sm-2">
					<button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
					<button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
					<button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
					<button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
				</div>
				
				<div class="col-sm-5">
					<select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple"></select>
			 
					<div class="row">
						<div class="col-sm-6">
							<button type="button" id="multiselect_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
						</div>
						<div class="col-sm-6">
							<button type="button" id="multiselect_move_down" class="btn btn-block col-sm-6" style="float: left;"><i class="glyphicon glyphicon-arrow-down" ></i></button>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="form-group col-md-12 ">										
				<input class="btn addnewplayerbtn" type="button" value="Submit">
				<button class="btn cancelbtn" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
		<table width='100%' id="loadingplayer"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Assign players to team... Please wait...</td></tr></table>

		<table width='100%' id="playerstsmsg"><tr><td align='center' style='font-size:15px;color:green;'>Players assigned to team successfully..</td></tr></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="SwitchUpdateModel" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Switch Team</h4>
            </div>
            <div class="modal-body">
            	<div class="wrongstatus">
            	</div>
            	<form name="frm_switchteam" id="frm_switchteam" method="POST" enctype="multipart/form-data">
            		<input type="hidden" id="hnd_player_id" name="hnd_player_id"> 
            		<input type="hidden" id="hnd_image" name="hnd_image">
            		<div class="row formcontainers" >
            			<div class="form-group col-md-10" style="margin:auto;float:none;margin-bottom: 15px;"><label>Uniform No</label><input type="text" name="uniform_no" id="uniform_no" class="form-control border-radius" value=""></div>
            			<div class="form-group col-md-10" style="margin:auto;float:none;margin-bottom: 15px;">
                         	<div class="form-group">
                             <label>Position</label>
                             <select class='form-control border-radius' id='position' name='position'>
                             </select>
                           </div>
                        </div>   
            			<div class="form-group col-md-10" style="margin:auto;float:none;">
							<label>Select Team</label>
							<select name="sel_switch" id="sel_switch" class="form-control border-radius" >
			            	</select>
						</div>
						<div class="form-group col-md-10" style="margin:auto;float:none;margin-top: 15px;">
							<label>Player Image</label>
							<input type="file" name="upload_file" id="upload_file">
						</div>
						<div class="form-group col-md-10" style="margin:auto;float:none;margin-top: 15px;">			    <input class="btn btn-success switch_btn" type="button" value="Submit">
							<button class="btn btn-danger" type="button" data-dismiss="modal" style="margin-left: 15px;">Cancel</button>
						</div>
            		</div>
				</form>
				 <table width='100%' id="loadingplayers"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Assign players to team... Please wait...</td></tr></table>
				 <table width='100%' id="playerswichmsg"><tr><td align='center' style='font-size:15px;color:green;'>Players assigned to team successfully..</td></tr></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<style>
#loadingplayer,#playerstsmsg,#loadingplayers,#playerswichmsg{
	display:none;
}

.modal-body{
	overflow:auto;
}
</style>
<?php include_once('footer.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="assets/global/plugins/multiselect.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

 <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#multiselect').multiselect({
		submitAllRight:false, 		
	});
});
</script>
<script>


$(document).on("click",'.addnewplayerbtn',function(){ 
	$("#multiselect_to option").prop("selected", "selected");
	var TeamId			= $("#teamidhidden").val();
	var SeasonId		= $("#seasonlist").val();
	var ConferenceId	= $("#conferencelist").val();
	var DivisionId		= $("#divisionlist").val();
	var SelectedPlayer	= $("#multiselect_to").val();
	
	if(SelectedPlayer==''){
		alert("Please add player");
		return false;
	}
	$("#loadingplayer").show();
	$(".formcontainer").hide();

	var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"saveplayerlist.php",
		 data:{"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId,PlayersId:SelectedPlayer},
		 dataType:"text",
	 }); 
	if(result.statusText=="OK"){
		$("#loadingplayer").hide();
		$("#playerstsmsg").show();
	}
		 
	
	console.log(result);
});

$(document).on("click",'.addplayerbtntop',function(){ 
	if($("#teamidhidden").val()==''){
		alert("Please select team");
		return false;
	}
	
	$("#addplayerform").submit();
	$("#loadingplayer").hide();
	$(".formcontainer").show();
	var TeamId		 = $('#undo_redo').val();
	var SeasonId	 = $("#seasonlist").val();
	var ConferenceId = $("#conferencelist").val();
	var DivisionId	 = $("#divisionlist").val();
	var post_type    = 'selectplayeroption';

	var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectplayerlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId},
		 dataType:"text",
	 });
		 console.log(result.responseText);
	
	 $("#multiselect_to").append(result.responseText);
	
	$("#PlayerModal").modal("show");

});
$(document).on("change",'#undo_redo',function(){ 
    var TeamId		 = $(this).val();
	var SeasonId	 = $("#seasonlist").val();
	var ConferenceId = $("#conferencelist").val();
	var DivisionId	 = $("#divisionlist").val();
	$("#teamidhidden").val(TeamId);

	if (!$("#addplayerform").validate()) { 		
		return false;
	} 	
	$("#undo_redo_to").empty().append("<img src='images/loading-publish.gif'>");
	var post_type='selectplayertbl';

	var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectplayerlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId},
		 dataType:"text",
	 }); 	
			
	if(result.statusText=="OK"){
		$("#undo_redo_to").empty().append(result.responseText);
	}


});


$(document).ready(function() {
	$('#addplayer').multiselect();

	$('#undo_redo').multiselect({
		sort:false,
        search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>',
        },
		moveToRight:function($left, $right, $options) { skipStack:false;},
		moveToLeft: function($left, $right, $options) {skipStack:false; }
    });


$.validator.addMethod('requiredcs',function(value){
	if(value==''){
		return false;
	}else{
		return true;
	}
},""
);


$("#addplayerform").validate({
	 rules: {
		 conferencelist:{requiredcs :true},
		 divisionlist:{requiredcs:true},
		 //"selectedteam[]":{requiredcs:true},
		 },
		messages: {
		 conferencelist:{requiredcs:"Please select conference"},
		 divisionlist:{requiredcs:"Please select division"},
		 //"selectedteam[]":{requiredcs:"Please add team"},
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			
			/*$( ".page-content").load( "assignteamtoseason.php?"+FormArr, function( response, status, xhr ) {	
				if(status=="success"){
					
				}else{
					
				}								
			});*/

		 }
	});

});

$(document).on('change','#seasonlist,#conferencelist,#divisionlist', function(evt) {
		var $this          =  $(this);	
		var SelectAttrid   =  $this.attr("id");
		var post_type      =  '';
		var SeasonId       =  '';	
		var ConferenceId   =  '';
		var DivisionId     =  '';

		if(SelectAttrid=="seasonlist"){
			if($(this).val()!=''){
				post_type      =  "seasonlist";
				SeasonId       =  $('#seasonlist').val();		
			}
		}else if(SelectAttrid=="conferencelist"){
			post_type      =  "conferencelist";
			SeasonId       =  $('#seasonlist').val();
			ConferenceId   =  $('#conferencelist').val();
			
		}else if(SelectAttrid=="divisionlist"){
			post_type      =  "divisionlist";
			SeasonId       =  $('#seasonlist').val();	
			ConferenceId   =  $('#conferencelist').val();
			DivisionId     =  $('#divisionlist').val();
			
		}
		
		if(SeasonId!=''){

			var result=$.ajax({
				 type:"POST",
				 async:false,
				 url:"selectteamlist.php",
				 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},
				 dataType:"text",
			 }); 	
			
			if(result.statusText=="OK"){
				if(post_type=="seasonlist"){					
					$('#conferencelist').empty().append(result.responseText);
					$('#divisionlist').empty().append("<option value=''>Select</option>");
				}else if(post_type=="conferencelist"){					
					$('#divisionlist').empty().append(result.responseText);
					
				}else{
					$('.addteammainwrap').empty().append(result.responseText);
				}
				return false;
				
			}else{
				return true;
			}		
		}else{
			return false;
		}
	
});


$(document).on('click','.removeplayerteam', function(evt) {
	if(!confirm("Are you sure want remove?")){
		return false;	
	}else{
		var PlayerId  = $(this).att("date-playerid");
		var result=$.ajax({
				 type:"POST",
				 async:false,
				 url:"deleteplayer.php",
				 data:{"PlayerId":playerid},
				 dataType:"text",
			 }); 
	}
	console.log(result);
});


$(document).on("click",".updateplayer",function(){
    
    var selectedOption=$("#undo_redo").val();
	var selectedhtml =$("#undo_redo").html();
    
    var PlayerId  = $(this).attr("data-playerid");
    if(PlayerId!=""){
        $.ajax({
            url:"updateswitch_playerinfo.php",  
            method:'POST',
            data:{PlayerId:PlayerId,post_type:'updateswitch'},
            success:function(data) {
             var DataArr  = data.split("##^^##");
	            if(DataArr[0]=='Success'){ 
	              	var uniform_no=DataArr[1];
	              	var postion=DataArr[2];
	              	var postion_val=DataArr[3];
	              	var image_url=DataArr[4];
					$("#uniform_no").val(uniform_no);
					$("#position").html(postion);
					$("#hnd_player_id").val(PlayerId);
					$("#sel_switch").empty().html(selectedhtml);
					$("#hnd_image").val(image_url);
                    $("#sel_switch").val(selectedOption);
					$("#SwitchUpdateModel").modal("show");
	            } 
	            else {
                	$('.wrongstatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something wrong</strong>');
                }
	        }
          });
    }
	
});
$(document).on("click",".switch_btn",function(){
	var form_data	   =   $("#frm_switchteam").serialize();
	var player_id 	   =   $('input[name="hnd_player_id"]').val();
	var upload_file_db  =	$('input[name="hnd_image"]').val();
	var info_form_data =   new FormData();
	var info_file_data =   $('#upload_file').prop('files')[0]; 
	info_form_data.append('info_file', info_file_data);
	info_form_data.append('form_data', form_data);
	info_form_data.append('post_type', 'manage_upadte_switch');
	info_form_data.append('upload_file_db', upload_file_db);
	info_form_data.append('player_id', player_id);
	$(".formcontainers").show();
	$(".playerswichmsg").css("display","none");
	jQuery.ajax({
			url:'updateswitch_playerinfo.php',
			type: "post",
			cache: false,
			contentType: false,
			processData: false,
			data: info_form_data,                         
			async:false,
			success: function(response){
				//console.log(response);
				if(response == "success"){
					$("#loadingplayers").hide();
		            $("#playerswichmsg").show();
		            $(".formcontainers").hide();
		            $("#frm_switchteam")[0].reset();
                } else {
                	$('.wrongstatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something wrong</strong>');
                }
			},
			error:function(){
				$("#frm_switchteam")[0].reset();
			}                
		});       
	

});
$(document).on("click",".btn_close",function(){
   $("#playerswichmsg").hide();
   $(".formcontainers").show();
});
</script>
   