<?php
include_once('session_check.php');
include_once('connect.php'); 
unset($_SESSION['divisionid']);
unset($_SESSION['seasonid']);
unset($_SESSION['conferenceid']);

if(isset($_POST['seasonid'])){
	$SeasonId      = $_POST['seasonid'];
	$PostType      = $_POST['post_type'];
	$_SESSION['seasonid']  = $SeasonId;
	
	
	$SeasonOptions = '';
	if($PostType == "seasonlist"){
		$_SESSION['conferenceid']	= '';		
		$_SESSION['divisionid']		= '';

		$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
		$Qryarr = array(":season_id"=>$SeasonId);
		$QryExe->execute($Qryarr);
		$QryCntSeasonconf	= $QryExe->rowCount();											
		
		if ($QryCntSeasonconf > 0) {
			$SeasonOptions     .= "<option value=''>Select conference</option>";
			while ($rowSeason   = $QryExe->fetch(PDO::FETCH_ASSOC)){				
				$SeasonOptions .= "<option value='".$rowSeason['conference_id']."'>".$rowSeason['conference_name']."</option>";
			}
		}else{
			$SeasonOptions   ="<option value=''>No conference found</option>";
		}
	}else if($PostType == "conferencelist"){
		$SeasonId					= $_POST['seasonid'];
		$conferenceid				= $_POST['conferenceid'];
		$_SESSION['conferenceid']	= $conferenceid;		
		$_SESSION['seasonid']		= $SeasonId;

		$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
		$QryarrCon = array(":conference_id"=>$conferenceid,":season_id"=>$SeasonId);

		$QryExeDiv->execute($QryarrCon);
		$QryCntSeason = $QryExeDiv->rowCount();
		$DivisionWrapHtml= $AddNewSeasonTree='';
		$Inc =0;
		if ($QryCntSeason > 0) {
			$SeasonOptions     .= "<option value=''>Select division</option>";
			while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
				$SeasonOptions     .= "<option value='".$row['id']."'>".$row['name']."</option>";
			}
		}else{
			$SeasonOptions     = "<option value=''>No season found</option>";
		}
	
	}else if($PostType == "divisionlist"){
		$conferenceid				= $_POST['conferenceid'];		
		$divisionid					= $_POST['divisionid'];
		$_SESSION['divisionid']		= $divisionid;	
		$_SESSION['conferenceid']	= $conferenceid;
		
		$QryExe1		= $conn->prepare("select * from customer_division_team where customer_id=:customer_id and conference_id=:conference_id and division_id!=:divisionid and season_id=:season_id");

		$Qryarr = array(":customer_id"=>$customerid,":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":divisionid"=>$_SESSION['divisionid']);

		$QryExe1->execute($Qryarr);
		$QryCntTeam = $QryExe1->rowCount();
		$TeamIdArr  = array();

		if ($QryCntTeam > 0) {
			while ($rowTeam = $QryExe1->fetch(PDO::FETCH_ASSOC)){	
				$TeamIdArr[] = $rowTeam['team_id'];
			}
		}
		
		?>
		                             
			<div class="row">                
				<div class="col-md-12">                              
					<div class="portlet-body form">
						<div class="form-body top-padding" style="padding-top:5px;"> 
							<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
							<div class="row">
								<div class="col-xs-5 col-md-5">
									
									<select name="from[]" id="undo_redo" class="form-control border-radius " size="13" multiple="multiple">
									<?php
									$QryExe1		= $conn->prepare("select * from customer_division_team where customer_id=:customer_id and conference_id=:conference_id and division_id!=:divisionid and season_id=:season_id");

									$Qryarr = array(":customer_id"=>$customerid,":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":divisionid"=>$_SESSION['divisionid']);

									$QryExe1->execute($Qryarr);
									$QryCntTeam = $QryExe1->rowCount();
									$TeamIdArr  = array();

									if ($QryCntTeam > 0) {
										while ($rowTeam = $QryExe1->fetch(PDO::FETCH_ASSOC)){	
											$TeamIdArr[] = $rowTeam['team_id'];
										}
									}
									$QryExe1		= $conn->prepare("select * from teams_info where customer_id=:custid");
									$Qryarr		= array(":custid"=>$customerid);
									$QryExe1->execute($Qryarr);
									$QryCntSeason = $QryExe1->rowCount();
									$DivisionWrapHtml= $AddNewSeasonTree='';
									$Inc =0;
									$AssignedTeams ='';
									if ($QryCntSeason > 0) {
										while ($row = $QryExe1->fetch(PDO::FETCH_ASSOC)){									
											
											if(in_array($row['id'],$TeamIdArr)){ 
												if($row['team_name']!=''){
													$AssignedTeams .= "<option value='".$row['id']."' disabled>".$row['team_name']."</option>";
												}
											}else{
												if($row['team_name']!=''){												
													echo "<option value='".$row['id']."'>".$row['team_name']."</option>";
												}
											}
										}
										echo $AssignedTeams;
									}else{
										echo "<option value=''>No team found</option>";
									}
									?>	
									</select>
								</div>
								
								<div class="col-xs-2 col-md-2 centeredbtnswrap">
									<!-- <button type="button" id="undo_redo_undo" class="btn btn-primary btn-block">undo</button> -->
									<button type="button" id="undo_redo_rightAll" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-forward"></i></button>
									<button type="button" id="undo_redo_rightSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
									<button type="button" id="undo_redo_leftSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
									<button type="button" id="undo_redo_leftAll" class="btn btn-default btn-block"><i class="glyphicon glyphicon-backward"></i></button>
									<!-- <button type="button" id="undo_redo_redo" class="btn btn-warning btn-block">redo</button> -->
								</div>
								
								<div class="col-xs-5 col-md-5 rightsidewrap" >									
									<select name="selectedteam[]" id="undo_redo_to" class="form-control border-radius requiredcs" size="13" multiple="multiple">
									<option value="" class="emptyselected"></option>
									<?php											

										$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
										$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

										$QryExeTeam->execute($QryarrCon);
										$QryCntSeason = $QryExeTeam->rowCount();										
										
										if ($QryCntSeason > 0) {
											while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){
												if($rowTeam['team_name']!='')
												echo "<option value='".$rowTeam['id']."'>".$rowTeam['team_name']."</option>";
											}
										}
										
									?>

									</select>
									<div class="row">
										<div class="col-sm-6">
											<button type="button" id="undo_redo_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
										</div>
										<div class="col-sm-6">
											<button type="button" id="undo_redo_move_down" class="btn btn-block col-sm-6"><i class="glyphicon glyphicon-arrow-down"></i></button>
										</div>
									</div>

								</div>
							</div>
						</div> 
						
						 <div class="">
								<button type="button" class="btn green-meadowsave" name="addsubmit" id="addteambtnid">Save</button>
								<button type="button" class="btn red" id="cancelbtn">Cancel</button>
						 </div>   
					</div>					           
				</div> 
            </div>  
			
			<script>
			$(document).ready(function() {
				$('#undo_redo').multiselect({
					sort:false,
					search: {
						left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
						right: '<p class="clearfix" style="margin-top:0px;margin-bottom: 3px;"><a href="add_divisionplayer.php"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Assign Players</button></a></p><p class="clearfix" style="margin-top:0px;margin-bottom: 0px;"><label>Selected Team</label></p>',
					},
					afterMoveToRight: function($left, $right, $options) { }
				});


			$(document).on('click','#addteambtnid', function(evt) {
				if (!$("#addteamform").validate()) { 
					return false;
				} 	
				$("#addteamform").submit();		
			});


			$.validator.addMethod('requiredcs',function(value){
				if(value==''){
					return false;
				}else{
					return true;
				}
			},""
			);


			$("#addteamform").validate({
				 rules: {
					 conferencelist:{requiredcs :true},
					 divisionlist:{requiredcs:true},
					 "selectedteam[]":{requiredcs:true},
					 },
					messages: {
					 conferencelist:{requiredcs:"Please select conference"},
					 divisionlist:{requiredcs:"Please select division"},
					 "selectedteam[]":{requiredcs:"Please add team"},
				   },
					submitHandler: function (form) {			
						var $form = $(form);
						var FormArr  = $form.serialize();	
						console.log(FormArr);
						//$form.hide(FormArr);
						$( ".page-content").load( "assignteamtoseason.php?"+FormArr, function( response, status, xhr ) {	
							if(status=="success"){
								
							}else{
								
							}								
						});

					 }
				});

			});

			
			</script>
   

		<?php
	}
	echo $SeasonOptions;
}
?>