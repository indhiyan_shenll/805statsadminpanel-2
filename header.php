<?php 
include_once('session_check.php'); 
include_once("connect.php");

$Current_PHP_File = basename($_SERVER['SCRIPT_NAME']);
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$urlArr= explode('/',$actual_link);

$Cid = "";
if (!empty($_SESSION['loginid']))  {

    if ($_SESSION['usertype'] == 'user' || $_SESSION['usertype'] == 'admin' || $_SESSION['usertype'] == 'team_manager') {
        $Cid = $_SESSION['loginid']; 
    } else { 
        header('Location:login.php');
        exit;
    }
} else {
    header('Location:login.php');
    exit;
} 
// $Cid = 10;
$Sports = array();
$SportListArr = array();
$SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
// $SportsLists = $conn->prepare("select * from customer_subscribed_sports inner join sports on customer_subscribed_sports.sport_id=sports.sportcode where customer_id=10 ORDER BY `customer_id` ASC");
$SportListArr = array(":customer_id"=>$Cid);
$SportsLists->execute($SportListArr);
$CntSportsLists = $SportsLists->rowCount();
if ($CntSportsLists > 0) {
    $SporstRes = $SportsLists->fetchAll(PDO::FETCH_ASSOC);
    foreach ($SporstRes as $SporstRow) {
        $Sports[]= $SporstRow['sport_id'];
    }
}

// $Sports[] = array('4444','4442');

// if (in_array('4444',$Sports)) {
//     $EnableSport = "basketball";
//     $SportImg = "basketball";
// } elseif (in_array('4443',$Sports)) {
//      $EnableSport = "football";
//      $SportImg = "football";
// } elseif (in_array('4441',$Sports)) {
//      $EnableSport = "baseball";
//      $SportImg = "baseball";
// } elseif (in_array('4442',$Sports)) {
//      $EnableSport = "softball";
//      $SportImg = "softball";
// } elseif (in_array('4445',$Sports)) {
//      $EnableSport = "soccer";
//      $SportImg = "soccer";
// } elseif (in_array('4446',$Sports)) {
//      $EnableSport = "volleyball";
//      $SportImg = "volleyball";
// }

?>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>805 Stats</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #2 for boxed page layout"
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<script src="assets/global/plugins/bootbox.min.js" type="text/javascript"></script>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

        <!-- END THEME GLOBAL STYLES --> 

		<!-- TABLE CSS START HERE-->
		  <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<!-- TABLE CSS END HERE-->

        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout2/css/themes/grey.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/custom/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png"> 
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<style>
		@media (min-width: 992px){
		.page-boxed .page-container {
			background-color: #4d5b69;
		}	
		
		}
	</style>
    </head>
    <!-- END HEAD -->

<!--   <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-boxed"> 
 -->  <body class="page-sidebar-closed-hide-logo page-container-bg-solid page-boxed page-header-fixed"> 
<!--    <body class="page-sidebar-closed-hide-logo page-container-bg-solid page-boxed page-header-fixed page-sidebar-fixed">-->
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <img src="assets/layouts/layout2/img/805_stats_logo.png" alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
               
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">  
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!-- <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" src="assets/layouts/layout2/img/aba-logo.png" />
                                    <span class="username username-hide-on-mobile"> American Basketball Association  </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>                               
                            </li> -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!-- <img alt="" src="assets/layouts/layout2/img/aba-logo.png" /> -->
                                    <span class="username username-hide-on-mobile"> <?php echo $_SESSION['username']; ?>  </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!-- <li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo_2.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li> -->
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out 
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->


                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>                                
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                   
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <div class="container">
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- END SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse ">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <?php 
                        
                        // if (!isset($_SESSION['signin'])) {
                        //     $_SESSION['signin'] = '';
                        // }
                            // echo $_SESSION['signin'];
                            if (isset($_SESSION['signin']) && $_SESSION['signin'] == 'team_manager') {
                                $teamlogin_id = $_SESSION['team_manager_id'];
                                $team_manager_id = "and id='$teamlogin_id'";
                                $customer_id = $_SESSION['loginid'];
                                $pageprivileage = $_SESSION['pageprivileage'];
                                $page = explode("#",$pageprivileage);
                                $res = $conn->prepare("select * from customer_tv_station where team_id='$teamlogin_id' and customer_id='$customer_id'");
                                $res->execute();
                                $row = $res->fetch(PDO::FETCH_ASSOC);
                                $manage_videos = $row['active'];
                            if (in_array('1',$page)) { ?>
                            <li class="nav-item <?php if($Current_PHP_File=="team_list.php"|| $Current_PHP_File=="manage_team.php"){echo "active";} ?>" >
                                <a href="manage_team.php?tid=<?php echo base64_encode($teamlogin_id);?>" class="nav-link nav-toggle">
                                    <i class="icon-user"></i>
                                    <span class="title">Edit Team</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>
                             <?php }
                            if (in_array('3',$page)) { ?>
                            <li class="nav-item <?php if($Current_PHP_File=="player_list.php"|| $Current_PHP_File=="manage_player.php"){echo "active";} ?>" >
                                <a href="player_list.php" class="nav-link nav-toggle">
                                    <i class="icon-user"></i>
                                    <span class="title">Manage Players</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>   
                            <?php }
                            if (in_array('2',$page)) { ?>        
                            <li class="nav-item <?php if($Current_PHP_File=="game_list.php"|| $Current_PHP_File=="mange_game.php"){echo "active";} ?>">
                                <a href="game_list.php" class="nav-link nav-toggle">
                                    <i class="icon-globe"></i>
                                    <span class="title">Manage Games</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li> 
                             <?php }
                             if ($manage_videos == true) { ?>
                            <li class="nav-item <?php if($Current_PHP_File=="manage_videos.php"){echo "active";} ?>" >
                                <a href="manage_videos.php" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-facetime-video"></i>
                                    <span class="title">Manage Videos</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>
                        <?php } } elseif ($_SESSION['usertype'] == 'admin') { ?>
                            <li class="nav-item <?php if($Current_PHP_File=="customerlist.php"|| $Current_PHP_File=="addcustomer.php"){echo "active";} ?>">
                                <a href="customerlist.php" class="nav-link nav-toggle">
                                    <i class="icon-social-dribbble"></i>
                                    <span class="title">Manage Customers</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>
                       <?php } else {?>

                            <!-- <li class="nav-item <?php if($Current_PHP_File=="customerlist.php"|| $Current_PHP_File=="addcustomer.php"){echo "active";} ?>">
                                <a href="customerlist.php" class="nav-link nav-toggle">
                                    <i class="icon-social-dribbble"></i>
                                    <span class="title">Manage Customers</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>
                            <li class="nav-item  <?php if($Current_PHP_File=="manage_season.php"|| $Current_PHP_File=="manage_season.php"){echo "active";} ?>">
                                <a href="manage_season.php" class="nav-link nav-toggle">
                                    <i class="icon-trophy"></i>
                                    <span class="title">Season/Division</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>  -->                           
                            <li class="nav-item <?php if($Current_PHP_File=="team_list.php"|| $Current_PHP_File=="manage_team.php"){echo "active";} ?>">
                                <a href="team_list.php" class="nav-link nav-toggle">
                                    <i class="icon-users"></i>
                                    <span class="title">Manage Teams</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                               
                            </li>
                            <li class="nav-item <?php if($Current_PHP_File=="player_list.php"|| $Current_PHP_File=="manage_player.php"){echo "active";} ?>">
                                <a href="player_list.php" class="nav-link nav-toggle">
                                    <i class="icon-user"></i>
                                    <span class="title">Manage Players</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>           
                            <li class="nav-item <?php if($Current_PHP_File=="game_list.php"|| $Current_PHP_File=="manage_game.php"){echo "active";} ?>">
                                <a href="game_list.php" class="nav-link nav-toggle">
                                    <i class="icon-globe"></i>
                                    <span class="title">Manage Games</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>   
							<li class="nav-item <?php if($Current_PHP_File=="manage_conference.php"|| $Current_PHP_File=="conference_list.php"){echo "active";} ?>" >
                                <a href="conference_list.php" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-globe"></i>
                                    <span class="title">Manage Conference</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li> 
                             <li class="nav-item <?php if($Current_PHP_File=="manage_division.php"|| $Current_PHP_File=="division_list.php"){echo "active";} ?>" >
                                <a href="manage_division.php" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-certificate"></i>
                                    <span class="title">Manage Division</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li> 
                            <li class="nav-item <?php if($Current_PHP_File=="manage_social_media.php"|| $Current_PHP_File=="manage_social_media.php"){echo "active";} ?>" >
                                <a href="manage_social_media.php" class="nav-link nav-toggle">
                                    <i class="fa fa-share"></i>
                                    <span class="title">Manage Social Media</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li>
                            <li class="nav-item <?php if($Current_PHP_File=="manage_videos.php"|| $Current_PHP_File=="manage_videos.php"){echo "active";} ?>" >
                                <a href="manage_videos.php" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-facetime-video"></i>
                                    <span class="title">Manage Videos</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li> 
							<li class="nav-item <?php if($Current_PHP_File=="manage_season.php"|| $Current_PHP_File=="manage_season.php"){echo "active";} ?>" >
                                <a href="manage_season.php" class="nav-link nav-toggle">
                                    <i class="icon-trophy"></i>
                                    <span class="title">Customer Season</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li> 
                            <li class="nav-item <?php if($Current_PHP_File=="settings.php"|| $Current_PHP_File=="settings.php"){echo "active";} ?>">
                                <a href="settings.php" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">Settings</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>                                
                            </li> 
                        <?php  } //} ?>  

                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <div class="modal fade" id="myModalDelete">
                    <div class="modal-dialog">
                        <form action="deletecustomer.php" method="POST" id="deletecustform">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 class="modal-title">Delete customer details </h3>
                            </div>
                            <style>
                            #tblGrid tr td{
                                border-top:0;
                            }
                            </style>
                            <div class="modal-body">
                                <table class="table table-striped" id="tblGrid">
                                    <tbody>
                                        <tr>
                                            
                                            <td>
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" name="custinfo" value="customer_information" id="custinfo">Customer information<span></span>
                                            </label>
                                            </td>
                                            <td>
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" name="ftpinfo" value="ftp" id="ftpinfo">FTP<span></span>
                                                
                                            </label>
                                            </td>
                                            <td>
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" name="subdominfo" value="subdomain" id="subdominfo">Subdomain<span></span>
                                            </label>
                                            </td>
                                            <td>
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" name="foldnfiles" value="foldernfiles" id="foldnfiles">Folder and Files<span></span>
                                                <input type="hidden" name="custid" id="custid">
                                                <input type="hidden" name="weburl" id="weburl">
                                            </label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default " data-dismiss="modal">Close</button>
                                <input type="submit" name="deletecust" class="btn blue" value="Delete" id="deletecust" >
                            </div>
                        </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
