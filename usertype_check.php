<?php
$Cid = "";
if (!empty($_SESSION['loginid']))  {

    if ($_SESSION['usertype'] == 'user' || $_SESSION['usertype'] == 'team_manager') {
        $Cid = $_SESSION['loginid']; 
        $cid = $Cid;
    } else { 
        header('Location:login.php');
        exit;
    }
} else {
    header('Location:login.php');
    exit;
}